# Program to calculate the Hardy stress using the Weighting (Localization fn) and Bond function
"""
LAMMPS "metal" units are followed
mass = grams / mole
distance = Angstroms
time = picoseconds
energy = eV
force = eV / Angstrom
velocity = Angstrom / picosecond
electron density -> Arbitrary unit
volume = Angstrom^3
stress = ev / Angstrom^3
"""

import numpy as np
from tqdm import tqdm

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient numerical computation. 
#    Computing in Science & Engineering, 13(2):22–30, 2011.
# 2) Nikhil Chandra Admal and Ellad B Tadmor. A unified interpretation of stress in molecular systems. Journal of elasticity, 100(1-2):63–143, 2010.
# 3) Robert J Hardy. Formulas for determining local properties in molecular-dynamics simulations: Shock waves. The Journal of Chemical Physics, 
#    76(1):622–628, 1982.) 
# 4) Paul Bourke. Spheres, equations and terminology, http://paulbourke.net/geometry/, April 1992
####################################################################################################################################################################

Epsilon = 1e-8 # Constant for value comparisons with floating point errors (used in assert statement in get_bonding_factor)

"""================================================================================================================================================================
                                                                Localization Function
   ================================================================================================================================================================"""

def get_weighting_factor(Spatial_averaging_volume_radius, Delta_radius, Radius_vector):
    """
        Description:
        ===========
        Calculates the weighting factor based on the distance between the alpha and beta 
        atoms based on the uniform kernel function
        
        Input:
        =====
        1) Spatial_averaging_volume_radius = Radius of the spherical averaging volume; Array size -> Scalar
        2) Delta_radius = Radius value at which mollifying function starts; Array size -> Scalar
        3) Radius vector = Distance vectors between alpha atom and all other atoms; Array size -> (No of atoms, 3)
        
        Output:
        ======
        1) Total_weighting_factor = Weighting factor for  all radius value; Array size -> (1,No of atoms)
        2) Constant_weighting_factor = Uniform weighting function value based on the spatial averaging radius; Array size -> Scalar
    """
    
    Constant_weighting_factor = 1 / ((4/3) * np.pi * Spatial_averaging_volume_radius**3) # 1/v for normalizing; Spherically symmetric uniform function; Integral = 1
    Radius = np.linalg.norm(Radius_vector,axis = 1) # Scalar radius value 
    Mollifying_function = 0.5 * Constant_weighting_factor * (1 - np.cos((Spatial_averaging_volume_radius - Radius) * (np.pi / Delta_radius))) # Mollifying function; Ref - Admal and Tadmor (MDstresslab)
    Total_weighting_factor = np.select([Radius > Spatial_averaging_volume_radius, Radius <= Spatial_averaging_volume_radius], [0,Radius]) # Radius greater than the characteristic size has "0" weights
    Total_weighting_factor = np.select([Total_weighting_factor >= (Spatial_averaging_volume_radius - Delta_radius), Radius < (Spatial_averaging_volume_radius - Delta_radius)], 
                                [0.5 * Constant_weighting_factor * (1 - np.cos((Spatial_averaging_volume_radius - Total_weighting_factor) * (np.pi / Delta_radius))), Constant_weighting_factor]) # Use of Weighting factor instead of radius for condition is to eliminate "0" to be replaced
    return Total_weighting_factor, Constant_weighting_factor
    
def get_bond_length_inside_volume(x_vector, x_alpha, x_beta, Spatial_averaging_volume_radius, Vector_center_to_alpha):
    """
        Description:
        ===========
        Calculates the points of intersection of the straight bond with the spatial averaging volume and 
        its radius from the center of the atom of interest. Ref - http://paulbourke.net/geometry/circlesphere/.
        Based on the quadratic equation, at^2 + bt + c = 0 and using the equation of the circle and a line segment.

        Input:
        =====
        1) x_vector = Coordinate of the atom for which stress is computed; Array size -> (1,3)
        2) x_alpha = Coordinates of the alpha atoms; Array size -> (No of alpha atoms, 3)
        3) x_beta = Coordinates of all the neighbors (beta) of alpha atoms; Array size -> (No of alpha atoms, No of neighbors, 3)
        4) Vector_center_to_alpha = Distance vector between x_vector and alpha atoms; Array size -> (No of alpha atoms, 3)
        
        Output:
        ======
        1) Bond_length_inside_volume = Length of the bonds inside the spatial averaging volume; Array size -> (1,No of alpha - beta bonds)
    """

    No_of_x_beta = int(x_beta.size / len(x_beta)) # getting the initial no of beta atoms 

    # Quadratic form variables
    a = np.sum((x_beta - x_alpha[:,None])**2, axis = 2) 
    b = np.sum((2 * (x_beta - x_alpha[:,None]) * (x_alpha[:,None] - x_vector)), axis = 2) # sum along axis = 2 since 3D array
    c = np.sum((x_alpha - x_vector)**2, axis = 1) - Spatial_averaging_volume_radius**2

    Negative_indices = np.argwhere((b**2 - (4*a*c.reshape(len(c),1))) < 0) # np.argwhere() -> To have the result in terms of numpy array not a tuple Finding the index which violates the quadratic form. The bond connecting the atoms (x-beta[i]) & x_alpha lies completely outside the spatial averaging volume 

    if Negative_indices.size != (0.5 * No_of_x_beta * len(x_alpha)): # Executes only if not all bonds lie outside of the volume
        Mask_x_beta = np.zeros_like(x_beta,bool) # Boolean array for masking beta atoms that lie outside of the volume
        Mask_a, Mask_b = np.zeros_like(a,bool), np.zeros_like(b,bool) # Boolean array for masking corresponding "a" & "b"" that lie outside of the volume

        if len(Negative_indices) != 0: # If atleast 1 bond lies outside of the averaging volume
            if len(Negative_indices.shape) > 1: # For more than 1 bond lying out of volume
                Mask_x_beta[Negative_indices[:,0],Negative_indices[:,1]] = True
                a[Negative_indices[:,0],Negative_indices[:,1]] = 0 # Making the values of the varibles for the bonds lying out of volume = 0
                b[Negative_indices[:,0],Negative_indices[:,1]] = 0
            else:
                Mask_x_beta[Negative_indices[0,0],Negative_indices[0,1]] = True
                a[Negative_indices[0,0],Negative_indices[0,1]] = 0
                b[Negative_indices[0,0],Negative_indices[0,1]] = 0

            x_beta = np.ma.masked_array(x_beta, Mask_x_beta) # Masks the correspoding beta rows that lie outside of volume

        t1 = (-b + np.sqrt((b**2) - (4*a*c.reshape(len(c),1)))) # Real number which is used in the parametric form as a scaling factor; 0<t<1
        t1 = np.divide(t1, (2*a), out = np.zeros_like(t1), where = a!=0) # To avoid divide by "0" which will appear if atleat one of the bond lie outside of the volume
        t2 = (-b - np.sqrt((b**2) - (4*a*c.reshape(len(c),1)))) # A straight bond will intersect the volume at two points -> So t1 & t2
        t2 = np.divide(t2, (2*a), out = np.zeros_like(t2), where = a!=0)
        t = np.array([t1,t2])

        x_beta_reshape = x_beta.reshape(np.prod(x_beta.shape[:2]),x_beta.shape[2]) # Reshaping the beta atoms to (No of beta atoms, 3)

        Intersection_points_1 = (x_beta_reshape - np.repeat(x_alpha, repeats = x_beta.shape[1], axis = 0)) * t[0].reshape(np.prod(x_beta.shape[:2]),1) + np.repeat(x_alpha, repeats = x_beta.shape[1], axis = 0) # Points of intersection for all the x_beta intersecting the volume; Reshaping is done for array dimension compatibility; For all the rows of beta atoms, alpha atom coordinate will be the same (that why np.repeat is used)
        Intersection_points_2 = (x_beta_reshape - np.repeat(x_alpha, repeats = x_beta.shape[1], axis = 0)) * t[1].reshape(np.prod(x_beta.shape[:2]),1) + np.repeat(x_alpha, repeats = x_beta.shape[1], axis = 0) # Only 2 intersection points are possible for a convex sphere !

        t1 = t[0].reshape(np.prod(x_beta.shape[:2]))
        t2 = t[1].reshape(np.prod(x_beta.shape[:2]))

        """*************************Bonds lying completely outside of the volume*************************"""

        Bond_indices_out_of_volume = np.argwhere(((t1 <= 0) & (t2 <= 0)) | ((t1 >= 1) & (t2 >= 1))).flatten()#  if len(np.argwhere(((t1 <= 0) & (t2 <= 0)) | ((t1 >= 1) & (t2 >= 1)))) == 0 else np.argwhere(((t1 <= 0) & (t2 <= 0)) | ((t1 >= 1) & (t2 >= 1)))[0] # If both t1 & t2 are not within the range (0,1), then the corresponding bond is outside the volume. "&" and "|" are overloading in numpy for "and" and "or" operator respectively !
        Intersection_points_1[Bond_indices_out_of_volume], Intersection_points_2[Bond_indices_out_of_volume] = np.zeros_like(Intersection_points_1[Bond_indices_out_of_volume]), np.zeros_like(Intersection_points_2[Bond_indices_out_of_volume]) # Replacing intersection points of the corresponding bonds with "0"; If the intersection points are "0" -> Bond length becomes "0"

        """*************************Bonds lying completely inside of the volume*************************"""

        Bond_indices_inside_volume = np.argwhere(((t1 <= 0) & (t2 >= 1)) | ((t2 <= 0) & (t1 >= 1))).flatten() #if len(np.argwhere(((t1 <= 0) & (t2 >= 1)) | ((t2 <= 0) & (t1 >= 1)))) == 0 else np.argwhere(((t1 <= 0) & (t2 >= 1)) | ((t2 <= 0) & (t1 >= 1)))[0] # If one of t1/t2 is less than zero and the other t1/t2 is greater than 1, then the bond lies completely inside the volume

        Intersection_points_1[Bond_indices_inside_volume] = np.ones_like(Intersection_points_1[Bond_indices_inside_volume]) * x_alpha[np.floor(Bond_indices_inside_volume / x_beta.shape[1]).astype(int)] # Making the intersection point 2 as the x_alpha coordinate
        Intersection_points_2[Bond_indices_inside_volume] = x_beta_reshape[Bond_indices_inside_volume] # Making the intersection point 2 as the x_beta coordinate
        
        """*************************Bonds crossing the volume at one point*************************"""        

        Bonds_crossing_at_one_point_1 = np.setdiff1d(np.argwhere((t1<=0) | (t1>=1)).flatten(), np.concatenate((Bond_indices_out_of_volume,Bond_indices_inside_volume))) # finding the indices that is not within the range(0,1) & also not previously listed under the above conditions

        if len(Bonds_crossing_at_one_point_1) != 0:
            Intersection_points_1[Bonds_crossing_at_one_point_1] = np.array([x_alpha[np.floor(bond_index / x_beta.shape[1]).astype(int)] if (np.sum(Vector_center_to_alpha[np.floor(bond_index / x_beta.shape[1]).astype(int)]**2) <= Spatial_averaging_volume_radius**2) else x_beta_reshape[bond_index] for bond_index in Bonds_crossing_at_one_point_1]) # Assigning x_beta / x_alpha based on which point is inside the volume ! x_beta.shape[1] -> Gives the number of neighbors for each atom 

        Bonds_crossing_at_one_point_2 = np.setdiff1d(np.argwhere((t2<=0) | (t2>=1)).flatten(), np.concatenate((Bond_indices_out_of_volume,Bond_indices_inside_volume))) # finding the indices that is not within the range(0,1) & also not previously listed under the above conditions
        
        if len(Bonds_crossing_at_one_point_2) != 0:
            Intersection_points_2[Bonds_crossing_at_one_point_2] = np.array([x_alpha[np.floor(bond_index / x_beta.shape[1]).astype(int)] if (np.sum(Vector_center_to_alpha[np.floor(bond_index / x_beta.shape[1]).astype(int)]**2) <= Spatial_averaging_volume_radius**2) else x_beta_reshape[bond_index] for bond_index in Bonds_crossing_at_one_point_2]) # Assigning x_beta / x_alpha based on which point is inside the volume !

        Bond_length_inside_volume = np.linalg.norm((Intersection_points_1 - Intersection_points_2), axis = 1) # Since both Intersection_points_1 & Intersection_points_2 both contains the masked coordinates for the respective atoms, taking their norm results "0" !!

    else:
        Bond_length_inside_volume = np.zeros(No_of_x_beta) # For the case, where all the bonds lies outside of the volume

    return Bond_length_inside_volume
    
    
"""================================================================================================================================================================
                                                                Bonding Function
   ================================================================================================================================================================"""
        
def get_bonding_factor(x_alpha,x_beta,x_vector, Constant_weighting_factor, Delta_radius, Spatial_averaging_volume_radius):
    """
        Description:
        ===========
        Calculates the weighted fraction of the bond length of atoms alpha and beta within the characteristic volume based using the uniform
        localization function
        
        Input:
        =====
        1) x_vector = Coordinate of the atom for which stress is computed; Array size -> (1,3)
        2) x_alpha = Coordinates of the alpha atoms; Array size -> (No of alpha atoms, 3)
        3) x_beta = Coordinates of all the neighbors (beta) of alpha atoms; Array size -> (No of alpha atoms, No of neighbors, 3)
        4) Constant_weighting_factor = Uniform weighting function value; Array size -> Scalar
        5) Delta_radius = Radius value at which mollifying function starts; Array size -> Scalar
        6) Spatial_averaging_volume_radius = Radius of the spherical averaging volume; Array size -> Scalar
        
        Output:
        ======
        1) Total_bonding_factor = Bonding factor for all bonds surrounding & crossing the spatial averaging volume; Array size -> (No of atoms, No of neighbors)
    """
        
    Vector_center_to_alpha = (x_vector - x_alpha) # Vector pointing from center of the sphere to the alpha atom
    Vector_center_to_beta = (x_vector - x_beta) # Vector pointing from center of the sphere to the beta atoms
    Total_bond_length = np.linalg.norm((x_alpha[:,None] - x_beta), axis = 2) # Bond length between all alpha with all its neighbor beta atoms
    Bond_lengths_inside_volume = np.zeros(x_beta.shape[:2]) # Dimensions - (No of atoms, No of neighbors of each atom)

    Index_alpha_equals_xvector = np.asscalar(np.argwhere((x_alpha == x_vector).all(axis = 1))) # Extracting the index of the alpha atom whose coordinates = x_vector coordinates; np.asscalar() -> Since it will be only a scalar (only one alpha can match x_vector coordinate)
    Mask_alpha = np.ones_like(x_alpha, bool) # Creates a boolean array. 1 - True; 0 - False
    Mask_beta = np.ones_like(x_beta, bool)
    Mask_alpha[Index_alpha_equals_xvector] = False # Making the boolean value of the row where x_alpha == x_vector; Removing it since for this atom, all its bonds with its neighbors will intersect the volume
    Mask_beta[Index_alpha_equals_xvector] = False

    Bond_lengths_inside_volume[Index_alpha_equals_xvector] = np.select([Total_bond_length[Index_alpha_equals_xvector] <= Spatial_averaging_volume_radius, Total_bond_length[Index_alpha_equals_xvector] > Spatial_averaging_volume_radius],[Total_bond_length[Index_alpha_equals_xvector], Spatial_averaging_volume_radius]) # If less than spatial averaging radius, radius is taken else spatial averaging radius since all of the bonds intersects the volume

    x_beta_mask_indices = (x_beta[Index_alpha_equals_xvector].mask)[:,0] # Taking the mask indices values for the neighbors of alpha that is equal to x_vector; [:,0] -> Considering only the first column values since other columns will also have the same boolean value !
    Bond_lengths_inside_volume[Index_alpha_equals_xvector][x_beta_mask_indices] = 0 # Explicitly making the indices of the bond length index equal to the Index_alpha_equals_xvector to "0" because taking norm of x_alpha & masked x_beta will result in a value rather than "0"; [0] -> since these are 2D arrays
    Bond_lengths_inside_volume[np.setdiff1d(np.arange(len(Bond_lengths_inside_volume)), Index_alpha_equals_xvector)] = get_bond_length_inside_volume(x_vector, x_alpha[Mask_alpha].reshape((len(x_alpha)-1,x_alpha.shape[1])), 
                                                              x_beta[Mask_beta].reshape((np.array(x_beta.shape) - np.array([1,0,0]))), Spatial_averaging_volume_radius, (Vector_center_to_alpha[Mask_alpha].reshape((len(x_alpha)-1,x_alpha.shape[1])))).reshape((len(x_alpha)-1),-1) # "len(x_alpha)-1" -> Since x_alpha = x_vector is neglected; Calling the get_bond_length_inside_volume function with arguments having all the values other than x_alpha == x_vector contribution

    Bond_fraction = np.divide(Bond_lengths_inside_volume, Total_bond_length, out = np.zeros_like(Bond_lengths_inside_volume), where=Total_bond_length!=0) # Fraction of the bond lying inside the volume; np.divide() used to avoid divide by "0" in the case of Free boundary conditions in masked arrays
    Total_bonding_factor = Bond_fraction * Constant_weighting_factor # Since using a constant weighting factor, only the fraction of the bond inside the volume weights the contribution of the bond to the stress at x_vector. 

    Total_bonding_factor_virial = np.floor(Bond_fraction) * Constant_weighting_factor # Since only the bonds completely inside the averaging volume are considered: np.floor() function returns the largest integer i such that i<=x (np.floor(0.6) = 0)
     
    assert (Bond_lengths_inside_volume <= 2*Spatial_averaging_volume_radius + Epsilon).all(), "Bonds lengths inside volume greater than the diameter of the averaging volume - CHECK atomic coordinates !" # Maximum bond length inside volume cannot be greater than the Diameter of the sphere; Epsilon = 1e-10 -> A small value is added since bond lengths inside volume exactly equals the diameter but has floating point errors

    return Total_bonding_factor, Total_bonding_factor_virial
    

def get_hardy_stress(No_of_atoms,Cluster_id_flatten,Velocity_vector, Atomic_mass, Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_vector, Total_atomic_coordinates, Neighbors_list, Spatial_averaging_volume_radius, Force_radius_tensor, Neighbor_atomic_coordinates_masked):
    """
        Description:
        ===========
        Calculates the per atom Hardy stress tensor using the cluster size as the spatial averaging volume radius and uses a constant uniform 
        weight (localization) function and a bond function
        
        Input:
        =====
        1) No_of_atoms = No of local atoms; Array size -> Scalar
        2) Cluster_id_flatten = Flattened array of cluster atom indices; Array size -> (1,No of atoms)
        3) Velocity_vector = Velocity vector of all local atoms; Array size -> (No of atoms, 3)
        4) Atomic_mass = Mass of the atom; Array size -> Scalar
        5) Distance_x_vector_unique = Distance x values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        6) Distance_y_vector_unique = Distance y values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        7) Distance_z_vector_unique = Distance z values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        8) Force vector = Force components of each with all other atoms; Array size -> (3,No of atoms,No of atoms)
        9) Total_atomic_coordinates = Coordinates of all local + image atoms; Array size -> (Total no of atoms,3)
        10) Neighbors_list = List of neighbors of all atoms (Local + Images); Array size -> List of different sized arrays based on the number of neighbors to each atom
        11) Spatial_averaging_volume_radius = Radius of the averaging sphere; Array size -> Scalar
        12) Force_radius_tensor = Force radius tensor components for all atoms; Array size -> (Total no of atoms, No of neighbors, 9)
        13) Neighbor_atomic_coordinates_masked = Atomic coordinates of the neighbors of all atoms with non-unique neighbors masked; Array size -> (Total no of atoms, No of unique neighbors, 3)
        
        Output:
        ======
        1) Hardy stress tensor = 9 component stress tensor for each atom; Array size -> (No of atoms,3,3)
    """
    
    Atomic_coordinates = Total_atomic_coordinates[:No_of_atoms] # Coordinates of local atoms
    Total_no_of_atoms = len(Total_atomic_coordinates) # Local atoms + Ghost atoms
    
    """The spatial averaging volume radius will be used as the radius of the volume averaging sphere for every cluster
    and it will be the cut-off radius for the spherically symmetric uniform kernel (localization). Hardy 
    stress expression is robust and is also INDEPENDENT of the choice of the LOCALIZATION function (Ref: Admal and Tadmor, 2011)"""    

    Delta_radius = 0.12 * Spatial_averaging_volume_radius # Distance from the characteristic radius where the mollifying function starts (To eliminate discontinuity)

    # Initialization
    Hardy_stress_tensor = np.zeros((No_of_atoms,3,3))
    Virial_stress_tensor = np.zeros((No_of_atoms,3,3))
 
    for i in tqdm(Cluster_id_flatten): # Stress tensor calculation for the atoms that are in the homogeneous cluster
        
        """================================================================================================================================================================
                                                                                    Kinetic contribution
           ================================================================================================================================================================"""
        
        Inter_atomic_distances = np.array([Distance_x_vector_unique[i], Distance_y_vector_unique[i], Distance_z_vector_unique[i]]).T # Creates all atom distances with one atom into a 2D array with shape (Total no of atoms,Dimensions); Periodic images are taken into account !
        Weighting_factor_all_atoms, Constant_weighting_factor = get_weighting_factor(Spatial_averaging_volume_radius,Delta_radius,Inter_atomic_distances)
        Weighting_factor_all_local_atoms = np.array([np.sum(Weighting_factor_all_atoms[index::No_of_atoms]) for index in range(No_of_atoms)]) # Summing up the contributions from all the periodic images for a local atom; Either periodic image / the local atom of a particular index will have a non-zero value; This is to assign the periodic contribution to the corresponding local atom; Values will not change
        Relative_velocities = (Velocity_vector - Velocity_vector[i]) # Considering only the relative velocity with respect to the "i"th atom
        Velocity_tensor = (Relative_velocities[:,:,None] * Relative_velocities[:,None]) # Per atom relative velocity tensor
        Kinetic_contribution_all_atoms = Atomic_mass * (Velocity_tensor.reshape(No_of_atoms,9)).T * Weighting_factor_all_local_atoms # All of the velocity tensors should be added with their corresponding weight factor
        Kinetic_contribution = (np.sum(Kinetic_contribution_all_atoms,axis = 1)).reshape(3,3) # Adding the corresponding components of all the tensors and reshaping back into a (3x3) tensor
        # Reshape & Transpose of velocity tensor is to make it compatible with the weight factor multiplication. 9 -> No of dimensions of velocity tensor
                
        """================================================================================================================================================================
                                                                                    Potential contribution
           ================================================================================================================================================================"""

        Bond_factor_hardy, Bond_factor_virial = get_bonding_factor(Total_atomic_coordinates, Neighbor_atomic_coordinates_masked, Atomic_coordinates[i],Constant_weighting_factor,Delta_radius,Spatial_averaging_volume_radius) # All alpha beta arrays calculating at a particular point @ atom "i" position
        Stress_tensor_all_atoms_hardy = np.array([np.sum(Force_radius_tensor[id].T * Bond_factor_hardy[id], axis = 1) for id in range(Total_no_of_atoms)]) # Calculates the force radius tensor weighted with bond factor for all atoms. Sum is done on the neighbors for each atom. Array size -> (No of atoms, 9)    
        Stress_tensor_all_atoms_virial = np.array([np.sum(Force_radius_tensor[id].T * Bond_factor_virial[id], axis = 1) for id in range(Total_no_of_atoms)]) # Calculates the force radius tensor weighted with bond factor for all atoms. Sum is done on the neighbors for each atom. Array size -> (No of atoms, 9)    
        Potential_contribution_hardy = (np.sum(Stress_tensor_all_atoms_hardy,axis = 0)).reshape(3,3) # Here, axis = 0, since no need to multiply with any 1D factor
        Potential_contribution_virial = (np.sum(Stress_tensor_all_atoms_virial,axis = 0)).reshape(3,3) # Here, axis = 0, since no need to multiply with any 1D factor
        Hardy_stress_tensor[i] = - (Potential_contribution_hardy + Kinetic_contribution) # Total contributions to the Hardy stress tensor per atom
        Virial_stress_tensor[i] = - (Potential_contribution_virial + Kinetic_contribution) # Total contributions to the Virial stress tensor per atom

    return Hardy_stress_tensor, Virial_stress_tensor
        
