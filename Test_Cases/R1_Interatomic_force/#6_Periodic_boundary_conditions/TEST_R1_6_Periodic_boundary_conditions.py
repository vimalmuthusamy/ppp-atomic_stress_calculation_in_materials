# Testing program for Interatomic force calculation routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R1_Interatomic_force import get_interatomic_force
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
from tqdm import tqdm
from mpl_toolkits.mplot3d import Axes3D

"""===========================Periodic Boundary Conditions==========================="""


def get_force_routine_inputs(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions


#########################################################################################################################################################
#                                                           Ag
#########################################################################################################################################################

def test_Ag_pbc():
    """
        ############################## TEST - R1 -> 6 - Periodic Boundary Conditions ##############################
        Description:
        ------------
        Testing the image boxes constructed around the local simulation box in the case of Periodic Boundary Conditions (PBC).
        
        Reference Paper : An embedded-atom potential for the Cu–Ag system, P L Williams , Y Mishin and J C Hamilton; Modelling Simul. Mater. Sci. Eng. 14 (2006) 817–833 – Published 30 May 2006
    """
    
    print(test_Ag_pbc.__doc__)
    print("="*100+"\n\t\t\t\t\t\tAg\n"+"="*100)

    Potential_file_name = "Ag.eam.alloy"
    Element = "Ag"
    Atomic_coordinates_file_name = "dump.Ag_SX_l_3.custom.2"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    
    # Plots
    DPI = 100
    
    fig = plt.figure(figsize = (60,60))
    ax = Axes3D(fig)
    ax.scatter(Total_atomic_coordinates[:,0],Total_atomic_coordinates[:,1],Total_atomic_coordinates[:,2],label = "Image atoms", s = 500)
    ax.scatter(Atomic_coordinates[:,0],Atomic_coordinates[:,1],Atomic_coordinates[:,2], label = "Local atoms", color = "r", s = 2000)
    ax.grid()
    ax.legend()
    ax.set_xlabel("x coordinate "+ r"$(\AA)$",labelpad=100)
    ax.set_ylabel("y coordinate "+ r"$(\AA)$",labelpad=100)
    ax.set_zlabel("z coordinate "+ r"$(\AA)$",labelpad=100)
    ax.set_title("Local box + Image boxes (PBC)", weight = "bold")
    plt.savefig("./Results/Local_plus_image_boxes_3d.png", dpi = DPI)
    plt.close()

    fig, ax = plt.subplots(figsize = (60,60))
    ax.scatter(Total_atomic_coordinates[:,0],Total_atomic_coordinates[:,1],label = "Image atoms", s = 500)
    ax.scatter(Atomic_coordinates[:,0],Atomic_coordinates[:,1], label = "Local atoms", color = "r", s = 2000)
    ax.grid()
    ax.legend()
    ax.set_xlabel("x coordinate "+ r"$(\AA)$",labelpad=100)
    ax.set_ylabel("y coordinate "+ r"$(\AA)$",labelpad=100)
    ax.set_title("Local box + Image boxes (PBC)", weight = "bold")
    plt.savefig("./Results/Local_plus_image_boxes_2d.png", dpi = DPI)
    plt.close()
        
test_Ag_pbc()

