# Testing program for Interatomic force calculation routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R1_Interatomic_force import get_interatomic_force
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
from tqdm import tqdm

"""===========================Al Diatom force & energy comparison==========================="""


def get_force_routine_inputs(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions


#########################################################################################################################################################
#                                                           Al
#########################################################################################################################################################

def test_Al_diatom():
    """
        ############################## TEST - R1 -> 5 - Force & Energy comparison for a Al diatom sample ##############################
        Description:
        ------------
        Force & Energy values should correlate with the reference values.
        
        Reference Paper : Development of an interatomic potential for the Ni-Al system, G.P. Purja Pun and Y. Mishin, Vol. 89, Nos. 34–36, 1–21 December 2009, 3245–3267
    """
    
    print(test_Al_diatom.__doc__)
    print("="*100+"\n\t\t\t\t\t\tAl\n"+"="*100)

    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Al"
    
    Radius, Energy, Ni, Al = np.genfromtxt("Al_diatom.txt", comments="#",unpack = True)[0:,1:] # [0:,1:] is to avoid the radius & element strings in the file
    Delta_radius = np.gradient(Radius)[0] # Since all the values are the same
    Force_computed = np.gradient(Energy,0.02, edge_order = 2)

    Atomic_coordinates = np.zeros((2,3))
    Boundary_conditions = ["ff","ff","ff"] # Free boundary conditions

    No_of_atoms = 2
    
    Potential_energy_diatom = np.zeros(len(Radius))
    Force_diatom = np.zeros(len(Radius))
    
    for i,distance in tqdm(enumerate(Radius)):
        Atomic_coordinates[0][0] = distance
        Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, [0,0,0], Boundary_conditions)
        Potential_energy_diatom[i] = Potential_energy
        Force_diatom[i] = Force_per_atom[1,0] # Taking only the x component of the second atom
        plt.close()
        
    #Plots 
    DPI = 100
    
    fig, ax = plt.subplots(2, figsize = (60,60))
    ax[0].plot(Radius,Energy, label = "Potential data", linewidth = 30)
    ax[0].plot(Radius,Potential_energy_diatom, label = "Computed", linestyle = "-",linewidth = 10, color = "red")
    ax[1].plot(Radius,Force_computed, label = "Potential data",linewidth = 30)
    ax[1].plot(Radius,Force_diatom, label = "Computed (x component)", linestyle = "-",linewidth = 10, color = "red")
    ax[0].set(title = "Radius vs Potential Energy", xlabel = "Radius ("+r"$\AA$"+")", ylabel = "Potential energy (eV)")
    ax[1].set(title = "Radius vs Force (x component)", xlabel = "Radius ("+r"$\AA$"+")", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")")
    plt.subplots_adjust(hspace = 0.5)
    plt.suptitle("Al diatom system potential energy & force calculation \n {}".format(Potential_file_name),fontweight = "bold")
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    plt.savefig("./Results/test_Al_diatom_force.png", dpi = DPI)
    plt.close()
        
test_Al_diatom()

