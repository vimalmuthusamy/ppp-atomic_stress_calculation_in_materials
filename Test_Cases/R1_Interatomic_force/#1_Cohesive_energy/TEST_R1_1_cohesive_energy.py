# Testing program for Interatomic force calculation routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R1_Interatomic_force import get_interatomic_force
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick


"""===========================Cohesive energies of Ag, Al & Pt==========================="""


def get_force_routine_inputs(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions


def test_Ag_Al_Pt_Cohesive_energy():
    """
        ############################## TEST - R1 -> 1 - Cohesive energy ##############################
        Description:
        ------------
        Calculates the Cohesive energies of Ag, Al & Pt single crystal and compares it with the values in the reference paper.
        Compares the Lattice_constant, Cutoff_radius and Atomic_mass values to the values in the EAM/ALLOY potential file for Ag.
        
        Reference Paper : An embedded-atom potential for the Cu–Ag system, P L Williams , Y Mishin and J C Hamilton; Modelling Simul. Mater. Sci. Eng. 14 (2006) 817–833 – Published 30 May 2006
        Reference Paper : Development of an interatomic potential for the Ni-Al system, G.P. Purja Pun and Y. Mishin, Vol. 89, Nos. 34–36, 1–21 December 2009, 3245–3267
        Reference Paper : Embedded-atom-method functions for the fcc metals Cu, Ag, Au, Ni, Pd, Pt, and their alloys - S. M. Foiles, M. I. Baskes, and M. S. Daw; PHYSICAL REVIEW B, VOLUME 33, NUMBER 12, 15 JUNE 1986
    """
    
    print(test_Ag_Al_Pt_Cohesive_energy.__doc__)
    
    
    xaxis = ["Ref_paper","Computed","LAMMPS"]
    #########################################################################################################################################################
    #                                                           Ag
    #########################################################################################################################################################

    print("="*100+"\n\t\t\t\t\t\tAg\n"+"="*100)
    
    Cohesive_energy_Ag_LAMMPS = -2.84999997473
    Cohesive_energy_Ag_paper = -2.85 

    Potential_file_name = "Ag.eam.alloy"
    Element = "Ag"
    Atomic_coordinates_file_name = "dump.Ag_SX_cohesive_energy_l_3.custom.2"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Cohesive_energy_Ag_calculated = (Potential_energy / No_of_atoms)
    
    cohesive_Ag = [-Cohesive_energy_Ag_paper,-Cohesive_energy_Ag_calculated,-Cohesive_energy_Ag_LAMMPS]
    percent_cohesive_Ag = [-((Cohesive_energy_Ag_paper-Cohesive_energy_Ag_paper)/Cohesive_energy_Ag_paper*100), -((Cohesive_energy_Ag_calculated-Cohesive_energy_Ag_paper)/Cohesive_energy_Ag_paper*100), -((Cohesive_energy_Ag_LAMMPS-Cohesive_energy_Ag_paper)/Cohesive_energy_Ag_paper*100)]    
    
    print("Lattice constant (Angstrom) [REF] : ",0.4090000000E+01,"\tLattice constant imported : ",Lattice_constant)
    print("Cut off radius (Angstrom) [REF] : ",0.5995011000293092E+01,"\tCut off radius imported : ",Cutoff_radius)
    print("Atomic mass (g/mol) [REF] : ",0.1078680000E+03,"\tAtomic mass imported : ",Atomic_mass)

    #########################################################################################################################################################
    #                                                           Al
    #########################################################################################################################################################

    print("="*100+"\n\t\t\t\t\t\tAl\n"+"="*100)
    
    Cohesive_energy_Al_LAMMPS = -3.35999998785
    Cohesive_energy_Al_paper = -3.36
    
    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Al"
    Atomic_coordinates_file_name = "dump.Al_SX_cohesive_energy_l_3.custom.1"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Cohesive_energy_Al_calculated = (Potential_energy / No_of_atoms)
    cohesive_Al = [-Cohesive_energy_Al_paper,-Cohesive_energy_Al_calculated,-Cohesive_energy_Al_LAMMPS]
    percent_cohesive_Al = [-((Cohesive_energy_Al_paper-Cohesive_energy_Al_paper)/Cohesive_energy_Al_paper*100), -((Cohesive_energy_Al_calculated-Cohesive_energy_Al_paper)/Cohesive_energy_Al_paper*100), -((Cohesive_energy_Al_LAMMPS-Cohesive_energy_Al_paper)/Cohesive_energy_Al_paper*100)]

    
    #########################################################################################################################################################
    #                                                           Pt
    #########################################################################################################################################################

    Cohesive_energy_Pt_LAMMPS = -5.76999999934
    Cohesive_energy_Pt_paper = -5.77
    
    print("="*100+"\n\t\t\t\t\t\tPt\n"+"="*100)
    
    Potential_file_name = "Pt_u3.eam"
    Element = "Pt"
    Atomic_coordinates_file_name = "dump.Pt_SX_cohesive_energy_l_3.custom.1"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Cohesive_energy_Pt_calculated = (Potential_energy / No_of_atoms)
    cohesive_Pt = [-Cohesive_energy_Pt_paper,-Cohesive_energy_Pt_calculated,-Cohesive_energy_Pt_LAMMPS]
    percent_cohesive_Pt = [-((Cohesive_energy_Pt_paper-Cohesive_energy_Pt_paper)/Cohesive_energy_Pt_paper*100), -((Cohesive_energy_Pt_calculated-Cohesive_energy_Pt_paper)/Cohesive_energy_Pt_paper*100), -((Cohesive_energy_Pt_LAMMPS-Cohesive_energy_Pt_paper)/Cohesive_energy_Pt_paper*100)]
    

    # Plots
    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,cohesive_Ag, label = "Cohesive energy", width = 0.2)
    for i,j in zip(xaxis,cohesive_Ag):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_cohesive_Ag,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(cohesive_Ag[0], label = "Ref paper (2.85 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [0,1.2e-6])
    ax[0].set(ylabel = "Cohesive energy (eV)", ylim = [0,3.3])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Cohesive energy comparison - Ag (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Ag_cohesive_energy_comparison.png",dpi = 100)
    plt.close()
    
    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,cohesive_Al, label = "Cohesive energy", width = 0.2)
    for i,j in zip(xaxis,cohesive_Al):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_cohesive_Al,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(cohesive_Al[0], label = "Ref paper (3.36 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [-0.006,0.0005])
    ax[0].set(ylabel = "Cohesive energy (eV)", ylim = [0,4])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Cohesive energy comparison - Al (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Al_cohesive_energy_comparison.png",dpi = 100)
    plt.close()

    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,cohesive_Pt, label = "Cohesive energy", width = 0.2)
    for i,j in zip(xaxis,cohesive_Pt):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_cohesive_Pt,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(cohesive_Pt[0], label = "Ref paper (5.77 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [-1.5e-8,1.5e-8])
    ax[0].set(ylabel = "Cohesive energy (eV)", ylim = [0,6.5])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Cohesive energy comparison - Pt (EAM)", weight = "bold")
    plt.savefig("./Results/Pt_cohesive_energy_comparison.png",dpi = 100)
    plt.close()


test_Ag_Al_Pt_Cohesive_energy()

