# Testing program for Interatomic force calculation routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R1_Interatomic_force import get_interatomic_force
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick

"""===========================Zero per atom force for Ag, Pt==========================="""


def get_force_routine_inputs(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions


def test_Ag_Pt_zero_force():
    """
        ############################## TEST - R1 -> 3 - Zero per atom force for a stress free homogeneous sample ##############################
        Description:
        ------------
        Test case for verifying "0" resultant per atom force output for all the atoms of Ag & Pt single homogeneous crystals.
        
        Reference Paper : An embedded-atom potential for the Cu–Ag system, P L Williams , Y Mishin and J C Hamilton; Modelling Simul. Mater. Sci. Eng. 14 (2006) 817–833 – Published 30 May 2006
        Reference Paper : Embedded-atom-method functions for the fcc metals Cu, Ag, Au, Ni, Pd, Pt, and their alloys - S. M. Foiles, M. I. Baskes, and M. S. Daw; PHYSICAL REVIEW B, VOLUME 33, NUMBER 12, 15 JUNE 1986
    """
    
    print(test_Ag_Pt_zero_force.__doc__)
    
    
    #########################################################################################################################################################
    #                                                           Ag
    #########################################################################################################################################################

    print("="*100+"\n\t\t\t\t\t\tAg\n"+"="*100)

    Potential_file_name = "Ag.eam.alloy"
    Element = "Ag"
    Atomic_coordinates_file_name = "dump.Ag_SX_homogeneous_stress_free_l_3.custom.2"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)

    #Plots
    DPI = 100
    
    fig, ax = plt.subplots(3,figsize = (60,60))
    ax[0].scatter(range(No_of_atoms),Force_per_atom[:,0], label = "fx", s = 500)
    ax[1].scatter(range(No_of_atoms),Force_per_atom[:,1], label = "fy", c = "red", s = 500)
    ax[2].scatter(range(No_of_atoms),Force_per_atom[:,2], label = "fz", c = "green",s = 500)
    ax[0].grid()
    ax[0].legend()
    ax[0].set(xlabel = "Atom ID", ylabel = "Force (" + r"$\frac{eV}{\AA}$" + ")", title = "Force x component", ylim = [-1e-11,1e-11])
    ax[1].set(xlabel = "Atom ID", ylabel = "Force (" + r"$\frac{eV}{\AA}$" + ")", title = "Force y component", ylim = [-1e-11,1e-11])
    ax[2].set(xlabel = "Atom ID", ylabel = "Force (" + r"$\frac{eV}{\AA}$" + ")", title = "Force z component", ylim = [-1e-11,1e-11])
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.suptitle("Per atom force of homogeneous stress free sample - Ag (EAM/alloy)", weight = "bold")
    plt.subplots_adjust(hspace = 0.5)
    plt.savefig("./Results/Ag_Force_homogeneous.png",dpi = DPI)
    plt.close()
    
    #########################################################################################################################################################
    #                                                           Pt
    #########################################################################################################################################################

    print("="*100+"\n\t\t\t\t\t\tPt\n"+"="*100)
    
    Potential_file_name = "Pt_u3.eam"
    Element = "Pt"
    Atomic_coordinates_file_name = "dump.Pt_SX_homogeneous_stress_free_l_3.custom.2"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    
    fig, ax = plt.subplots(3,figsize = (60,60))
    ax[0].scatter(range(No_of_atoms),Force_per_atom[:,0], label = "fx", s = 500)
    ax[1].scatter(range(No_of_atoms),Force_per_atom[:,1], label = "fy", c = "red", s = 500)
    ax[2].scatter(range(No_of_atoms),Force_per_atom[:,2], label = "fz", c = "green",s = 500)
    ax[0].grid()
    ax[0].legend()
    ax[0].set(xlabel = "Atom ID", ylabel = "Force (" + r"$\frac{eV}{\AA}$" + ")", title = "Force x component", ylim = [-1e-11,1e-11])
    ax[1].set(xlabel = "Atom ID", ylabel = "Force (" + r"$\frac{eV}{\AA}$" + ")", title = "Force y component", ylim = [-1e-11,1e-11])
    ax[2].set(xlabel = "Atom ID", ylabel = "Force (" + r"$\frac{eV}{\AA}$" + ")", title = "Force z component", ylim = [-1e-11,1e-11])
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.suptitle("Per atom force of homogeneous stress free sample - Pt (EAM)", weight = "bold")
    plt.subplots_adjust(hspace = 0.5)
    plt.savefig("./Results/Pt_Force_homogeneous.png",dpi = DPI)
    plt.close()
        
test_Ag_Pt_zero_force()

