# Testing program for Interatomic force calculation routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R1_Interatomic_force import get_interatomic_force
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick

# Unit conversion constants
ev_to_joule = 1.602176565e-19
Angstrom_to_m = 1e-10

"""===========================Surface energies of Ag, Al & Pt==========================="""


def get_force_routine_inputs(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions


def test_Ag_Al_Pt_Surface_energy():
    """
        ############################## TEST - R1 -> 2 - Surface energy (100,110,111) ##############################
        Description:
        ------------
        Calculates the Surface energies (100, 110, 111) of Ag, Al & Pt single crystal and compares it with the values in the reference paper.
        
        Reference Paper : An embedded-atom potential for the Cu–Ag system, P L Williams , Y Mishin and J C Hamilton; Modelling Simul. Mater. Sci. Eng. 14 (2006) 817–833 – Published 30 May 2006
        Reference Paper : Development of an interatomic potential for the Ni-Al system, G.P. Purja Pun and Y. Mishin, Vol. 89, Nos. 34–36, 1–21 December 2009, 3245–3267
        Reference Paper : Embedded-atom-method functions for the fcc metals Cu, Ag, Au, Ni, Pd, Pt, and their alloys - S. M. Foiles, M. I. Baskes, and M. S. Daw; PHYSICAL REVIEW B, VOLUME 33, NUMBER 12, 15 JUNE 1986
    """
    
    print(test_Ag_Al_Pt_Surface_energy.__doc__)
    
    
    xaxis = ["Ref_paper","Computed","LAMMPS"]
    #########################################################################################################################################################
    #                                                           Ag
    #########################################################################################################################################################

    print("="*100+"\n\t\t\t\t\t\tAg\n"+"="*100)
    
    Cohesive_energy = -2.849999974726837
    Surface_100_Ag_LAMMPS = 940.426581209
    Surface_100_Ag_paper = 940
    Surface_110_Ag_LAMMPS = 1016.82280937
    Surface_110_Ag_paper = 1017
    Surface_111_Ag_LAMMPS = 862.213683549
    Surface_111_Ag_paper = 862

    Potential_file_name = "Ag.eam.alloy"
    Element = "Ag"

    """-------------------Surface - 100-------------------"""
    Atomic_coordinates_file_name = "dump.Ag_SX_surface_energy_x_100_l_3.custom.13"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[1,1]*Box_dimensions[2,1]) # 2 * len(y) * len(z)
        
    Surface_100_Ag_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates_file_name = "dump.Ag_SX_surface_energy_y_110_l_3.custom.18"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[0,1]*Box_dimensions[2,1]) # 2 * len(x) * len(z)
        
    Surface_110_Ag_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))

    """-------------------Surface - 111-------------------"""
    Atomic_coordinates_file_name = "dump.Ag_SX_surface_energy_z_111_l_3.custom.83"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[1,1]*Box_dimensions[0,1]) # 2 * len(y) * len(x)    
    
    Surface_111_Ag_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))

    surface_100 = [Surface_100_Ag_paper,Surface_100_Ag_calculated,Surface_100_Ag_LAMMPS]
    percent_surface_100 = [((Surface_100_Ag_paper-Surface_100_Ag_paper)/Surface_100_Ag_paper*100), ((Surface_100_Ag_calculated-Surface_100_Ag_paper)/Surface_100_Ag_paper*100), ((Surface_100_Ag_LAMMPS-Surface_100_Ag_paper)/Surface_100_Ag_paper*100)]

    surface_110 = [Surface_110_Ag_paper,Surface_110_Ag_calculated,Surface_110_Ag_LAMMPS]
    percent_surface_110 = [((Surface_110_Ag_paper-Surface_110_Ag_paper)/Surface_110_Ag_paper*100), ((Surface_110_Ag_calculated-Surface_110_Ag_paper)/Surface_110_Ag_paper*100), ((Surface_110_Ag_LAMMPS-Surface_110_Ag_paper)/Surface_110_Ag_paper*100)]

    surface_111 = [Surface_111_Ag_paper,Surface_111_Ag_calculated,Surface_111_Ag_LAMMPS]
    percent_surface_111 = [((Surface_111_Ag_paper-Surface_111_Ag_paper)/Surface_111_Ag_paper*100), ((Surface_111_Ag_calculated-Surface_111_Ag_paper)/Surface_111_Ag_paper*100), ((Surface_111_Ag_LAMMPS-Surface_111_Ag_paper)/Surface_111_Ag_paper*100)]

    

    #########################################################################################################################################################
    #                                                           Al
    #########################################################################################################################################################

    print("="*100+"\n\t\t\t\t\t\tAl\n"+"="*100)
    
    Cohesive_energy = -3.3601739311399914
    Surface_100_Al_LAMMPS = 944.780669559
    Surface_100_Al_paper = 943.59
    Surface_110_Al_LAMMPS = 1007.59007455
    Surface_110_Al_paper = 1006.03
    Surface_111_Al_LAMMPS = 872.483081312
    Surface_111_Al_paper =  870.52
        
    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Al"

    """-------------------Surface - 100-------------------"""
    Atomic_coordinates_file_name = "dump.Al_SX_surface_energy_x_100_l_3.custom.12"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[1,1]*Box_dimensions[2,1]) # 2 * len(y) * len(z)
        
    Surface_100_Al_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates_file_name = "dump.Al_SX_surface_energy_y_110_l_3.custom.24"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[0,1]*Box_dimensions[2,1]) # 2 * len(x) * len(z)
        
    Surface_110_Al_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))

    """-------------------Surface - 111-------------------"""
    Atomic_coordinates_file_name = "dump.Al_SX_surface_energy_z_111_l_3.custom.81"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[1,1]*Box_dimensions[0,1]) # 2 * len(y) * len(x)    
    
    Surface_111_Al_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))

    surface_100_Al = [Surface_100_Al_paper,Surface_100_Al_calculated,Surface_100_Al_LAMMPS]
    percent_surface_100_Al = [((Surface_100_Al_paper-Surface_100_Al_paper)/Surface_100_Al_paper*100), ((Surface_100_Al_calculated-Surface_100_Al_paper)/Surface_100_Al_paper*100), ((Surface_100_Al_LAMMPS-Surface_100_Al_paper)/Surface_100_Al_paper*100)]

    surface_110_Al = [Surface_110_Al_paper,Surface_110_Al_calculated,Surface_110_Al_LAMMPS]
    percent_surface_110_Al = [((Surface_110_Al_paper-Surface_110_Al_paper)/Surface_110_Al_paper*100), ((Surface_110_Al_calculated-Surface_110_Al_paper)/Surface_110_Al_paper*100), ((Surface_110_Al_LAMMPS-Surface_110_Al_paper)/Surface_110_Al_paper*100)]

    surface_111_Al = [Surface_111_Al_paper,Surface_111_Al_calculated,Surface_111_Al_LAMMPS]
    percent_surface_111_Al = [((Surface_111_Al_paper-Surface_111_Al_paper)/Surface_111_Al_paper*100), ((Surface_111_Al_calculated-Surface_111_Al_paper)/Surface_111_Al_paper*100), ((Surface_111_Al_LAMMPS-Surface_111_Al_paper)/Surface_111_Al_paper*100)]

    
    #########################################################################################################################################################
    #                                                           Pt
    #########################################################################################################################################################

    Cohesive_energy = -5.7700000005952194
    Surface_100_Pt_LAMMPS = 1651.16785714
    Surface_100_Pt_paper = 1650
    Surface_110_Pt_LAMMPS = 1754.20596656
    Surface_110_Pt_paper = 1750
    Surface_111_Pt_LAMMPS = 1442.48449184
    Surface_111_Pt_paper = 1440
    
    print("="*100+"\n\t\t\t\t\t\tPt\n"+"="*100)
    
    Potential_file_name = "Pt_u3.eam"
    Element = "Pt"
    """-------------------Surface - 100-------------------"""
    Atomic_coordinates_file_name = "dump.Pt_SX_surface_energy_x_100_l_3.custom.9"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[1,1]*Box_dimensions[2,1]) # 2 * len(y) * len(z)
        
    Surface_100_Pt_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates_file_name = "dump.Pt_SX_surface_energy_y_110_l_3.custom.16"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[0,1]*Box_dimensions[2,1]) # 2 * len(x) * len(z)
        
    Surface_110_Pt_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))

    """-------------------Surface - 111-------------------"""
    Atomic_coordinates_file_name = "dump.Pt_SX_surface_energy_z_111_l_3.custom.18"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Surface_area = 2 * (Box_dimensions[1,1]*Box_dimensions[0,1]) # 2 * len(y) * len(x)    
    
    Surface_111_Pt_calculated = ((Potential_energy - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))

    surface_100_Pt = [Surface_100_Pt_paper,Surface_100_Pt_calculated,Surface_100_Pt_LAMMPS]
    percent_surface_100_Pt = [((Surface_100_Pt_paper-Surface_100_Pt_paper)/Surface_100_Pt_paper*100), ((Surface_100_Pt_calculated-Surface_100_Pt_paper)/Surface_100_Pt_paper*100), ((Surface_100_Pt_LAMMPS-Surface_100_Pt_paper)/Surface_100_Pt_paper*100)]

    surface_110_Pt = [Surface_110_Pt_paper,Surface_110_Pt_calculated,Surface_110_Pt_LAMMPS]
    percent_surface_110_Pt = [((Surface_110_Pt_paper-Surface_110_Pt_paper)/Surface_110_Pt_paper*100), ((Surface_110_Pt_calculated-Surface_110_Pt_paper)/Surface_110_Pt_paper*100), ((Surface_110_Pt_LAMMPS-Surface_110_Pt_paper)/Surface_110_Pt_paper*100)]

    surface_111_Pt = [Surface_111_Pt_paper,Surface_111_Pt_calculated,Surface_111_Pt_LAMMPS]
    percent_surface_111_Pt = [((Surface_111_Pt_paper-Surface_111_Pt_paper)/Surface_111_Pt_paper*100), ((Surface_111_Pt_calculated-Surface_111_Pt_paper)/Surface_111_Pt_paper*100), ((Surface_111_Pt_LAMMPS-Surface_111_Pt_paper)/Surface_111_Pt_paper*100)]

    #Plots
    DPI = 100
    
    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_100, label = "Surface energy (100)", width = 0.2)
    for i,j in zip(xaxis,surface_100):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_100,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_100[0], label = "Ref paper (940 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [0,0.06])
    ax[0].set(ylabel = "Surface energy (eV)", ylim = [0,1000])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (100) comparison - Ag (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Ag_surface_energy_100_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_110, label = "Surface energy (110)", width = 0.2)
    for i,j in zip(xaxis,surface_110):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_110,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_110[0], label = "Ref paper (1017 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [-0.02,0.01])
    ax[0].set(ylabel = "Surface energy (eV)", ylim = [0,1200])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (110) comparison - Ag (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Ag_surface_energy_110_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_111, label = "Surface energy (111)", width = 0.2)
    for i,j in zip(xaxis,surface_111):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_111,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_111[0], label = "Ref paper (862 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [0,0.03])
    ax[0].set(ylabel = "Surface energy (eV)", ylim = [0,1000])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (111) comparison - Ag (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Ag_surface_energy_111_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_100_Al, label = "Surface energy (100)", width = 0.2)
    for i,j in zip(xaxis,surface_100_Al):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_100_Al,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_100_Al[0], label = "Ref paper (943.59 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [-0.08,0.13])
    ax[0].set(ylabel = "Surface energy (eV)",ylim = [0,1000])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (100) comparison - Al (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Al_surface_energy_100_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_110_Al, label = "Surface energy (110)", width = 0.2)
    for i,j in zip(xaxis,surface_110_Al):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_110_Al,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_110_Al[0], label = "Ref paper (1006.03 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [-0.1,0.18])
    ax[0].set(ylabel = "Surface energy (eV)", ylim = [0,1100])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (110) comparison - Al (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Al_surface_energy_110_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_111_Al, label = "Surface energy (111)", width = 0.2)
    for i,j in zip(xaxis,surface_111_Al):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_111_Al,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_111_Al[0], label = "Ref paper (870.52 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [-0.1,0.25])
    ax[0].set(ylabel = "Surface energy (eV)",ylim = [0,1000])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (111) comparison - Al (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Al_surface_energy_111_comparison.png",dpi = DPI)
    plt.close()

    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_100_Pt, label = "Surface energy (100)", width = 0.2)
    for i,j in zip(xaxis,surface_100_Pt):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_100_Pt,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_100_Pt[0], label = "Ref paper (1650 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [0,0.1])
    ax[0].set(ylabel = "Surface energy (eV)",ylim = [0,1800])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (100) comparison - Pt (EAM/alloy)", weight = "bold")
    plt.savefig("./Results/Pt_surface_energy_100_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_110_Pt, label = "Surface energy (110)", width = 0.2)
    for i,j in zip(xaxis,surface_110_Pt):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_110_Pt,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_110_Pt[0], label = "Ref paper (1750 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [0,0.3])
    ax[0].set(ylabel = "Surface energy (eV)", ylim = [0,1900])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (110) comparison - Pt (EAM)", weight = "bold")
    plt.savefig("./Results/Pt_surface_energy_110_comparison.png",dpi = DPI)
    plt.close()


    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].bar(xaxis,surface_111_Pt, label = "Surface energy (111)", width = 0.2)
    for i,j in zip(xaxis,surface_111_Pt):
        ax[0].annotate(str(j),xy=(i,j))
    ax[1].plot(xaxis,percent_surface_111_Pt,label = "Error (%) w.r.t ref paper", marker = "o", c = "orange", linewidth = 17, markersize = 40)
    ax[0].axhline(surface_111_Pt[0], label = "Ref paper (1440 eV)", linestyle = "--", color = "r", linewidth = 13)
    ax[0].grid()
    ax[0].legend()
    ax[1].set(ylabel = "Error (%)", ylim = [0,0.2])
    ax[0].set(ylabel = "Surface energy (eV)", ylim = [0,1600])
    ax[1].legend(loc = 2)
    ax[1].grid()
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    plt.suptitle("Surface energy (111) comparison - Pt (EAM)", weight = "bold")
    plt.savefig("./Results/Pt_surface_energy_111_comparison.png",dpi = DPI)
    plt.close()


test_Ag_Al_Pt_Surface_energy()

