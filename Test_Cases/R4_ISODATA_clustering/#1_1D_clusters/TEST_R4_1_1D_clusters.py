# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from sklearn.datasets import make_blobs # To generate synthetic data sets


def test_1D_clustered_data():
    """
        ############################## TEST - R4 -> 1 - 1D Clusters ##############################
        Description:
        ------------
        The data points "1"s and "0"s should get clustered separately. Standard deviation threshold = 0.2; Inter-cluster distance threshold = 0.12.
    """
    print(test_1D_clustered_data.__doc__)
    
    No_of_data_points = 300
    x = np.random.randint(0,2,(No_of_data_points,3))
    x[:,1] = 0 # Explicitly making it as only 1D
    x[:,2] = 0 # Explicitly making it as only 1D
    
    Input_data_unstandardized = x
    Inter_cluster_radius_threshold = 0.12
    Standard_deviation_threshold = 0.2
    Max_iterations = 1000
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0],[0,0]])
    Boundary_conditions = ["f","f","f"]
    Initial_no_of_clusters = 2
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iterations = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions,  Initial_no_of_clusters)

    # Plots
    DPI = 100
    
    fig, ax = plt.subplots(2,figsize = (60,60))
    ax[0].scatter(range(len(x)),x[:,0], label = "Original cluster", s = 1000, c = "g")

    x_plot = np.arange(len(x))
    x_plot_1 = x_plot[:len(Clustered_data[0])]
    x_plot_2 = x_plot[len(Clustered_data[0]):]

    x_plots = np.array([x_plot_1,x_plot_2])

    for j,cluster in enumerate(Clustered_data):
        ax[1].scatter(Cluster_indices[j],cluster[:,0],label = "Predicted cluster - {}".format(j), s= 1000)
        
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[0].set(xlabel = "Point ID", ylabel = "Values")
    ax[1].set(xlabel = "Point ID", ylabel = "Values")
    plt.subplots_adjust(hspace = 0.4)
    plt.suptitle("Original clusters vs Predicted clusters --- 1D data", weight = "bold")
    plt.savefig("./Results/test_1D_clustered_data.png", dpi = DPI)
    plt.close()




test_1D_clustered_data()
