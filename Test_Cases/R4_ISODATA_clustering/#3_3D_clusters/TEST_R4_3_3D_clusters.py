# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D



def test_3D_clustered_data():
    """
        ############################## TEST - R4 -> 3 - 3D Clusters ##############################
        Description:
        ------------
        The 3D data points should be clustered accordingly such that it should match the original clusters. 
        Standard deviation threshold = 0.5; Inter-cluster distance threshold = 0.3
    """
    print(test_3D_clustered_data.__doc__)
    
    No_of_data_points = 3000
    No_of_clusters = 8
    x,y = make_blobs(No_of_data_points,3,centers=No_of_clusters, center_box = (-20,20), random_state = 4587)
    
    Input_data_unstandardized = x
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6*Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0],[0,0]])
    Boundary_conditions = ["f","f","f"]
    Initial_no_of_clusters = 2
        
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions,Initial_no_of_clusters)
    
    DPI = 100
    fig = plt.figure(figsize = (40,40))
    ax = fig.add_subplot(111,projection = "3d")

    for i in (np.unique(y)):
        index = np.argwhere(y == i).flatten()
        ax.scatter(x[index,0],x[index,1],x[index,2], label = "Original cluster - {}".format(i), s = 500)
    
    for j,cluster in enumerate(Clustered_data):
        ax.scatter(cluster[:,0],cluster[:,1],cluster[:,2],label = "Predicted cluster - {}".format(j), s= 100)
        
    ax.grid()
    ax.set_title("Original clusters vs Predicted clusters - 3D data; \nPredicted No:{} Thresholds SD : {} IC distance : {} ".format(No_of_clusters,Standard_deviation_threshold, Inter_cluster_radius_threshold), weight = "bold", fontsize = 75)
    ax.set_xlabel("x coordinate", labelpad = 100)
    ax.set_ylabel("y coordinate", labelpad = 100)
    ax.set_zlabel("z coordinate", labelpad = 100)
    plt.savefig("./Results/test_3D_clustered_data.png", dpi = DPI)
    plt.close()




test_3D_clustered_data()
