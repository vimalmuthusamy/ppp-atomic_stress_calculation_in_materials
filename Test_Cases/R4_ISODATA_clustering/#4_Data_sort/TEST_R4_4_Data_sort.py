# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
from R4_ISODATA_clustering import get_cluster_sorted
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from tqdm import tqdm
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D



def test_data_sort():
    """
        ############################## TEST - R4 -> 4 - Cluster data sort function ##############################
        Description:
        ------------
        Data points that belong to two different clusters are mixed between clusters. For a given 
        new centroid to both the clusters, the data points should gets assigned to its nearest 
        centroid's cluster.
    """
    print(test_data_sort.__doc__)
    
    No_of_data_points = 100
    No_of_clusters = 2
    No_of_dimensions = 2
    x,y = make_blobs(No_of_data_points,No_of_dimensions, centers=No_of_clusters, random_state = 100) # y - cluster numbers
    np.random.shuffle(x)
    Cluster = np.array_split(x,No_of_clusters,axis = 0)
    Cluster_unsorted = np.copy(Cluster)
    Mean = np.array([[-5,-5],[5,5]])

    Sorted_cluster = get_cluster_sorted(No_of_clusters, Cluster, Mean)
    
    DPI = 100
    fig, ax = plt.subplots(figsize = (60,60))

    for i,cluster in tqdm(enumerate(Cluster_unsorted)):
        ax.scatter(cluster[:,0],cluster[:,1], label = "Unsorted - {}".format(i), s = 5000)
    
    for j,sort_cluster in tqdm(enumerate(Sorted_cluster)):
        ax.scatter(sort_cluster[:,0],sort_cluster[:,1], label = "Sorted - {}".format(i), s = 1000)
    ax.scatter(Mean[:,0],Mean[:,1], label = "Centroid",c = "r", s = 10000)
    ax.set(xlabel = "x coordinate(" + r"$\AA)$", ylabel = "y coordinate (" + r"$\AA)$")
    ax.set_title("ISODATA test - Cluster data sort function", weight = "bold")
    ax.legend()
    ax.grid()
    plt.savefig("./Results/test_data_sort.png", dpi = DPI)
    plt.close()


test_data_sort()
