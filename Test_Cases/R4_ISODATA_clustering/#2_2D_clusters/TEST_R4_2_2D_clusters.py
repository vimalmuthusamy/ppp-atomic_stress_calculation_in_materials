# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from sklearn.datasets import make_blobs # To generate synthetic data sets
from textwrap import wrap



def test_2D_clustered_data():
    """
        ############################## TEST - R4 -> 2 - 2D Clusters ##############################
        Description:
        ------------
        The data points should be clustered accordingly such that it should match the original clusters. 
        Standard deviation threshold = 0.5; Inter-cluster distance threshold = 0.3
    """
    print(test_2D_clustered_data.__doc__)
    
    No_of_data_points = 10000
    No_of_clusters = 10
    x,y = make_blobs(No_of_data_points,2, centers=No_of_clusters,center_box = (-30,20), random_state = 14654) # y - cluster numbers
    
    Input_data_unstandardized = x
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6*Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0]])
    Boundary_conditions = ["f","f","f"]    
    Initial_no_of_clusters = 2
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions, Initial_no_of_clusters)

    Original_clusters_list = []
    DPI = 100
    fig, ax = plt.subplots(figsize = (60,60))
    for i in (np.unique(y)):
        index = np.argwhere(y == i).flatten()
        ax.scatter(x[index,0],x[index,1], label = "O-{}".format(i), s = 3000, alpha = 0.8)
        Original_clusters_list.append(x[index])
    
    for j,cluster in enumerate(Clustered_data):
        ax.scatter(cluster[:,0],cluster[:,1],label = "Predicted cluster - {}".format(j), s= 500)
    ax.grid()
    # ax.legend()
    ax.set(xlabel = "x coordinate", ylabel = "y coordinate", title = "\n".join(wrap("Original clusters vs Predicted clusters - 2D data; Predicted No:{} Thresholds SD : {} IC distance : {} ".format(len(Clustered_data),Standard_deviation_threshold, Inter_cluster_radius_threshold),60)))
    plt.savefig("./Results/test_2D_clustered_data.png", dpi = DPI)
    plt.close()




test_2D_clustered_data()
