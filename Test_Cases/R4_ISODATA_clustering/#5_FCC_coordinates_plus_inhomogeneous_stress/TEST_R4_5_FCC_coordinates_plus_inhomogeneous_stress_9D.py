# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
from R4_ISODATA_clustering import get_cluster_sorted
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from tqdm import tqdm
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D

def get_fcc_atomic_coordinates(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions

def test_fcc_coordinate_plus_inhomogeneous_stress():
    """
        ############################## TEST - R4 -> 5 - FCC coordinates (3D) + Inhomogeneous stress (6D) ##############################
        Description:
        ------------
        FCC atomic coordinates are used for positional data. All the atoms has “0” stress values in all components except 3 
        atoms which has stress magnitude of 500, 1000, 2000 as their sigma 11. Standard deviation threshold = 0.5, 
        Inter-cluster distance threshold = 0.6 * Standard deviation threshold.
    """
    print(test_fcc_coordinate_plus_inhomogeneous_stress.__doc__)
    
    Atomic_coordinates_file_name = "dump.Al_SX_l_20.custom.0"
    Atomic_cooridnates, No_of_atoms, Box_dimensions, Boundary_conditions = get_fcc_atomic_coordinates(Atomic_coordinates_file_name)
    Stress_data = np.zeros((No_of_atoms,6)) # 6D symmetric zero stress tensor
    Stress_data[0,0:3] = 1000 # Assigning 1000 stress value to all the principal stress components of atom "0"
    Stress_data[20,0:3] = 500 # Assigning 500 stress value to all the principal stress components of atom "20"
    Stress_data[40,0:3] = 2000 # Assigning 2000 stress value to all the principal stress components of atom "40"
    Input_data_unstandardized = np.append(Atomic_cooridnates, Stress_data, axis = 1)
    Standard_deviation_threshold = 0.3
    Inter_cluster_radius_threshold = 0.6 * Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 0
    Initial_no_of_clusters = 2
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions, Initial_no_of_clusters)

    #Plots
    DPI = 100
    fig, ax = plt.subplots(figsize = (60,60))

    for i,cluster in enumerate(Clustered_data):
        if len(cluster) == 1:
            ax.scatter(i,len(cluster), s = 10000, label = "ID - {}; ".format(i) + r"$\sigma_{11} - $" + "{}".format(cluster[0,3]))
            ax.annotate("{}".format(len(cluster)), (i,len(cluster)))
        else:
            ax.scatter(i,len(cluster), s = 2000)
        
    ax.grid()
    ax.legend()
    ax.set(xlabel = "Cluster ID", ylabel = "No of atoms in the cluster")
    ax.set_title("ISODATA test - No of atoms in every cluster", weight = "bold")
    plt.savefig("./Results/test_9D_local_inhomogeneous_stress_num_atoms_in_cluster.png", dpi = DPI)
    plt.close()

test_fcc_coordinate_plus_inhomogeneous_stress()
