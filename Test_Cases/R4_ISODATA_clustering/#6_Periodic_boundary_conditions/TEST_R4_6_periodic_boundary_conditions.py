# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
from R4_ISODATA_clustering import apply_pbc
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
from matplotlib.patches import Rectangle
import sklearn
from tqdm import tqdm
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D

def get_fcc_atomic_coordinates(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions

def test_pbc():
    """
        ############################## TEST - R4 -> 6 - Periodic Boundary Conditions ##############################
        Description:
        ------------
        Testing the periodic boundary conditions function by calculating the distance between the atom at (0,0,0) (corner atom) to all atoms.
        Output -> Atoms at a distance more than half the box dimensions is displaced to the other side of the reference atom thus
        making the reference atom inside the bulk.
    """
    print(test_pbc.__doc__)
    
    Atomic_coordinates_file_name = "dump.Al_SX_l_8.custom.0"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions = get_fcc_atomic_coordinates(Atomic_coordinates_file_name)    
    Box_end_dimensions = np.array([(Box_dimensions[0,1]-Box_dimensions[0,0]),
                                   (Box_dimensions[1,1]-Box_dimensions[1,0]),
                                   (Box_dimensions[2,1]-Box_dimensions[2,0])])
    Reference_atom = Atomic_coordinates[0]
    New_atom_coordinates = np.zeros_like(Atomic_coordinates)
    New_atom_coordinates = apply_pbc((Atomic_coordinates - Reference_atom),Box_end_dimensions,Boundary_conditions) 
    
    #Plots
    DPI = 100
    
    fig, ax = plt.subplots(figsize = (60,60))
    ax.add_patch(Rectangle((Reference_atom[0], Reference_atom[1]), Box_dimensions[0,1], Box_dimensions[1,1], facecolor="grey",  alpha = 0.5, label = "Simulation box"))
    ax.scatter(Atomic_coordinates[:,0], Atomic_coordinates[:,1], label = "Actual coordinates", s = 5000)
    ax.scatter(New_atom_coordinates[:,0], New_atom_coordinates[:,1], label = "Coordinates after PBC", s = 5000)
    ax.scatter(Reference_atom[0],Reference_atom[1], label = "Reference atom", c = "r", s = 10000)
    ax.set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")")
    ax.set_title("ISODATA test - Periodic Boundary Conditions function", weight = "bold")
    ax.legend()
    ax.grid()
    plt.savefig("./Results/test_periodic_boundary_conditions.png", dpi = DPI)
    plt.close()

test_pbc()
