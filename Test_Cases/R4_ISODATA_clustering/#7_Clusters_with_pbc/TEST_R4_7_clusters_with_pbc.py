# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D

def test_clusters_with_pbc():
    """
        ############################## TEST - R4 -> 7 - Clusters with PBC ##############################
        Description:
        ------------
        For the clusters lying close to the boundaries , clusters on the opposite ends should merge if IC
        distance <= IC threshold such that they belong the same cluster. 
        Standard deviation threshold = 0.5; Inter-cluster threshold = 0.8
    """
    print(test_clusters_with_pbc.__doc__)
    
    No_of_dimensions = 2
    Cluster1, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (1,2) ,random_state=7987)
    Cluster2, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (48,2) ,random_state=7987)
    Cluster3, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (0,52) ,random_state=7987)
    Cluster4, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (48,48) ,random_state=7987)
    Cluster5, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (25,2) ,random_state=7987)
    Cluster6, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (25,48) ,random_state=7987)
    Cluster7, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (48,25) ,random_state=7987)
    Cluster8, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (2,25) ,random_state=7987)

    Input_data = np.concatenate((Cluster1,Cluster2,Cluster3,Cluster4,Cluster5,Cluster6,Cluster7,Cluster8), axis = 0)
    Input_data = np.append(Input_data, np.zeros(len(Input_data)).reshape(-1,1), axis = 1)
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6 * Standard_deviation_threshold
    Inter_cluster_radius_threshold = 0.8
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 20
    Box_x_end = 50
    Box_y_end = 50
    Box_z_end = 0
    Box_dimensions = np.array([[0,Box_x_end],[0,Box_y_end],[0,Box_z_end]])
    Initial_no_of_clusters = 2
    Boundary_conditions = [["ff","ff","ff"],["pp","pp","pp"]]

    DPI = 100
    fig, ax = plt.subplots(2,figsize = (60,60))
    for j,boundary_condition in enumerate(Boundary_conditions):
        Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, boundary_condition,Initial_no_of_clusters)

        for i,cluster in enumerate(Clustered_data):
            ax[j].scatter(cluster[:,0], cluster[:,1], label = "Cluster - {}".format(i), s = 2000)
    
    ax[0].grid()
    ax[1].grid()
    ax[0].set(xlabel = "x coordinate ", ylabel = "y coordinate", title = "Free boundary conditions")
    ax[1].set(xlabel = "x coordinate ", ylabel = "y coordinate ", title = "Periodic boundary conditions")
    plt.suptitle("ISODATA test - Clusters with PBC \nThresholds SD : {} IC distance : {} ".format(Standard_deviation_threshold, Inter_cluster_radius_threshold), weight = "bold")
    plt.subplots_adjust(hspace = 0.5)
    plt.savefig("./Results/test_clusters_with_pbc.png", dpi = DPI)
    plt.close()


test_clusters_with_pbc()
