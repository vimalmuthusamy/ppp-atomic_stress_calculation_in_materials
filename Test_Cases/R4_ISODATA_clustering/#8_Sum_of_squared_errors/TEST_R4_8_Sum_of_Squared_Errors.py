# Testing program for ISODATA clustering routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R4_ISODATA_clustering import get_clustered_data
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick
import sklearn
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D

def test_SSE():
    """
        ############################## TEST - R4 -> 8 - Sum of Squared Errors ##############################
        Description:
        ------------
        The Sum of Squared Error (SSE) is the cost function for clustering and it should converge to a 
        minimum value for the better quality clustering of data points. Number of clusters = 8, 
        Standard deviation threshold = 0.1, Intercluster distance threshold = 0.6*Standard deviation 
        threshold. Number of data points = 3000.
    """
    print(test_SSE.__doc__)
    
    No_of_data_points = 3000
    No_of_clusters = 8
    x,y = make_blobs(No_of_data_points,3,centers=No_of_clusters, center_box = (-20,20), random_state = 4587)
    
    Input_data_unstandardized = x
    Standard_deviation_threshold = 0.1
    Inter_cluster_radius_threshold = 0.6*Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0],[0,0]])
    Boundary_conditions = ["f","f","f"]
    Initial_no_of_clusters_list = np.array([3,10,50])
    
    for Initial_no_of_clusters in Initial_no_of_clusters_list:
        fig, ax = plt.subplots(figsize=(60,60))
        Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions,Initial_no_of_clusters)
        ax.plot(range(Iteration), SSE, linewidth = 10)
        ax.set(xlabel = "Iteration", ylabel = "Sum of Squared Error (SSE)")
        ax.set_title("SSE for {} initial no of clusters".format(Initial_no_of_clusters), weight = "bold")
        ax.grid()
        plt.savefig("./Results/Iterations_SSE_initial_no_of_clusters_{}.png".format(Initial_no_of_clusters))
        plt.close()
        
test_SSE()
