# Testing program for GMM stress routine
import numpy as np
import scipy as sp
from scipy.stats import norm
from scipy.stats import multivariate_normal
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R5_Gaussian_mixture_model_stress import get_gmm_fit_probability
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
np.random.seed(141) 
from tqdm import tqdm

def get_multi_variate_distribution_inputs():
    """
        Generates data points for probability distributions
    """
    No_of_clusters = np.random.randint(2,20,1)[0]
    No_of_clusters = 4
    
    Mean = np.random.randint(0,30,No_of_clusters)
    Standard_deviation = np.random.uniform(1,5,No_of_clusters)
    No_of_points = np.random.randint(10,10000,No_of_clusters)
    
    No_of_data = np.sum(No_of_points)
    Clusters = []
    Complete_data_points = np.array([])
    for i in range(No_of_clusters):
        Clusters.append(np.random.normal(Mean[i], Standard_deviation[i], No_of_points[i]))
        Complete_data_points = np.append(Complete_data_points, Clusters[i])
    
    return np.array(Clusters), No_of_data, Complete_data_points

def test_multi_variate_probability_function():
    """
        ############################## TEST - R5 -> 1 - Multi-variate probability distribution ##############################
        Description:
        ------------
        Creates "n" normal unimodal 1D distributions and tests whether the multi_variate 
        distribution probability function returns the multi-modal distribution for equally
        weighted distributions and distributions that are weighted according to the number of
        data points
    """
    print(test_multi_variate_probability_function.__doc__)

    Clusters, No_of_data, All_data_points = get_multi_variate_distribution_inputs()
    No_of_clusters = len(Clusters)
    if len(Clusters[0].shape) == 1: # For 1D clusters 
        No_of_dimensions = 1
    else:
        No_of_dimensions = Clusters[0].shape[1] # From the shape of the 1st clusters, the no of columns represents the no of dimensions         
    Mixing_coefficient = np.zeros(No_of_clusters)
    Cluster_mean = np.zeros((No_of_clusters,No_of_dimensions)) # Mean is a vector of size of dimension
    Cluster_covariance_matrix = np.zeros((No_of_clusters,No_of_dimensions,No_of_dimensions)) # Co-variance matrix of a cluster is a n x n matrix; n-> Dimensions

    for i in tqdm(range(No_of_clusters)):
        Mixing_coefficient[i] = len(Clusters[i]) / No_of_data # Weight of every cluster
        Cluster_mean[i] = np.mean(Clusters[i],axis=0) # Mean of data points in every cluster; axis = 0 -> Mean of every dimension
        Data_to_mean_delta = Clusters[i] - Cluster_mean[i]
        for delta_vector in Data_to_mean_delta:
            Covariance_matrix_data = np.outer(delta_vector,delta_vector) # Tensor product of the (x - mean) vectors
            Cluster_covariance_matrix[i] += Covariance_matrix_data # Summing up all data points outer product of (Data point - Mean)
        Cluster_covariance_matrix[i] = (1/len(Clusters[i])) * Cluster_covariance_matrix[i] # Dividing the sum by No of data points in the cluster

    Total_data_points, GMM_probability = get_gmm_fit_probability(No_of_clusters,Clusters,No_of_dimensions,No_of_data,Mixing_coefficient,Cluster_mean,Cluster_covariance_matrix)

    #Plots
    DPI = 100

    fig, ax = plt.subplots()#figsize = (60,60))
    ax.scatter(All_data_points,GMM_probability, label = "GMM probability", marker = "o", s = 50)
    for i in range(len(Clusters)):
        ax.scatter(Clusters[i],norm.pdf(Clusters[i], np.mean(Clusters[i]), np.std(Clusters[i])), label = "PDF - {}".format(i+1), marker = ",", s = 1)

    ax.set(xlabel = "Data points", ylabel = "Probability")
    ax.set_title("Multi-variate probability for 4 distributions weighted \naccording to the number of data points", weight = "bold")
    ax.legend()
    ax.grid()
    plt.savefig("./Results/test_multi_variate_probability_function_weighted.png", dpi = DPI)
    plt.close()
    
    Mixing_coefficient = np.ones_like(Mixing_coefficient)
    Total_data_points, GMM_probability = get_gmm_fit_probability(No_of_clusters,Clusters,No_of_dimensions,No_of_data,Mixing_coefficient,Cluster_mean,Cluster_covariance_matrix)

    #Plots
    DPI = 100

    fig, ax = plt.subplots()#figsize = (60,60))
    ax.scatter(All_data_points,GMM_probability, label = "GMM probability", marker = "o", s = 50)
    for i in range(len(Clusters)):
        ax.scatter(Clusters[i],norm.pdf(Clusters[i], np.mean(Clusters[i]), np.std(Clusters[i])), label = "PDF - {}".format(i+1), marker = ",", s = 1)
    
    ax.set(xlabel = "Data points", ylabel = "Probability")
    ax.set_title("Multi-variate probability for 4 distributions weighted equally", weight = "bold")
    ax.legend()
    ax.grid()
    plt.savefig("./Results/test_multi_variate_probability_function_weighted_equally.png", dpi = DPI)
    plt.close()

test_multi_variate_probability_function()
        
