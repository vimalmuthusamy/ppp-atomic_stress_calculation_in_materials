# Testing program for GMM stress routine
import numpy as np
import scipy as sp
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R5_Gaussian_mixture_model_stress import get_GMM_probability_stress
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
from tqdm import tqdm
from itertools import product


def get_stress_conditional_distribution_inputs(Distance_threshold):
    """
        Generates atomic coordinates and splits it into appropriate clusters
    """
    x = np.arange(50)
    No_of_clusters = 1
    Coordinates = np.array(list(product(x, repeat = 2))) # Combinations for x & y axis
    Coordinates = np.append(Coordinates, np.zeros((len(Coordinates), 7)), axis = 1) # Adding zeros to all z coordinates & 6 stress components
    Center_atom_coordinate = np.array([25,25,0,0,0,0,0,0,0])
    Distance = np.linalg.norm((Coordinates - Center_atom_coordinate), axis = 1)
    Inhomogeneous_indices = np.where(Distance < Distance_threshold)[0]
    Atoms_with_higher_stress = Coordinates[Inhomogeneous_indices]
    Atoms_with_higher_stress[:,3] = 100 # Applying 100 ev/A^3 uniaxial stress (Sigma 11)
    Coordinates_homogeneous = np.delete(Coordinates,Inhomogeneous_indices, axis = 0) # Removing the atoms within the radius of distance threshold from the center atom
    if len(Coordinates_homogeneous) != 0:
        Clusters = np.array_split(Coordinates_homogeneous, No_of_clusters) # Splitting coordinates into clusters
        Clusters.append(Atoms_with_higher_stress)
    else:
        Clusters = np.array_split(Atoms_with_higher_stress, No_of_clusters)
    No_of_data = len(Coordinates)
    All_data_points = np.append(Coordinates_homogeneous,Atoms_with_higher_stress,axis = 0)
    
    return Clusters, No_of_data, All_data_points

def test_conditional_stress_distribution():
    """
        ############################## TEST - R5 -> 3 - Conditional stress distribution ##############################
        Description:
        ------------
        Test the distribution of stress around the atoms having the peak stress based on conditional probability
    """
    print(test_conditional_stress_distribution.__doc__)

    Distance_thresholds = np.linspace(5,35,4)
    for distance_limit in Distance_thresholds:
        Clusters, No_of_data, All_points = get_stress_conditional_distribution_inputs(distance_limit)
        GMM_probability, GMM_stress = get_GMM_probability_stress(Clusters, No_of_data)
        
        #Plots
        DPI = 100
        
        fig, ax = plt.subplots(2)
        plot1 = ax[0].scatter(All_points[0:,0],All_points[0:,1], c = All_points[0:,3], label = "Before GMM", cmap = "rainbow")
        plot2 = ax[1].scatter(All_points[0:,0],All_points[0:,1], c = GMM_stress[0:,0], label = "After GMM", cmap = "rainbow")
        ax[0].grid()
        ax[1].grid()
        cbar1 = fig.colorbar(plot1, ax = ax[0])
        cbar1.set_label(r"$\sigma_{11} \ (\frac{ev}{\AA^3})$")
        cbar2 = fig.colorbar(plot2, ax = ax[1])
        cbar2.set_label(r"$\sigma_{11} \ (\frac{ev}{\AA^3})$")
        ax[0].set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")")
        ax[0].set_title("Before applying GMM conditional probability")
        ax[1].set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")")
        ax[1].set_title("After applying GMM conditional probability")
        plt.suptitle("GMM stress distribution (Inhomogeneous radius - {}".format(distance_limit) + r"$\AA)$", weight = "bold")
        plt.subplots_adjust(hspace = 0.7)
        plt.savefig("./Results/test_Conditional_probability_distribution_stress_inhomogeneous_threshold_{}.png".format(distance_limit), dpi = DPI)
        plt.close()
    
test_conditional_stress_distribution()
        
