# Testing program for GMM stress routine
import numpy as np
import scipy as sp
from scipy.stats import norm
from scipy.stats import multivariate_normal
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R5_Gaussian_mixture_model_stress import get_gmm_fit_probability
from R5_Gaussian_mixture_model_stress import get_multivariate_distribution_probability
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
np.random.seed(141) 
from tqdm import tqdm

def test_multi_variate_with_scipy():
    """
        ############################## TEST - R5 -> 2 - Multi-variate probability distribution (Single PDF) ##############################
        Description:
        ------------
        Creates a 1D array and compares the multi-variate probability values with the scipy multi-variate probability outputs
    """
    print(test_multi_variate_with_scipy.__doc__)

    No_of_data_points = 10000
    Mean = 2
    Co_variance = 2
    Data_points = np.random.normal(Mean,Co_variance,No_of_data_points)
    
    Multivariate_probability = []
    for point in Data_points:
        Multivariate_probability.append((get_multivariate_distribution_probability(point, Mean, Co_variance, 1).item()))
    Scipy_probability = multivariate_normal.pdf(Data_points, Mean, Co_variance)
    
    #Plots
    DPI = 100
    
    fig, ax = plt.subplots()
    ax.scatter(Data_points,Multivariate_probability, label = "Multi-variate probability", s = 100)
    ax.scatter(Data_points, Scipy_probability, label = "Scipy probability", s = 10)
    ax.set(xlabel = "Data points", ylabel = "Probability")
    ax.set_title("Multivariate probability computed vs Probability \nfrom Scipy function", weight = "bold")
    ax.legend()
    ax.grid()
    plt.savefig("./Results/test_multi_variate_with_scipy.png", dpi = DPI)
    plt.close()

test_multi_variate_with_scipy()
        
