# Testing program for LAMMPS virial stress routine
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R1_Interatomic_force import get_interatomic_force
from R3_LAMMPS_virial import get_virial_stress_tensor
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
import matplotlib.ticker as mtick

Unit_conv_ke_eV = ((1e-3)*(1e-10)*(1e-10) * 6.241509e18) / ((1e-12) * (1e-12) * (6.02214179 * 1e23)) # 1 J = 6.241509e18 eV; Avogadro constant, 1 mole = 6.02214179 * 1e23 molecules


def get_force_routine_inputs(Atomic_coordinates_file_name):
    """
        Imports atomic configurations from the LAMMPS dump file
    """
    
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    Velocity_vector = np.genfromtxt(Atomic_coordinates_file_name, usecols = (4,5,6), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format  
    
    return Atomic_coordinates, No_of_atoms,Box_dimensions, Boundary_conditions, Velocity_vector

#########################################################################################################################################################
#                                                           Ag
#########################################################################################################################################################

def test_Ag_uniaxial():
    """
        ############################## TEST - R3 -> 3 - Homogeneous sample under uniaxial tension with periodic boundary conditions ##############################
        Description:
        ------------
        Calculates the LAMMPS virial stress components for an Ag homogeneous sample under 0.15% uniaxial tension with periodic boundary conditions in all directions
        
        Reference Paper : An embedded-atom potential for the Cu–Ag system, P L Williams , Y Mishin and J C Hamilton; Modelling Simul. Mater. Sci. Eng. 14 (2006) 817–833 – Published 30 May 2006
    """
    
    print(test_Ag_uniaxial.__doc__)
    
    print("="*100+"\n\t\t\t\t\t\tAg\n"+"="*100)
    
    Potential_file_name = "Ag.eam.alloy"
    Element = "Ag"
    Atomic_coordinates_file_name = "dump.Ag_stress_uniaxial_strain_0.0015.custom.10"
    Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions, Velocity_vector = get_force_routine_inputs(Atomic_coordinates_file_name)
    Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
    Atomic_mass_ke = Atomic_mass * Unit_conv_ke_eV # Since atomic mass is involved in kinetic contribution, the unit conversions for correct energy units is embedded into the atomic mass
    Virial_volume_per_atom = np.ones(No_of_atoms) # Since LAMMPS stress output is not divided by volume !
    
    Velocity_tensor, Force_radius_tensor, Virial_stress_tensor, Symmetric_virial_stress_tensor = get_virial_stress_tensor(No_of_atoms, Total_atomic_coordinates, Force_vector, Atomic_mass_ke, Velocity_vector, Virial_volume_per_atom, Neighbors_list)
    
    #Plots
    DPI = 100

    ev_ang3_to_bar = 160.21766208 * 1e4 # bar
    Stress_lammps = np.genfromtxt(Atomic_coordinates_file_name, usecols = (10,11,12,13,14,15), skip_header = 9)
    Stress_lammps = Stress_lammps / ev_ang3_to_bar

    fig, ax = plt.subplots(3,2,figsize = (60,60))
    ax[0,0].scatter(range(No_of_atoms),Stress_lammps[:,0], label = r"$\sigma_{11}-LAMMPS$", s = 2000)
    ax[0,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[:,0], label = r"$\sigma_{11}-Computed$", s = 800)
    ax[1,0].scatter(range(No_of_atoms),Stress_lammps[:,1], label = r"$\sigma_{22}-LAMMPS$", s = 2000)
    ax[1,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[:,1], label = r"$\sigma_{22}-Computed$", s = 800)
    ax[2,0].scatter(range(No_of_atoms),Stress_lammps[:,2], label = r"$\sigma_{33}-LAMMPS$", s = 2000)
    ax[2,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[:,2], label = r"$\sigma_{33}-Computed$", s = 800)
    ax[0,1].scatter(range(No_of_atoms),Stress_lammps[:,3], label = r"$\sigma_{12}-LAMMPS$", s = 2000)
    ax[0,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[:,3], label = r"$\sigma_{12}-Computed$", s = 800)
    ax[1,1].scatter(range(No_of_atoms),Stress_lammps[:,4], label = r"$\sigma_{13}-LAMMPS$", s = 2000)
    ax[1,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[:,4], label = r"$\sigma_{13}-Computed$", s = 800)
    ax[2,1].scatter(range(No_of_atoms),Stress_lammps[:,5], label = r"$\sigma_{23}-LAMMPS$", s = 2000)
    ax[2,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[:,5], label = r"$\sigma_{23}-Computed$", s = 800)
    ax[0,0].grid()
    ax[0,0].legend()
    ax[1,0].grid()
    ax[1,0].legend()
    ax[0,1].grid()
    ax[0,1].legend()
    ax[2,0].grid()
    ax[2,0].legend()
    ax[1,1].grid()
    ax[1,1].legend()
    ax[2,1].grid()
    ax[2,1].legend()
    ax[0,0].set(xlabel = "Atom ID", ylabel = "Stress * Volume (eV)", title = "Stress component - " + r"$\sigma_{11}$")
    ax[1,0].set(xlabel = "Atom ID", ylabel = "Stress * Volume (eV)", title = "Stress component - " + r"$\sigma_{22}$")
    ax[2,0].set(xlabel = "Atom ID", ylabel = "Stress * Volume (eV)", title = "Stress component - " + r"$\sigma_{33}$")
    ax[0,1].set(xlabel = "Atom ID", ylabel = "Stress * Volume (eV)", title = "Stress component - " + r"$\sigma_{12}$")
    ax[1,1].set(xlabel = "Atom ID", ylabel = "Stress * Volume (eV)", title = "Stress component - " + r"$\sigma_{13}$")
    ax[2,1].set(xlabel = "Atom ID", ylabel = "Stress * Volume (eV)", title = "Stress component - " + r"$\sigma_{23}$")
    plt.suptitle("Ag - Homogeneous sample under 0.15% uniaxial \ntension (LAMMPS Virial)", weight = "bold")
    plt.subplots_adjust(hspace = 0.5, wspace = 0.5)
    plt.savefig("./Results/test_Virial_stress_homogeneous_0.15_percent_uniaxial_tension.png", dpi = DPI)
    plt.close()
    
    percent_11 = (Symmetric_virial_stress_tensor[:,0] - Stress_lammps[:,0])/Stress_lammps[:,0]*100
    percent_22 = (Symmetric_virial_stress_tensor[:,1] - Stress_lammps[:,1])/Stress_lammps[:,1]*100
    percent_33 = (Symmetric_virial_stress_tensor[:,2] - Stress_lammps[:,2])/Stress_lammps[:,2]*100

    fig, ax = plt.subplots(3,figsize = (60,60))
    ax[0].plot(range(No_of_atoms),percent_11, marker = "o", markersize = 20)
    ax[1].plot(range(No_of_atoms),percent_22, marker = "o", markersize = 20)
    ax[2].plot(range(No_of_atoms),percent_33, marker = "o", markersize = 20)
    ax[0].grid()
    ax[1].grid()
    ax[2].grid()
    ax[0].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax[1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax[2].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax[0].set(xlabel = "Atom ID", ylabel = "Error (%)", title = "Stress component - " + r"$\sigma_{11}$")
    ax[1].set(xlabel = "Atom ID", ylabel = "Error (%)", title = "Stress component - " + r"$\sigma_{22}$")
    ax[2].set(xlabel = "Atom ID", ylabel = "Error (%)", title = "Stress component - " + r"$\sigma_{33}$")
    plt.subplots_adjust(hspace = 0.5, wspace = 0.5)
    plt.suptitle("Ag - Homogeneous sample under 0.15% uniaxial \ntension (LAMMPS Virial) - Error (%)", weight = "bold")
    plt.savefig("./Results/test_Virial_stress_homogeneous_0.15_percent_uniaxial_tension_error.png", dpi = DPI)
    plt.close()


test_Ag_uniaxial()

