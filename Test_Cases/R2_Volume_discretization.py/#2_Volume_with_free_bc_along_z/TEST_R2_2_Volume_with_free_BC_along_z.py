# Testing program for Volume discretization
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R2_Volume_discretization import get_voronoi_tessellation
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
from tqdm import tqdm
from mpl_toolkits.mplot3d import Axes3D

"""===========================Voronoi volume - Free boundary condition along z==========================="""
    
def test_3d_volume_equal_distant_points_x_y_pbc_tessellation():
    """
        ############################## TEST - R2 -> 1 - Voronoi volume - PBC - Equidistant points ##############################
        Description:
        ------------        
        Calculates the volume per point for a regularly spaced (equal distance between points in x,y & z) 3D points and the box is set to be periodic
        along x & y directions
        
        OBJECTIVE: 
        1) Volume for points at the boundary along the z direction should be equal to half the volume of the interior points
    """
    print(test_3d_volume_equal_distant_points_x_y_pbc_tessellation.__doc__)
    
    Distance_between_points_x = 1
    Distance_between_points_y = 1
    Distance_between_points_z = 1
    
    Box_x = [0,10]
    Box_y = [0,10]
    Box_z = [0,(10 + 1e-15)] # Box dimension along z is added with the small value 1e-15 to have equal contribution to the pointw at the bottom & top of the box along z; Error results when the final z dimension lies on the boundary
    Boundary_conditions = ["pp","pp","f"]
    
    x = np.arange(Box_x[0],(Box_x[1]),Distance_between_points_x)
    y = np.arange(Box_y[0],(Box_y[1]),Distance_between_points_y)
    z = np.arange(Box_z[0],(Box_z[1]),Distance_between_points_z) # Box end dimension is changed due to non-pbc in z
    
    No_of_points = len(x) * len(y) * len(z)
    x_coordinates, y_coordinates, z_coordinates = np.meshgrid(x,y,z)
    x_coordinates, y_coordinates, z_coordinates = x_coordinates.flatten(), y_coordinates.flatten(), z_coordinates.flatten()
    Box_dimensions = np.array([Box_x,Box_y,Box_z])
    
    Coordinates = np.zeros((No_of_points,3))
    
    for i in range(No_of_points):
        Coordinates[i] = np.array([x_coordinates[i],y_coordinates[i],z_coordinates[i]])
    
    Volume_per_point, Face_areas_per_point, Neighbors_per_point, Vertices_per_face = get_voronoi_tessellation(Coordinates,Box_dimensions, Boundary_conditions)       
    DPI = 100
    fig, ax = plt.subplots(figsize = (40,20))
    vol_plot = ax.scatter(Coordinates[0:,0],Coordinates[0:,2], c = Volume_per_point, s = 500, cmap = plt.cm.get_cmap("rainbow"))
    cbar = plt.colorbar(vol_plot)
    cbar.set_label("Volume per point ("+r"$\AA^3$"+")", fontsize = 30)
    ax.grid()
    ax.set_title("Free boundary condition along z", fontsize = 40, fontweight = "bold")
    ax.set_ylabel("z coordinate" + r"($\AA$)", fontsize = 30)
    ax.set_xlabel("x coordinate" + r"($\AA$)", fontsize = 30)
    cbar.ax.tick_params(labelsize = 20)
    ax.set(xticks = x, yticks = z)
    plt.tick_params(labelsize = 20)
    plt.savefig("./Results/test_3d_volume_equal_distant_points_x_y_pbc_tessellation.png", dpi = DPI)
    plt.close()
    
test_3d_volume_equal_distant_points_x_y_pbc_tessellation()

