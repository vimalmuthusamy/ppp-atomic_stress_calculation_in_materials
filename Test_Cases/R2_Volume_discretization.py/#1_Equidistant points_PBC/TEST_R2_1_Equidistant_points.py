# Testing program for Volume discretization
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../../..") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from R2_Volume_discretization import get_voronoi_tessellation
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)
plt.rcParams.update({"font.size":90})#, "font.weight":"bold"}) # Changes the size of the entire plot
from tqdm import tqdm
from mpl_toolkits.mplot3d import Axes3D

"""===========================Voronoi volume - Equidistant point==========================="""

def test_3d_volume_equal_distant_points_all_pbc_tessellation():
    """
        ############################## TEST - R2 -> 1 - Voronoi volume - PBC - Equidistant points ##############################
        Description:
        ------------      
        Calculates the volume per point for a regularly spaced (equal distance between points in x,y & z) 3D points and the box is set to be periodic
        
        OBJECTIVE: 
        1) All the volume per point values should be same
        2) Computed volume per point should be equal to the analytical cuboid volume per point
    """
    print(test_3d_volume_equal_distant_points_all_pbc_tessellation.__doc__)
    
    Box_x = [0.0,3.0]
    Box_y = [0.0,3.0]
    Box_z = [0.0,3.0]
    Distance_between_points_x = 1
    Distance_between_points_y = 1
    Distance_between_points_z = 1
    Boundary_conditions = ["pp","pp","pp"]
    
    x = np.arange(Box_x[0],(Box_x[1]),Distance_between_points_x)
    y = np.arange(Box_y[0],(Box_y[1]),Distance_between_points_y)
    z = np.arange(Box_z[0],(Box_z[1]),Distance_between_points_z)
    
    Analytical_volume_per_point = Distance_between_points_x * Distance_between_points_y * Distance_between_points_z
    
    No_of_points = len(x) * len(y) * len(z)
    x_coordinates, y_coordinates, z_coordinates = np.meshgrid(x,y,z)
    x_coordinates, y_coordinates, z_coordinates = x_coordinates.flatten(), y_coordinates.flatten(), z_coordinates.flatten()
    
    Box_dimensions = np.array([Box_x,Box_y,Box_z])
    Coordinates = np.zeros((No_of_points,3))
    
    for i in range(No_of_points):
        Coordinates[i] = np.array([x_coordinates[i],y_coordinates[i],z_coordinates[i]])

    Volume_per_point, Face_areas_per_point, Neighbors_per_point, Vertices_per_face = get_voronoi_tessellation(Coordinates,Box_dimensions, Boundary_conditions)
    
    # Plots
    DPI = 100
    
    fig, ax = plt.subplots(figsize = (60,60))
    ax.axhline(Analytical_volume_per_point, c = "green", linewidth = 20, label = "Analytical volume per point")
    ax.scatter(range(No_of_points), Volume_per_point, label = "Computed volume per point", s = 2500, c = "red")
    ax.grid()
    ax.legend()
    ax.set(xlabel = "Point ID",ylabel = "Volume ("+r"$\AA)$")
    ax.set_title("Voronoi volume per point - PBC - Equidistant point", weight = "bold")
    plt.savefig("./Results/test_voronoi_volume_equidistant_points.png", dpi = DPI)
    plt.close()
    
test_3d_volume_equal_distant_points_all_pbc_tessellation()

