# coding: utf-8
# Program to calculate the Interatomic force from the atomic coordinates & EAM potential file
"""
LAMMPS "metal" units are followed
mass = grams / mole
distance = Angstroms
time = picoseconds
energy = eV
force = eV / Angstrom
velocity = Angstrom / picosecond
electron density -> Arbitrary unit
volume = Angstrom^3
stress = ev / Angstrom^3
"""


####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Murray S Daw and Michael I Baskes. Embedded-atom method: Derivation and application to impurities, surfaces, and other defects in metals. 
#    Physical Review B, 29(12):6443, 1984.
# 2) H Bekker, EJ Dijkstra, MKR Renardus, and HJC Berendsen. An efficient, box shape independent non-bonded force and virial algorithm for molecular dynamics. 
#    Molecular Simulation, 14(3):137–151, 1995.
# 3) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient
#    numerical computation. Computing in Science & Engineering, 13(2):22–30, 2011.
# 4) Pauli Virtanen, Ralf Gommers, Travis E. Oliphant, Matt Haberland, Tyler Reddy, David Cournapeau, Evgeni Burovski, Pearu Peterson, Warren Weckesser, 
#    Jonathan Bright, Stéfan J. van der Walt, Matthew Page 47Computational Materials Science Journal Club, Summer 2019 Brett, Joshua Wilson, K. Jarrod Millman, 
#    Nikolay Mayorov, Andrew R. J. Nelson, Eric Jones, Robert Kern, Eric Larson, CJ Carey, İlhan Polat, Yu Feng, Eric W. Moore, Jake Vand erPlas, Denis Laxalde, Josef
#    Perktold, Robert Cimrman, Ian Henriksen, E. A. Quintero, Charles R Harris, Anne M. Archibald, Antônio H. Ribeiro, Fabian Pedregosa, Paul van Mulbregt, and 
#    SciPy 1. 0 Contributors. SciPy 1.0: Fundamental Algorithms for Scientific Computing in Python. Nature Methods, 17:261–272, 2020.
####################################################################################################################################################################

import numpy as np
import matplotlib.pyplot as plt # To generate EAM potential plots
import scipy.interpolate as sp # To interpolate potential derivative functions
from itertools import product # To generate combinations for box translation vector
from tqdm import tqdm
import sys

Batch_limit_no_of_atoms = 10000 # For numpy array size problems for datatype float64

"""
=================================================================================================================================================================================================
                                                Translation vectors for the image boxes & Applying periodic boundary conditions
=================================================================================================================================================================================================
"""
    
def get_local_plus_image_atom_coordinates(Atomic_coordinates,Boundary_conditions,Box_dimensions):
    """
        Description:
        ==========
        Creates 26 image boxes around the local simulation box for the periodic images of local atoms (images) applying the translation
        vectors to the local atomic coordinates
        
        Input:
        =====
        1) Atomic_coordinates -> Array size (No of atoms, 3)
        2) Boundary_conditions -> Array size (1,3) - pp = periodic; else = non-periodic
        3) Box_dimensions -> Array size (3,2) - Box start & end dimensions along the 3 directions
        
        Output:
        ======
        1) Atomic_coordinates -> Array size (Total no of atoms (Local + Image), 3)
    """
    
    PBC_index = np.where(Boundary_conditions == "pp")[0] # Finds the directions along which periodic boundary condition is applied; [0] -> Only the 1st array is required; "pp" -> Periodic as per LAMMPS
            
    if len(PBC_index) == 0: # Free boundary conditions in all directions
        return Atomic_coordinates, np.array([0,0,0]) # Returning only the central box coordinates if no periodic boundary exists in all 3 directions            

    else: # At-least one direction is periodic
        Combinations = np.array(list(product([0,1,-1], repeat = (len(PBC_index))))) # Creating different combinations of the 3 basis directions (0,-1,1)
        Translation_vector = np.zeros((len(Combinations),3), dtype = float) # Initializing the size of the translation vector; dtype = float -> Explicit due to "type cast" error during multiplying with float box dimensions
        Translation_vector[0:,PBC_index] = Combinations # Assigning the basis direction combinations along the periodic axis
        
        Shift_box_dimensions = np.array([(Box_dimensions[0][1] - Box_dimensions[0][0]),(Box_dimensions[1][1] - Box_dimensions[1][0]),(Box_dimensions[2][1] - Box_dimensions[2][0])]) # Calculating actual box shift values rather than just using the box end dimensions
        Translation_vector_scaled = Translation_vector * Shift_box_dimensions # Finding the scaled translation vectors according to the current simulation box dimensions
        
        # Appending image atom coordinates to the current atoms ------ "Total" -> Variables holding this name includes central box atoms properties + Image atoms properties
        Total_atomic_coordinates = np.copy(Atomic_coordinates) # Copying the central box coordinates
        
        for vector in Translation_vector_scaled[1:]: # Avoiding the first vector since the central box coordinates was already copied
            Total_atomic_coordinates = np.append(Total_atomic_coordinates,(Atomic_coordinates + vector), axis = 0) # Appending the coordinates of every image box (Central box coordinates shifted by the translation vector)
    
    return Total_atomic_coordinates, Translation_vector


"""
=================================================================================================================================================================================================
                                                                                Image atom neighbor calculation
=================================================================================================================================================================================================
"""

def get_image_neighbors(Neighbors_list_equal_size, No_of_atoms, Initial_box_numbers, Translation_vector, Box_translation_vector):
    """
        Description:
        ===========
        Calculates the neighbors of image atoms of the periodic box based on the neighbors of the local atom using translation vector and box numbers
        
        Input:
        1) Neighbors_list_equal_size = Equal sized array of neighbors of all local atoms; Array size -> (No of local atoms, No of neighbors)
        2) No_of_atoms = No of local atoms; Array size -> Scalar
        3) Initial_box_numbers = Box numbers of all periodic boxes of all local atoms; Array size -> (No of local atoms, No of box number based on local atom neighbors, 1)
        4) Translation_vector = Translation vectors (only the unit values) of the 26 boxes; Array size -> (26, 3) 
        5) Box_translation_vector = Translation vectors of the periodic boxes for which the neighbors to be calculated; Array size -> (1,3)
        
        Output:
        ======
        1) Neighbors of all atoms the given periodic box; Array size -> (No of image atoms,No of neighbors) 
    """
    
    Sign_factor = np.sign(Initial_box_numbers)
    Sign_factor[Sign_factor<0] = -10
    New_translation_vector = (Translation_vector[Initial_box_numbers].reshape(-1,3)) * Sign_factor.reshape(-1,1) + Box_translation_vector
    Sign_array = np.sign(Neighbors_list_equal_size)
    Sign_array[Sign_array >= 0] = 1
    return (((Neighbors_list_equal_size)%No_of_atoms * Sign_array) + (No_of_atoms * (np.array([np.argwhere((vector == Translation_vector).all(axis=1)) if len(np.argwhere((vector == Translation_vector).all(axis=1))) !=0 else np.array([[-2]]) for vector in New_translation_vector]).flatten())).reshape((np.array(Neighbors_list_equal_size)%No_of_atoms).shape)) # "-1" is used for the case of image atoms that lies at the boundaries of the image + local boxes which will not have neighbors all around
    

"""
=================================================================================================================================================================================================
                                                                                Potential energy calculation
=================================================================================================================================================================================================
"""

def get_potential_energy(Radius_x_axis,Electron_density_function,Electron_density,Embedding_function,Pair_potential_function, Radius_neighbors):
    """
        Description:
        ===========
        Calculates the potential energy of the system using the Electron density function, embedding function 
        and Pair potential function of EAM potential formulae
        
        Input:
        =====
        1) Radius_x_axis = Radius data imported from the potential file; Array size -> (1,No of radius data)
        2) Electron_density_function = Function values imported from the potential file; Array size -> Array size -> (1,No of radius data)
        3) Electron_density = Function values imported from the potential file; Array size -> Array size -> (1,No of electron density data)
        4) Embedding_function = Function values imported from the potential file; Array size -> Array size -> (1,No of electron density data)
        5) Pair_potential_function = Function values imported from the potential file; Array size -> Array size -> (1,No of radius data)
        6) Radius_neighbors = Interatomic radius of all local atoms; Array size -> (No of local atoms, (No of local atoms + No of image atoms))
        
        Output:
        ======
        1) Potential_energy = Potential energy of the system
        2) rho_electron_density = Electron density around each local atom; Array size -> (1, No of local atoms)
        3) Potential_energy_per_atom = Potential energy contribution of each local atom; Array size -> (1, No of local atoms)
    """
    rho_electron_density, Pair_energy = np.zeros(len(Radius_neighbors)), np.zeros(len(Radius_neighbors))
    
    for index,Radius in tqdm(enumerate(Radius_neighbors)):
        rho_electron_density[index] = np.sum(sp.CubicSpline(Radius_x_axis, Electron_density_function)(Radius)) # sp.cubicspline(x,y)(value) -> Cubic spline interpolation, Electron density around each atom
        Pair_energy[index] = np.sum(sp.CubicSpline(Radius_x_axis, Pair_potential_function)(Radius)) # Pair energy per atom
    G_embedding_energy = sp.CubicSpline(Electron_density, Embedding_function)(rho_electron_density) # Embedding energy per atom
    Potential_energy_per_atom = (0.5 * Pair_energy) + G_embedding_energy # Potential energy per atom
    Potential_energy = np.sum(Potential_energy_per_atom) # Potential energy of the system

    return Potential_energy, rho_electron_density, Potential_energy_per_atom

"""
=================================================================================================================================================================================================
                                                                                Interatomic Force calculation
=================================================================================================================================================================================================
"""
    
def get_force_vector_per_atom(No_of_atoms,Total_no_of_atoms,Radius_x_axis,Pair_potential_interpol_derivative,Electron_density,
                                Embedding_function_interpol_derivative,rho_electron_density,Electron_density_interpol_derivative, Neighbors, Total_atomic_coordinates, Radius):
    """
        Description:
        ===========
        Calculates the interatomic force between atoms using the EAM potential formula and returns the resultant Force vector per atom and 
        the force vector components of every atom interactions with all the other atoms
        
        Input:
        =====
        1) No_of_atoms = No of local atoms
        2) Total_no_of_atoms = No of local atoms + No of image atoms in the case of periodic boundary conditions else No of local atoms
        3) Radius_x_axis = Radius data imported from the potential file; Array size -> (1,No of radius data)
        4) Pair_potential_derivative = Differentiated pair potential function values imported from the potential file; Array size -> (1,No of radius data)
        5) Electron_density = Function values imported from the potential file; Array size -> Array size -> (1,No of electron density data)
        6) Embedding_function_derivative = Differentiated embedding function function values imported from the potential file; Array size -> (1,No of electron density data)
        7) rho_electron_density = Electron density around each local atom; Array size -> (1, No of local atoms)
        8) Electron_density_derivative = Differentiated embedding function function values imported from the potential file; Array size -> (1,No of radius data)
        9) Neighbors = Neighbor list for every local atom; Array size -> (No of atoms, depends on the no of neighbors)
        
        Output:
        ======
        1) Force_vector = Pairwise interactions of all local atoms with all atoms (local + image); Array size -> (3, No of atoms, No of neighbors) 
        2) Force_per_atom = Resultant force acting on all local atoms; Array size -> (No of atoms, 3)
    """
    
    Force_x = np.zeros_like(Neighbors[:No_of_atoms], dtype = float) # Array size also includes the image atoms
    Force_y = np.zeros_like(Neighbors[:No_of_atoms], dtype = float) # Force interactions for all atoms (including image atoms) are required for virial stress calculations
    Force_z = np.zeros_like(Neighbors[:No_of_atoms], dtype = float)

    for i in tqdm(range(No_of_atoms)): # No of atoms since only the central particle force evaluations are required
        rij = Radius[i] # Radius array has size (No of atoms, Total no of atoms) -> So i -> No of atoms, j -> Total no of atoms
        Neighbor_per_atom = Neighbors[i][Neighbors[i] >= 0]
        Pair_electron_density_index = Neighbor_per_atom % No_of_atoms # This will return the indices of central atoms that are linked to the image atoms. Only image atoms indices will be changed to their central atom not the central atoms in the neighbor list. % -> Returns the reminder of the division. np.array() -> Due to casting error btw 'list' & 'int'            

        Distance = Total_atomic_coordinates[i] - Total_atomic_coordinates[Neighbor_per_atom]

        Force_scalar_array = - (Pair_potential_interpol_derivative(rij) + (Embedding_function_interpol_derivative(rho_electron_density[i]) + Embedding_function_interpol_derivative(rho_electron_density[Pair_electron_density_index])) * Electron_density_interpol_derivative(rij))

        Force_x[i][:len(Neighbor_per_atom)] = Force_scalar_array * ((Distance[:,0]) / rij) # Since no "0" radius wil be encountered in divide, np.divide is unnecessary
        
        Force_y[i][:len(Neighbor_per_atom)] = Force_scalar_array * ((Distance[:,1]) / rij) # Force_y[i][:len(Neighbor_per_atom)] -> To avoid the negative indices
        
        Force_z[i][:len(Neighbor_per_atom)] = Force_scalar_array * ((Distance[:,2]) / rij)
    
    Force_vector = np.array([Force_x,Force_y,Force_z]) # Combining all the force vector for interactions of every atom with all atoms into an array -> Array size is (Total no of atoms, Total no of atoms)
    Force_per_atom = np.array([(np.sum(Force_x, axis = 1)).flatten(), (np.sum(Force_y, axis = 1)).flatten(), (np.sum(Force_z, axis = 1)).flatten()]).T # 2D array. 1D -> Each atom, 2D -> x,y,z components of the force vector

    return Force_vector, Force_per_atom


def get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions):
    """
        Description:
        ===========
        Function to calculate the Interatomic force and Potential energy on a per atom basis based on inputs from EAM potential file and atomic coordinates.
        Returns the Resultant Force per atom, Force between atoms & its neighbors, Interatomic radius vector between all atom interactions, Potential energy per atom, 
        Atomic mass, Cut off radius.
        
        Input:
        =====
        1) Potential_file_name = Name of the EAM or EAM/ALLOY potential file; dtype = str;  NOTE: EAM potential file should be in DYNAMO funcfl format for EAM or setfl format for EAM/ALLOY files
        2) Element = Symbol of the element; dtype = str
        3) Atomic_coordinates = Coordinates of the atoms imported from LAMMPS dump file;  Array size -> (No of atoms, 3)
        4) No_of_atoms = No of atoms in the simulation box
        5) Box_dimensions = Box start & end dimensions in all 3 directions; Array size -> (3,2)
        6) Boundary_conditions = Boundary conditions imported from the LAMMPS dump file; Array size -> (1,3)
        
        Output:
        ======
        1) Lattice_constant = Imported from the potential file for the given element
        2) Atomic_mass = Imported from the potential file for the given element
        3) Cut_off = Imported from the potential file
        4) Force_vector = Pairwise interactions of all local atoms with all atoms (local + image); Array size -> (3,No of atoms, No of neighbors) 
        5) Force_per_atom = Resultant force acting on all local atoms; Array size -> (No of atoms, 3)
        6) Potential_energy_system = Potential energy for the entire system
        7) Potential_plot = Plot consisting of Embeding energy function, Electron density function and Pair potential function
        8) Neighbors = Neighbor list for every local atom; Array size -> (No of atoms, depends on the no of neighbors)
        9) Total_atomic_coordinates = Local atoms + Image atoms coordinates; Array size -> (Total no of atoms, 3)
    """
    # Unit conversions
    Hartree_to_eV = 27.2 # unit conversion constant
    Bohr_to_Angstroms = 0.529 # unit conversion constant
    
    if (Potential_file_name.split(".")[-1] == "eam"): # Check whether the given potential is "eam" -> Single element DYNAMO  funcfl file format
        
        """
        =================================================================================================================================================================================================
                                                                                        EAM single element (funcfl) file
        =================================================================================================================================================================================================
        """
        
        Atomic_parameters = np.genfromtxt(Potential_file_name, comments = "#",skip_header = 1, max_rows = 1)
        Potential_parameters = np.genfromtxt(Potential_file_name, comments = "#", skip_header = 2, max_rows = 1)
        Atomic_number = Atomic_parameters[0]
        Atomic_mass = Atomic_parameters[1]
        Lattice_constant = Atomic_parameters[2]
        No_of_density_values = int(Potential_parameters[0])
        Density_distance = Potential_parameters[1]
        No_of_radius_values = int(Potential_parameters[2])
        Radius_distance = Potential_parameters[3]
        Cut_off = Potential_parameters[4]

        Potential_data = np.genfromtxt(Potential_file_name, comments = "#", skip_header = 3).flatten() # Reading all the function values into an 1D array
        Electron_density = np.arange(No_of_density_values) * Density_distance # Equispaced Electron density x-axis based on dhro
        Radius_x_axis = np.arange(No_of_radius_values) * Radius_distance # Equispaced interatomic radius x-axis based on dr
        Embedding_function = Potential_data[:No_of_density_values] # Slicing the 1D potential data array based on corresponding no of values (Nrho)
        Effective_charge_function = Potential_data[No_of_density_values:(No_of_density_values+No_of_radius_values)] # Slicing the 1D potential data array based on corresponding no of values (Nr)
        Pair_potential_function = (np.divide((Effective_charge_function * Effective_charge_function) , (Radius_x_axis), 
                                            out = np.zeros_like(Radius_x_axis), where = Radius_x_axis != 0)) * (Hartree_to_eV * Bohr_to_Angstroms)
        Electron_density_function = Potential_data[-No_of_radius_values:] # Slicing the 1D potential data array based on corresponding no of values (Nr)
    
    elif (Potential_file_name.split(".")[-2] == "eam") and (Potential_file_name.split(".")[-1] == "alloy"): # Check whether the given potential is "eam.alloy" -> Single element DYNAMO setfl file format
       
        """
        =================================================================================================================================================================================================
                                                                                        EAM / alloy (setfl) file
        =================================================================================================================================================================================================
        """

        Elements_potential_file = np.genfromtxt(Potential_file_name, comments = "#", skip_header = 3, max_rows = 1, dtype = "str")[1:] # [1:] -> Eliminating the no of elements in the first entry;  data type string is specified else "nan" will be returned in element names
        try:
            Element_index = np.where(Elements_potential_file == Element)[0][0] # [1][0] means that np.where returns in the format of row number, column number. Since only one row, we need only the column number 
        except IndexError:
            sys.exit("Error: Element symbol given contradicts with the element in the potential file !")
        
        No_of_elements = len(Elements_potential_file) # No of elements for which the potential value are entered
        Potential_parameters = np.genfromtxt(Potential_file_name, comments = "#", skip_header = 4, max_rows = 1)
        No_of_density_values = int(Potential_parameters[0])
        Density_distance = Potential_parameters[1]
        No_of_radius_values = int(Potential_parameters[2])
        Radius_distance = Potential_parameters[3]
        Cut_off = Potential_parameters[4]
        Lines_to_skip = Element_index * (No_of_density_values + No_of_radius_values + Element_index) + 6 # 6 -> Header lines; If only one element is present, Element_index = 0 & no lines will be skipped. If the required element is present after one element, corresponding no of values are skipped. Addition of Element_index inside paranthesis is to skip the element parameter lines in between
        
        Atomic_parameters = np.genfromtxt(Potential_file_name, comments = "#", skip_header = (Lines_to_skip - 1), max_rows = 1) # Lines to skip -1 -> to read only the atomic parameters based on the element index, since only atomic parameters for subsequent elements are placed in between the element values 
        Atomic_number = Atomic_parameters[0]
        Atomic_mass = Atomic_parameters[1]
        Lattice_constant = Atomic_parameters[2]
        Potential_data = np.genfromtxt(Potential_file_name, comments = "#", skip_header = Lines_to_skip, max_rows= (No_of_density_values + No_of_radius_values)).flatten() # Reading all the function values into an 1D array
        Electron_density = np.arange(No_of_density_values) * Density_distance # Equispaced Electron density x-axis based on dhro
        Radius_x_axis = np.arange(No_of_radius_values) * Radius_distance # Equispaced interatomic radius x-axis based on dr
        Embedding_function = Potential_data[:No_of_density_values] # Slicing the 1D potential data array based on corresponding no of values (Nrho)
        Electron_density_function = Potential_data[No_of_density_values:(No_of_density_values+No_of_radius_values)] # Slicing the 1D potential data array based on corresponding no of values (Nr)
        Lines_to_skip_effective_charge_function = No_of_elements * (No_of_density_values + No_of_radius_values) + No_of_elements + 5 # 1st term -> F & rho values for all elements; 2nd part (No of elements) -> 1 line containing atomic parameters for all elements; 3rd part (5) -> top header lines
        Effective_charge_function_all_elements = np.genfromtxt(Potential_file_name, comments = "#", skip_header = Lines_to_skip_effective_charge_function).flatten() # Reads all the charge function for all the element combinations
        Charge_function_start_index = (np.sum(np.arange(Element_index + 2)) - 1 ) * No_of_radius_values # Effective charge function is arranged in (1,1),(2,1)(2,2),(3,1),(3,2),(3,3),...,(Nelements,Nelements) combinations, in order to take only the single element interactions like (1,1) or (2,2), the relation of (sum(0...No of elements) - 1) -> "-1" is get the starting index for the respective element
        Effective_charge_function = Effective_charge_function_all_elements[Charge_function_start_index:(Charge_function_start_index + No_of_radius_values)] # Slicing the 1D charge function data array based on corresponding no of values (Nr)
        Pair_potential_function = (np.divide((Effective_charge_function), (Radius_x_axis), 
                                            out = np.zeros_like(Radius_x_axis), where = Radius_x_axis != 0)) # Since in the setfl format, function is already in r*phi (eV-Angstroms)        
        
    if Electron_density_function[0] > 1e-15: # If Electron density is not 0 at radius = 0
        Electron_density_function[1:] = Electron_density_function[:-1] # Shifting all the values by 1 index right
        Electron_density_function[0] = 0.0 # At radius = 0 -> Electron density = 0
            
            
    """
    =================================================================================================================================================================================================
                                                                                    Inter-atomic potential plot
    =================================================================================================================================================================================================
    """
        
    fig, Potential_plot = plt.subplots(nrows = 3,figsize=(20,20)) # 3 plots for Electron Density vs Embedding function, Radius vs Electron density, Radius vs Pair potential function 
    Potential_plot[0].plot(Electron_density, Embedding_function, label = "Embedding function")
    Potential_plot[1].plot(Radius_x_axis, Electron_density_function, label = "Electron density function", c = "r")
    Potential_plot[2].plot(Radius_x_axis, Pair_potential_function, label = "Pair potential function",  c = "g")
    Potential_plot[0].set(title = "Electron Density vs Embedding function", xlabel = "Electron density [" + r'$\rho$'"] (1/" + r'$\AA^{3}$'")", ylabel = "Embedding function [F] (eV)")
    Potential_plot[0].grid()
    Potential_plot[0].legend()
    Potential_plot[1].set(title = "Radius vs Electron density function", ylabel = "Electron density [" + r'$\rho$'"]", xlabel = "Radius [r] (" + r'$\AA$'")")
    Potential_plot[1].grid()
    Potential_plot[1].legend()
    Potential_plot[2].set(title = "Radius vs Pair potential function", ylabel = "Pair potential function [" + r'$\phi$'"] (eV)", xlabel = "Radius [r] (" + r'$\AA$'")")
    Potential_plot[2].grid()
    Potential_plot[2].legend()
    plt.subplots_adjust(hspace = 0.3)
    plt.suptitle("EAM potential functions - {}".format(Potential_file_name), fontsize = 20,weight = "bold", color = "red")
    
    """ 
    EAM potential file (funcfl file format) contains values as effective charge function (in sqrt(Hartee * Bohr radius). The above conversion is to get the pair potential using the 
    formula r * phi = 27.2 * 0.529 * Zi * Zj (since here it is only one element, Z^2)
    EAM/Alloy potential has r * phi(r) directly in terms of eV-Angstrom -> So no need to multiply it with unit conversions and itself. Divide only by the radius.
    """
            
    Total_atomic_coordinates, Translation_vector = get_local_plus_image_atom_coordinates(Atomic_coordinates,Boundary_conditions,Box_dimensions) # Applying periodic boundary conditions if applicable
    Total_no_of_atoms = len(Total_atomic_coordinates) # No of atoms in the central box + Image atoms    

    
    """
    =================================================================================================================================================================================================
                                                                Calculating Interatomic distance between all atoms (Local + Image) for force calculations
    =================================================================================================================================================================================================
    """
    
    x = Total_atomic_coordinates[:,0]
    y = Total_atomic_coordinates[:,1]
    z = Total_atomic_coordinates[:,2]
    
    No_of_batches = int(np.ceil(Total_no_of_atoms/Batch_limit_no_of_atoms)) # np.ceil() function rounds to the greatest interger of that number ie) np.ceil(0.3) = 1 -> This function is required to atleast run the distance calculation for loop to run once if the no of atoms is less than 10000
    x_batches = np.array_split(x[:No_of_atoms],No_of_batches)
    y_batches = np.array_split(y[:No_of_atoms],No_of_batches)
    z_batches = np.array_split(z[:No_of_atoms],No_of_batches)
    
    Non_zero_indices_row = np.array([])
    Non_zero_indices_columns = np.array([])
    Radius = [] # List used since the no of non-zero radius may vary between atom to atom
    
    No_of_batch_atoms = 0
    
    """****************Calculating all distances in batches to avoid array size problems************************"""
    
    for batch_index in tqdm(range(No_of_batches)): # Due to limitation in the numpy array size, Local atoms distance calculation is splitted into batches
        Distance_x = x_batches[batch_index][:,np.newaxis] - x # Finding the distance between all atoms with all atoms only in the central box (x[i] - x) -> x[i] - current atom; x - all atoms in the central box
        Distance_y = y_batches[batch_index][:,np.newaxis] - y # Distance array row size is limited to the the central box atoms !
        Distance_z = z_batches[batch_index][:,np.newaxis] - z # using z_batches[][] rather than z_batches[,,] -> Since z_batches will be a list due to split

        """
        =================================================================================================================================================================================================
                                                                                        Applying Cut - off radius
        =================================================================================================================================================================================================
        """
        
        radius = (Distance_x**2 + Distance_y**2 + Distance_z**2) # No significant difference in time if sqrt is taken after applying cut off !
        radius = np.select([radius > Cut_off**2, radius <= Cut_off**2], [0, radius]) # Replacing the radius with "0" if it is greater than the Cut-off radius
        Non_zero_batch = np.nonzero(radius)
        Non_zero_indices_row = np.append(Non_zero_indices_row,(Non_zero_batch[0] + No_of_batch_atoms))
        Non_zero_indices_columns = np.append(Non_zero_indices_columns,Non_zero_batch[1])
        No_of_batch_atoms += len(x_batches[batch_index])

        for radius_non_zero in radius:
            Radius.append(np.sqrt(radius_non_zero[radius_non_zero > 0])) # Taking the radius values of only the neighbors that are within the cut off radius


    Distance_x, Distance_y, Distance_z = None, None, None

    """
    =================================================================================================================================================================================================
                                                                                   Calculating neighbors of each atom
    =================================================================================================================================================================================================
    """
    
    # In neighbor list calculation, the image atoms will have only the central atoms as their neighbors not the atoms in their own boxes due to the way of radius calculated above !
    
    Non_zero_radius_indices = np.array([Non_zero_indices_row,Non_zero_indices_columns]).astype(int) # Taking all the neighbors of local + ghost atoms which is required for bond calculation in Task_6 -> Hardy stress calculation"
    Neighbors_list = [[] for i in range(No_of_atoms)] # Initializing a no of lists equal to the total no of atoms (Central particles + Image atoms) so that it can be appended
    for j in range(len(Non_zero_radius_indices[0])): # No of atoms that has neighbors
        Neighbors_list[Non_zero_radius_indices[0][j]].append(Non_zero_radius_indices[1][j]) # Appending the neighbor of a particular atom in the non-zero indices list;  
    

    No_of_columns = np.zeros(No_of_atoms) # Initializing the no of columns ie) neighbors for each atom
    
    for i, neighbor in enumerate(Neighbors_list):
        No_of_columns[i] = len(neighbor) # Since the neighbor list can be an unequal sized array, "object" data type array is produced -> So, "for" loops
    No_of_columns = No_of_columns.astype(int) # Since they are going to be indices
    
    Neighbors = np.ones((No_of_atoms,np.amax(No_of_columns)), dtype = int) * (-1) # Assigning all the non-contributing neighbors to value "-1"
    for j,neighbor in enumerate(Neighbors_list):
        Neighbors[j,:No_of_columns[j]] = neighbor # Replacing "-1" with the actual neighbor index only for the length of the neighbor

    if No_of_atoms != Total_no_of_atoms:
        Neighbors_image_atoms = [] # List used since it is easier to append
        Initial_box_numbers = np.array(Neighbors) // No_of_atoms # "//" operation returns only the quotient of the division operation which is equal to their box numbers
        for boxes in tqdm(range(len(Translation_vector[1:]))):
            Neighbor_per_box = get_image_neighbors(Neighbors, No_of_atoms, Initial_box_numbers, Translation_vector, Translation_vector[boxes+1])
            Neighbors_image_atoms.append(Neighbor_per_box)

        Neighbors = np.append(Neighbors,np.array(Neighbors_image_atoms).reshape(-1,np.array(Neighbors_image_atoms).shape[2]), axis = 0) # Appending all the neighbors of all periodic images

        
    Potential_energy_system, rho_electron_density, Potential_energy_per_atom = get_potential_energy(Radius_x_axis,Electron_density_function,
                                                                                                    Electron_density,Embedding_function,Pair_potential_function, Radius)    
   
    """
    =================================================================================================================================================================================================
                                                                                   Function differentiation
    =================================================================================================================================================================================================
    """

    Pair_potential_interpol_derivative_cubic = sp.CubicSpline(Radius_x_axis, Pair_potential_function).derivative() # Cubic spline polynomial function & taking its derivative
    Electron_density_interpol_derivative_cubic = sp.CubicSpline(Radius_x_axis, Electron_density_function).derivative()
    Embedding_function_interpol_derivative_cubic = sp.CubicSpline(Electron_density, Embedding_function).derivative()
    
    Pair_potential_interpol = sp.CubicHermiteSpline(Radius_x_axis, Pair_potential_function, Pair_potential_interpol_derivative_cubic(Radius_x_axis))
    Electron_density_interpol = sp.CubicHermiteSpline(Radius_x_axis, Electron_density_function, Electron_density_interpol_derivative_cubic(Radius_x_axis))
    Embedding_function_interpol = sp.CubicHermiteSpline(Electron_density, Embedding_function, Embedding_function_interpol_derivative_cubic(Electron_density))

    Pair_potential_interpol_derivative = Pair_potential_interpol.derivative()
    Electron_density_interpol_derivative = Electron_density_interpol.derivative()
    Embedding_function_interpol_derivative = Embedding_function_interpol.derivative()


    Force_vector, Force_per_atom = get_force_vector_per_atom(No_of_atoms,Total_no_of_atoms,Radius_x_axis,Pair_potential_interpol_derivative,
                                                                              Electron_density,Embedding_function_interpol_derivative,rho_electron_density,Electron_density_interpol_derivative, Neighbors, Total_atomic_coordinates, Radius)

    return Lattice_constant, Atomic_mass, Cut_off, Force_vector, Force_per_atom, Potential_energy_system, Potential_plot, Neighbors, Total_atomic_coordinates # Returning the complete information about local + ghost atoms for the following functions
