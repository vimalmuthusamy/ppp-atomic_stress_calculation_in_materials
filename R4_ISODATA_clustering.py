# Program to cluster data set using ISODATA algorithm

import numpy as np
from scipy import stats
import inspect # to get the current frame 
from tqdm import tqdm

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient numerical computation. 
#    Computing in Science & Engineering, 13(2):22–30, 2011.
# 2) Pauli Virtanen, Ralf Gommers, Travis E. Oliphant, Matt Haberland, Tyler Reddy, David Cournapeau, Evgeni Burovski, Pearu Peterson, Warren Weckesser, 
#    Jonathan Bright, Stéfan J. van der Walt, Matthew Page 47Computational Materials Science Journal Club, Summer 2019 Brett, Joshua Wilson, K. Jarrod Millman, 
#    Nikolay Mayorov, Andrew R. J. Nelson, Eric Jones, Robert Kern, Eric Larson, CJ Carey, İlhan Polat, Yu Feng, Eric W. Moore, Jake Vand erPlas, Denis Laxalde, Josef
#    Perktold, Robert Cimrman, Ian Henriksen, E. A. Quintero, Charles R Harris, Anne M. Archibald, Antônio H. Ribeiro, Fabian Pedregosa, Paul van Mulbregt, and 
#    SciPy 1. 0 Contributors. SciPy 1.0: Fundamental Algorithms for Scientific Computing in Python. Nature Methods, 17:261–272, 2020.
# 3) Ricardo Gutierrez-Osuna, Texas A&M university - Pattern Analysis lecture slides L15: Statistical clustering
# 4) Supporting information S1 Theory: ISODATA clustering algorithm 
#    https://journals.plos.org/plosone/article/file?type=supplementary&id=info:doi/10.1371/journal.pone.0099936.s005
# 5) Bao Lei Du and Qi Fei Jian. An isodata approach to the estimation of atomistic definition for continuum stress. In Materials Science Forum, 
#    volume 909, pages 293–299. Trans Tech Publ, 2017.
####################################################################################################################################################################


def get_mean_sd(Cluster,No_of_dimensions):
    """
        Description:
        ===========
        Calculates the Mean, Standard deviation & No of clusters for each cluster in cluster array
        
        Input:
        =====
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        2) No_of_dimensions = No of dimensions in the cluster; Array size -> Scalar
        
        Output:
        ======
        1) No_of_clusters = Total number of clusters; Array size -> Scalar
        2) Mean = Centroid of each cluster; Array size -> (No of clusters)
        3) Standard_deviation = Spread of each cluster; Array size -> (No of clusters)
    """
    No_of_clusters = len(Cluster)
    Standard_deviation = np.zeros((No_of_clusters,No_of_dimensions)) 
    Mean = np.zeros((No_of_clusters,No_of_dimensions))
    for i in range(No_of_clusters):
        Mean[i] = np.mean(Cluster[i],axis=0) # Calculates the centroid of each cluster in a vector format; Finding the mean along each dimension
        Standard_deviation[i] = np.std(Cluster[i],axis =0) # Calculates the spread of each cluster in a vector format; Finding the standard deviation along each dimension

    return No_of_clusters,Mean, Standard_deviation

def clean_empty_array(Cluster):
    """
        Description:
        ===========
        Removes the empty array resulting from slicing / dividing clusters
        
        Input:
        =====
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        
        Output:
        ======
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values) [Without empty arrays]
    """
    Cluster_copy = np.copy(Cluster) # Takes the copy of cluster without call by reference
    Cluster = [] # Direct delete using "del list" simultaneously rearranges the list indices after delete operation which allows neighboring empty arrays to escape !!!
    for array in Cluster_copy:
        if len(array) != 0:
            Cluster.append(array)

    return Cluster
    
def apply_pbc(Distance, Box_end_dimensions, Boundary_conditions):
    """
        Description:
        ===========
        Applies the Periodic Boundary Conditions if boundary conditions are "periodic" to the 
        distance values that are greater than half the value of the box end dimensions along 
        the respective axis 
        
        Input:
        =====
        1) Distance = Inter cluster distance of a cluster; Array size -> (3,No of clusters)
        2) Box_end_dimensions = Total box length along 3 directions; Array size -> (3)
        3) Boundary_conditions = Boundary conditions applied in the simulation box; Array size -> (3)
        
        Output:
        ======
        1) Distance= Distance values with periodicity applied; Array size -> (3,No of clusters)
    """
    # x - axis 
    if (Boundary_conditions[0] == "pp"): # LAMMPS format of periodic boundary conditions
        Distance[0:,0] = np.select([Distance[0:,0] <= (Box_end_dimensions[0]/2),Distance[0:,0] > (Box_end_dimensions[0]/2)], [Distance[0:,0], (Distance[0:,0] - (Box_end_dimensions[0]))]) # Applying PBC only to x-axis
        Distance[0:,0] = np.select([Distance[0:,0] >= (-(Box_end_dimensions[0]/2)),Distance[0:,0] < (-(Box_end_dimensions[0]/2))], [Distance[0:,0], (Distance[0:,0] + (Box_end_dimensions[0]))]) # Box_end dimensions are all the maximum values
    # y - axis 
    if (Boundary_conditions[1] == "pp"): # LAMMPS format of periodic boundary conditions
        Distance[0:,1] = np.select([Distance[0:,1] <= (Box_end_dimensions[1]/2),Distance[0:,1] > (Box_end_dimensions[1]/2)], [Distance[0:,1], (Distance[0:,1] - (Box_end_dimensions[1]))]) # Applying PBC only to y-axis
        Distance[0:,1] = np.select([Distance[0:,1] >= (-(Box_end_dimensions[1]/2)),Distance[0:,1] < (-(Box_end_dimensions[1]/2))], [Distance[0:,1], (Distance[0:,1] + (Box_end_dimensions[1]))]) # Box_end dimensions are all the maximum values
    # z - axis 
    if (Boundary_conditions[2] == "pp"): # LAMMPS format of periodic boundary conditions
        Distance[0:,2] = np.select([Distance[0:,2] <= (Box_end_dimensions[2]/2),Distance[0:,2] > (Box_end_dimensions[2]/2)], [Distance[0:,2], (Distance[0:,2] - (Box_end_dimensions[2]))]) # Applying PBC only to z-axis
        Distance[0:,2] = np.select([Distance[0:,2] >= (-(Box_end_dimensions[2]/2)),Distance[0:,2] < (-(Box_end_dimensions[2]/2))], [Distance[0:,2], (Distance[0:,2] + (Box_end_dimensions[2]))]) # Box_end dimensions are all the maximum values
    
    return Distance     
    
def get_cluster_sorted(No_of_clusters, Cluster, Mean):
    """
        Description:
        ===========
        Calculates the minimum distance between cluster centroids and the data points and
        assigns the data points to its nearest cluster
        
        Input:
        =====
        1) No_of_clusters = Total number of clusters; Array size -> Scalar
        2) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        3) Mean =  Centroid of each cluster; Array size -> (No of clusters)
        
        Output:
        ======
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
    """
    
    Index = [] # Since index can be a list
    for i in range(No_of_clusters):
        Delta = np.zeros_like(Cluster) # Dimensions equivalent to the corresponding cluster dimensions
        Delta_radius = np.zeros((No_of_clusters,len(Cluster[i])))
        for j in range(No_of_clusters): #"""EUCLIDEAN distance is used -> Since ISODATA - refinement of k-means clustering"""
            Delta[j] = Cluster[i] - Mean[j] # Calculation of Euclidean distance between all the points to the mean of each cluster
            Delta_radius[j] = np.linalg.norm(Delta[j],axis=1) # Calculating the Euclidean norm -> 1D array comes as a result                                    
        Index.append(np.argmin(abs(Delta_radius),axis=0)) # Taking the index of the elements that are closer to a particular centroid of a cluster
    
    for i in range(No_of_clusters):
        Cluster_index = Index[i]
        Cluster_index_delete = []
        for j in range(len(Cluster_index)):
            if Cluster_index[j] != i:
                try:
                    Cluster[Cluster_index[j]] = np.append(Cluster[Cluster_index[j]], np.array([Cluster[i][j]]),axis = 0) # To switch data points between clusters: """np.array([Cluster[i][j]]) -> To have equal dimensions of the new cluster !!!"""
                except IndexError: # Index error occurs due to mismatch between initial length of the clusters & the modification of cluster length (Array splitting below) but "for" loop has the predefined no of iterations
                    break
                Cluster_index_delete.append(j) # Indices of the switched data points that needs to be deleted from the host cluster
        try:
            Cluster[i] = np.delete(Cluster[i],Cluster_index_delete, axis = 0) # To remove the data point from the original cluster & all data points are in row wise -> so axis = 0
        except IndexError: # Index error occurs due to mismatch between initial length of the clusters & the modification of cluster length 
            break
    
    Cluster = clean_empty_array(Cluster) # To remove empty arrays
    return Cluster

"""
=================================================================================================================================================================================================
                                                        Cluster Split
=================================================================================================================================================================================================
"""
        
def get_cluster_split(Standard_deviation, Standard_deviation_threshold, Cluster, Cluster_average_distance, No_of_data_per_cluster, Minimum_no_of_data_per_cluster, Total_average_distance):
    """
        Description:
        ===========
        Splits the clusters in to 2 clusters which has the spread greater than the Standard deviation threshold
        
        Input:
        1) Standard_deviation = Spread of each cluster; Array size -> (No of clusters)
        2) Standard_deviation_threshold = Threshold for splitting the cluster based on the spread within the cluster; Array size -> (Scalar)
        3) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        4) Cluster_average_distance = Average distance of all the data points in the cluster to its centroid; Array size -> (No of clusters)
        5) No_of_data_per_cluster = No of data points in every cluster; Array size -> (No of clusters)
        6) Minimum_no_of_data_per_cluster = Minimum number of atoms to be in a cluster; Array size -> Scalar
        7) Total_average_distance = Average of average distances of all data points to the centroid of their cluster; Array size -> Scalar
        
        Output:
        ======
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
    """
    
    Delta_SD = Standard_deviation_threshold - Standard_deviation # Spread difference along each dimension from threshold
    Split_index = np.where(Delta_SD < 0) # Since SD cannot be "0" -> Segregating higher spread indices
    
    for i in np.unique(Split_index[0]): # Since 1D array, only [0] position is present from np.where # Unique -> Because repeated same cluster split results in empty cluster
        if (Cluster_average_distance[i] > Total_average_distance) and (No_of_data_per_cluster[i] > (2*Minimum_no_of_data_per_cluster + 1)): # +1 is to avoid creating exactly 2 clusters with the minimum no of atoms (if no of data per cluster is even)
            ### Condition is checked whether the average distance between data and its centroid is greater than the global average distance and the no of data in the 
            #   cluster is greater than the minimum number of data within a cluster
            Cluster_split = np.array_split(Cluster[i],2,axis = 0) # Splitting arrays with higher spread into 2 arrays
            Cluster[i] = Cluster_split[0] # Direct assignment of Cluster[0] = np.array_split(Cluster[i],2) results in list of lists -> Hard to flatten the list
            Cluster.append(Cluster_split[1]) # Since a list, direct append works (Checked)
            ### Since the new cluster is only appended to the entire cluster array, it will not modify the index of the previous clusters. 
            #   Reason for keeping cluster as a list not as a numpy array -> Due to different size of the arrays"""
    
    return Cluster

"""
=================================================================================================================================================================================================
                                                        Cluster Merge
=================================================================================================================================================================================================
"""
            
def get_cluster_merge(Mean, Inter_cluster_radius_threshold, No_of_clusters, Cluster, Box_bound = None, Boundary_conditions = None):
    """
        Description:
        ===========
        Merges two clusters whose centroids are less than or equal to the inter-cluster distance threshold and returns the merged cluster
        
        Input:
        =====
        1) Mean = Centroid of each cluster; Array size -> (No of clusters)
        2) Inter_cluster_radius_threshold = Threshold for merging the clusters based o  n their inter cluster radius; Array size -> (Scalar) 
        3) No_of_clusters = Total number of clusters; Array size -> Scalar
        4) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        5) Box_bound = Total box length along 3 directions; Array size -> (3)
        6) Boundary_conditions = Boundary conditions applied in the simulation box; Array size -> (3)
        
        Output:
        ======
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
    """
    Inter_cluster_distance = np.repeat(Mean[np.newaxis,:],len(Mean),axis = 0) - Mean[:,np.newaxis] # Direct calculation of inter-cluster distance eliminating nested for loops
    
    ### ONLY executed after the clustering process is converged
    if inspect.stack()[1][3] == "get_cluster_data_return_format": # [1][3] to get the function name from where the current function is called (In order to avoid previous functions in stack)
            for i in range(No_of_clusters):
                Inter_cluster_distance[i][0:,:3] = apply_pbc(Inter_cluster_distance[i][0:,:3], Box_bound, Boundary_conditions) # Sending only the x,y,z centroid distances
        
    Inter_cluster_radius = np.zeros((No_of_clusters,No_of_clusters))
    
    for i in range(No_of_clusters):
        Inter_cluster_radius[i] = np.linalg.norm(Inter_cluster_distance[i],axis = 1) # Finds the straight line distance of cluster to cluster distance (EUCLIDEAN norm)
        
    Merge_index = np.where(Inter_cluster_radius < Inter_cluster_radius_threshold) # This will result always in a 2 row matrix
    Merged_cluster_status = np.ones(No_of_clusters) * (-1) # All the non-merged clusters are given a negative value as the initial value
    Merged_cluster_delete_index = [] # Stores the indices of the merged clusters to delete them at the end
    
    for i in range(len(Merge_index[0])): # Merge_index will always be a 2 array (1 array -> row, 2 array -> Column) carrying index info 
        ### Redirects the merged cluster index to the parent cluster such that all the clusters that needs to be merged with the current
        #   cluster are merged to the parent cluster
        
        Merge_index[0][i] = np.select([Merged_cluster_status[Merge_index[0][i]] >= 0, Merged_cluster_status[Merge_index[0][i]] < 0], 
                            [Merged_cluster_status[Merge_index[0][i]], Merge_index[0][i]]) # If already unmerged, same index is retained
        Merge_index[1][i] = np.select([Merged_cluster_status[Merge_index[1][i]] >= 0, Merged_cluster_status[Merge_index[1][i]] < 0], 
                            [Merged_cluster_status[Merge_index[1][i]], Merge_index[1][i]])
        
        Minimum_index = min(Merge_index[0][i],Merge_index[1][i]) # Maximum between the two cluster indices -> Made such that the lower indices are given preference for appending
        Maximum_index = max(Merge_index[0][i],Merge_index[1][i]) # Minimum between the two cluster indices
        
        if (Merge_index[0][i] != Merge_index[1][i]): # Merge_index[0][i] & Merge_index[1][i] represents the same cluster; Checks neither of the cluster is already merged
            Cluster[Minimum_index] = np.concatenate((Cluster[Minimum_index], Cluster[Maximum_index])) # Merging 2 close distant clusters
            Merged_cluster_status[Maximum_index] = Minimum_index # Assigning the merged cluster status to the parent cluster
            Merged_cluster_status[Merged_cluster_status == Maximum_index] = Minimum_index # Replacing the all the other status which has the maximum index as their reference with the minimum index to which it is assigned to
            Merged_cluster_delete_index.append(Maximum_index) # Appending the merged cluster to be deleted. NOTE: Deleting at the end of "for" loop rather than here, is to avoid dynamic index changes due to delete operation
    if len(Merged_cluster_delete_index) != 0:                          
        Cluster = list(np.delete(Cluster,Merged_cluster_delete_index)) # Removing the merged cluster; list is to use "del" directly in the next iteration (which will be the input format)
    return Cluster

def get_cluster_data_return_format(Cluster, No_of_clusters, Input_data, Input_data_unstandardized, Mean, Inter_cluster_radius_threshold, Box_bound, Boundary_conditions, No_of_dimensions):
    """
        Description:
        ===========
        Segregates the clusters and its indices using the input data for routine return to the main function
        
        Input:
        =====
        1) Cluster = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        2) No_of_clusters = Total number of clusters; Array size -> Scalar
        3) Input_data = Position - stress data; Array size -> (No of atoms, 9) [Standardizes]
        4) Input_data_unstandardized = Position - stress data; Array size -> (No of atoms, 9)
        5) Mean = Centroid of each cluster; Array size -> (No of clusters)
        6) Inter_cluster_radius_threshold = Threshold for merging the clusters based on their inter cluster radius; Array size -> (Scalar) 
        7) Box_bound = Total box length along 3 directions; Array size -> (3)
        8) Boundary_conditions = Boundary conditions applied in the simulation box; Array size -> (3)
        9) No_of_dimensions = No of dimensions in the cluster; Array size -> Scalar
        
        Output:
        ======
        1) Clustered_data = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values) [In the unstandardized values]
        2) Cluster_indices = Atom IDs of the corresponding atoms in the cluster; Array size -> (No of clusters, No of atoms in a cluster, Atom ID)
        3) No_of_clusters = Total number of clusters; Array size -> Scalar
    """ 
    # To apply Periodic Boundary Conditions
    Cluster = get_cluster_merge(Mean,Inter_cluster_radius_threshold, No_of_clusters, Cluster, Box_bound, Boundary_conditions)

    Cluster_indices = [] # It is easy to append in list when the shape is unknown; Since only indices, no need to convert to numpy array
    No_of_clusters,Mean,Standard_deviation = get_mean_sd(Cluster,No_of_dimensions) # Re-initializing the cluster parameters

    for i in tqdm(range(No_of_clusters)):
        Indices = np.array([np.where((Input_data == data_point).all(axis = 1)) for data_point in Cluster[i]]) # Finding the indices of the cluster data points in the Input data
        Repeating_index_signal = np.array([1 if len(index[0]) == 1 else 0 for index in Indices]) # index[0] is the array for each data point checked with the input data
        if (Repeating_index_signal == np.ones_like(Repeating_index_signal)).all(): # If there is no repeating same data, then all will have only one occurrence
            Cluster_index = np.unique(Indices.flatten()) # Taking only the unique values of the indices; Since np.concatenate cannot works on 0D values
        else:
            Cluster_index = np.unique(Indices.flatten()) if (len((Indices.flatten()).shape) == 1) else np.unique(np.concatenate(Indices.flatten())) # Taking only the unique values of the indices; Additional "if" case to handle 1D input data
        Cluster_index = Cluster_index.astype(int) # Converting the values to int since indices (1.0 -> 1)
        Cluster_indices.append(Cluster_index) # Only atom ID's are stored; Checked !!             

    """========================================Segregating the input data based on clustered indices========================================"""

    Clustered_data = [] # Easy to apppend since uneven dimensions of clusters

    for i in tqdm(range(No_of_clusters)):
        Individual_Clustered_data = np.zeros_like(Cluster[i])
        Individual_Clustered_data = np.array([Input_data_unstandardized[int(Index)] for Index in Cluster_indices[i]]) # Taking the input data based on cluster_index value & assigning it to the clustered array; int -> since float is in array as 1.0
        Clustered_data.append(Individual_Clustered_data)
        
    return np.array(Clustered_data), Cluster_indices, No_of_clusters # Returns final force stress clustered data; Cluster indices for calculating cluster volume in the main program


def get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions, Initial_no_of_clusters):

    """
        Description:
        ============
        NOTE: Input data format
        Each row corresponds to a data point and each column to a dimension,
        Inter cluster radius threshold should be in terms of normalized distances

        Data Standardization done to eliminate the effect of different units on clustering & Euclidean distance measure
        -> Standardized value = (Value - Mean) / Standard deviation
        -> Scipy Zscore does the above formulation
        -> Standard deviation and Inter-cluster distance threshold
        -> Inter-cluster distance should be < 60% of the Standard deviation threshold
        -> This avoids the non-convergence due to merge and split of the same cluster repeatedly !!    
            
             ****************** PBC ***********************
            Applying PBC before hand can be complex since SD calculation involves actual data points !
            After convergence, calculate the intercluster radius for all the dimensions as usual & 
            calculate the inter-cluster radius for only 3 positional dimensions. If both the values are
            close enough ie) except the positions, all other dimensions has the same values, apply
            PBC (Box dimensions are found while standardizing the data) & if the distance is less than
            IC distance threshold, merge them together. No problem since inter-cluster distance is obtained
            from the SD with a relation, merging them together will not alter their SD !! -> So it can be 
            done at last. This avoids the continuous calculation of PBC every iteration, since it has more
            than 3 dimensions !!
            
        Input:
        =====
        1) Input_data_unstandardized = Position - stress data; Array size -> (No of atoms, 9)
        2) Inter_cluster_radius_threshold = Threshold for merging the clusters based on their inter cluster radius; Array size -> (Scalar) 
        3) Standard_deviation_threshold = Threshold for splitting the cluster based on the spread within the cluster; Array size -> (Scalar)
        4) Max_iterations = Maximum number of iterations; Array size -> Scalar
        5) Mean_tolerance = Tolerance value for convergence; Array size -> Scalar
        6) Std_tolerance = Tolerance value for convergence; Array size -> Scalar
        7) Minimum_no_of_data_per_cluster = Minimum number of atoms to be in a cluster; Array size -> Scalar
        8) Box_dimensions = Simulation box dimensions; Array size -> (2,3) [[Box_x_start, Box_y_start, Box_z_start],[Box_x_end, Box_y_end, Box_z_end]]
        9) Boundary_conditions = Boundary conditions applied in the simulation box; Array size -> (3)
        10) Initial_no_of_clusters = Initial number of clusters; Array size -> Scalar
        
        Output:
        ======
        1) Clustered_data = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        2) No_of_clusters = Total number of clusters; Array size -> Scalar
        3) Cluster_indices = Atom IDs of the corresponding atoms in the cluster; Array size -> (No of clusters, No of atoms in a cluster, Atom ID)
        4) Log_message = Convergence message to be written in the log file
        5) SSE = Sum squared error for the clustering; Array size -> Scalar
        6) Iteration = No of iterations taken for convergence; Array size -> Scalar
    """      
    
    No_of_data = Input_data_unstandardized.shape[0] # First index is no of rows
    No_of_dimensions = Input_data_unstandardized.shape[1] # Second index is no of columns
    Positions_unstandardized = Input_data_unstandardized[0:,:3] # Extracting only the atomic coordinates of all the atoms
    Positions_box_unstandardized = np.append(Positions_unstandardized, Box_dimensions.T, axis = 0) # Appending the box dimensions to the end to get them standardized. NOTE: Box dimension should be a (2,3) array with row -> Start, End; column -> x, y, z;
    
    """
        =========================================Data Standardization====================================
        Standardization is done to work with multiple dimensions having different units !
        Up on standardization, Standard deviation of the data along a given axis = 1.0 and its mean = 0
    """
    
    Input_data = stats.zscore(Input_data_unstandardized,axis = 0)  # Standardize data, Axis = 0 -> Standardize each dimension not each data point !!
    Positions_box = stats.zscore(Positions_box_unstandardized, axis = 0) # Standardizing the positions together with the box dimensions
    Input_data[:,:3] = Positions_box[:-2] # Replacing the standardized values of positions with standardized values that included box dimensions; Since the standardization is done only along a particular dimension
    Box_start_dimensions, Box_end_dimensions = Positions_box[-2], Positions_box[-1]
    Box_bound = (Box_end_dimensions - Box_start_dimensions) # To calculate the length of the simulation such that the box_start can be made as "0" origin
    
    Input_data = np.nan_to_num(Input_data) # NaN values results while standardizing "0" values
    No_of_clusters = Initial_no_of_clusters # Initial no of clusters given by the user; Default - 2
    Cluster = np.array_split(Input_data,No_of_clusters,axis=0) # Splitting input data into required no of clusters 
    Cluster = clean_empty_array(Cluster)
    """Every dimension is along the column wise & every row refers to a data point. So, splitting along axis = 0, splits the data point not the dimensions !"""
    No_of_clusters, Mean, Standard_deviation = get_mean_sd(Cluster, No_of_dimensions)
    Last_mean = np.zeros_like(Mean) # Initializing the variable 
    Last_standard_deviation = np.zeros_like(Mean) # Initializing the variable 
    Iteration = 0
    SSE = [] # Sum of Squared Error
    
    
    while True:
        Iteration += 1
                
        Cluster = get_cluster_sorted(No_of_clusters, Cluster, Mean)
        No_of_clusters,Mean,Standard_deviation = get_mean_sd(Cluster,No_of_dimensions) # Re-initializing the cluster parameters
            
        Cluster_average_distance = np.zeros(No_of_clusters) # Average distance between all the data points in a cluster and its centroid
        No_of_data_per_cluster = np.zeros(No_of_clusters) # Holds the no of data points in a cluster
        for i in range(No_of_clusters): # To get both the cluster and mean of the same cluster
            Distance_cluster = np.linalg.norm((Cluster[i] - Mean[i]), axis = 1) # Getting the radius between centroid and the data points in each cluster
            No_of_data_per_cluster[i] = len(Cluster[i])
            Cluster_average_distance[i] = np.sum(Distance_cluster) / No_of_data_per_cluster[i]

        Total_average_distance = np.sum(No_of_data_per_cluster * Cluster_average_distance) / No_of_data # Total average distance between points and cluster centers

        SSE.append(np.sum(Cluster_average_distance**2)) # Sum of the squares of the distances between the points and its centroids !! (This is always minimied in ISODATA)
        
        Cluster = get_cluster_split(Standard_deviation, Standard_deviation_threshold, Cluster, Cluster_average_distance, No_of_data_per_cluster, Minimum_no_of_data_per_cluster,Total_average_distance) # Splits the clusters
        
        No_of_clusters,Mean,Standard_deviation = get_mean_sd(Cluster,No_of_dimensions) # Re-initializing the cluster parameters after slit
        Cluster = get_cluster_sorted(No_of_clusters, Cluster, Mean) # Re-assigning cluster data to one of the nearest cluster after split
        No_of_clusters,Mean,Standard_deviation = get_mean_sd(Cluster,No_of_dimensions) # Re-initializing the cluster parameters after the clusters get sorted
        
        Cluster = get_cluster_merge(Mean, Inter_cluster_radius_threshold, No_of_clusters, Cluster) # Merging the clusters
        
        No_of_clusters,Mean,Standard_deviation = get_mean_sd(Cluster,No_of_dimensions) # Re-initializing the cluster parameters 
        
        """
            =================================================================================================================================================================================================
                                                                                                Convergence condition
            =================================================================================================================================================================================================
        """
        if len(Mean) == len(Last_mean) and Iteration < Max_iterations: # length of mean array check to avoid array size error
            if np.allclose(Last_mean, Mean, atol = Mean_tolerance) and np.allclose(Last_standard_deviation, Standard_deviation, atol = Std_tolerance): # Absolute tolerance is arbitrary
                if len(np.diff(SSE)) != 0: # If the cluster is not splitted once, then SSE will be same for all the iterations and its difference will be "0" -> One value
                    ### NOTE: atol -> Should be kept as enough possible, since very low error tolerances leads to non-convergence because of single data points flip between clusters at the borders !"""                
                    ### Finding the indices of the cluster from the input data"""
                    Log_message = "Convergence criterion : Mean tolerance norm - {} ; Standard deviation tolerance norm - {}".format(np.linalg.norm(Last_mean - Mean), np.linalg.norm(Last_standard_deviation - Standard_deviation))
                    
                    Clustered_data, Cluster_indices, No_of_clusters = get_cluster_data_return_format(Cluster, No_of_clusters, Input_data, Input_data_unstandardized, Mean,Inter_cluster_radius_threshold, Box_bound, Boundary_conditions, No_of_dimensions)
                    
                    return Clustered_data, No_of_clusters, Cluster_indices, Log_message, SSE, Iteration
                    break
                else:
                    Cluster = np.array_split(Input_data,(No_of_clusters + 1),axis=0) # Re-initializing the clusters with a +1 higher than the no of clusters; No of clusters (not initial no of clusters) since this may get executed more than once
                    SSE = [] # Re-initializing the variables
                    Iteration = 0
                    
        elif (Iteration == Max_iterations):
            Log_message = "Convergence criterion : Maximum iterations reached (Check the threshold values)"
                            
            ### Even if not converged due to threshold values, the current status of the cluster is returned with the warning message in the log file !"""

            Clustered_data, Cluster_indices, No_of_clusters = get_cluster_data_return_format(Cluster, No_of_clusters, Input_data, Input_data_unstandardized, Mean, Inter_cluster_radius_threshold, Box_bound, Boundary_conditions, No_of_dimensions)
            
            return Clustered_data, No_of_clusters, Cluster_indices, Log_message, SSE, Iteration
            break
        
        Last_mean, Last_standard_deviation = np.copy(Mean), np.copy(Standard_deviation) # np.copy() -> To avoid the issue of call by reference