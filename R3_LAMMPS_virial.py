# Program to calculate LAMMPS Local Virial stress tensor
"""
LAMMPS "metal" units are followed
mass = grams / mole
distance = Angstroms
time = picoseconds
energy = eV
force = eV / Angstrom
velocity = Angstrom / picosecond
volume = Angstrom^3
stress = ev / Angstrom^3
"""

import numpy as np
from tqdm import tqdm

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient numerical computation. 
#    Computing in Science & Engineering, 13(2):22–30, 2011.
# 2) Aidan P Thompson, Steven J Plimpton, and William Mattson. General formulation of pressure and stress tensor for arbitrary many-body interaction potentials 
#    under periodic boundary conditions. The Journal of chemical physics, 131(15):154107, 2009
####################################################################################################################################################################


def get_kinetic_contribution(No_of_atoms,Velocity_vector, Atomic_mass, Virial_volume_per_atom):
    """
    Description:
    ===========
    Calculates the Kinetic contribution of the virial stress tensor using atomic mass and velocity vector
    
    Input:
    1) No_of_atoms = No of local atoms
    2) Velocity_vector = Velocity vector imported from the LAMMPS dump file for every local atom; Array size -> (No of local atoms,3)
    3) Atomic_mass = Imported from the potential file for the given element
    4) Virial_volume_per_atom = Per atom voronoi volume based on the contributions from its neighbors
    
    Output:
    ======
    1) Velocity_tensor = Velocity tensor for each local atom; Array size -> (No of atoms,3,3)
    2) Kinetic_virial_stress_tensor = Kinetic part of the per atom virial stress tensor; Array size -> (No of atoms,3,3)
    """
    
    Kinetic_virial_stress_tensor = np.zeros((No_of_atoms,3,3))
    Velocity_tensor = np.zeros((No_of_atoms,3,3))
    
    for i in tqdm(range(No_of_atoms)):
        Velocity_tensor[i] += np.outer(Velocity_vector[i], Velocity_vector[i]) # Adding the contributions of all the atoms within the characteristic volume ie) cut-off radius
        Kinetic_virial_stress_tensor[i] = - (1/Virial_volume_per_atom[i]) * (Atomic_mass * Velocity_tensor[i])
    
    return Velocity_tensor, Kinetic_virial_stress_tensor

def get_potential_contribution(No_of_atoms, Virial_volume_per_atom, Total_atomic_coordinates, Force_x, Force_y, Force_z, Neighbors_list):
    """
        Description:
        ===========
        Calculates the Potential contribution of the virial stress tensor using Force and Interatomic distance vectors
        
        Input:
        =====
        1) No_of_atoms = No of local atoms
        2) Virial_volume_per_atom = Per atom voronoi volume based on the contributions from its neighbors
        3) Total_atomic_coordinates = Atomic coordinates of all atoms (local + images); Array size -> (Total no of atoms, 3)
        4) Force_x = Pairwise interactions of all local atoms with all atoms (local + image) along x direction; Array size -> (No of atoms, No of neighbors) 
        5) Force_y = Pairwise interactions of all local atoms with all atoms (local + image) along y direction; Array size -> (No of atoms, No of neighbors) 
        6) Force_z = Pairwise interactions of all local atoms with all atoms (local + image) along z direction; Array size -> (No of atoms, No of neighbors) 
        7) Neighbors_list = Neighbor list for every local atom; Array size -> (No of atoms, depends on the no of neighbors)
        
        Output:
        ======
        1) Force_radius_tensor = Dyadic product of force vector and distance vector for every local atom; Array size -> (No of atoms,3,3)
        2) Potential_virial_stress_tensor = Virial part of the per atom virial stress tensor; Array size -> (No of atoms,3,3)
    """
    
    Potential_virial_stress_tensor = np.zeros((No_of_atoms,3,3))
    Force_radius_tensor = np.zeros((No_of_atoms,3,3))
    
    for i in tqdm(range(No_of_atoms)):           
        Neighbors_per_atom = (Neighbors_list[i][Neighbors_list[i] >= 0])
        Distance_vector_neighbors = Total_atomic_coordinates[i] - Total_atomic_coordinates[Neighbors_per_atom]
        Force_vector_neighbors = (np.array([Force_x[i,:len(Neighbors_per_atom)],Force_y[i,:len(Neighbors_per_atom)],Force_z[i,:len(Neighbors_per_atom)]]).T).reshape(-1,3)
        Force_radius_tensor[i] = np.sum((Distance_vector_neighbors[:,:,None] * Force_vector_neighbors[:,None]), axis = 0)

        Potential_virial_stress_tensor[i] = - 0.5 * (1/Virial_volume_per_atom[i]) * (Force_radius_tensor[i])

    return Force_radius_tensor, Potential_virial_stress_tensor


def get_virial_stress_tensor(No_of_atoms, Total_atomic_coordinates, Force_vector, Atomic_mass, Velocity_vector, Virial_volume_per_atom, Neighbors_list):
    """
        Description:
        ===========
        Calculates the local virial stress tensor for every atom (both virial part + kinetic part) based on Clausius virial theorem
        
        Input:
        =====
        1) No_of_atoms = No of local atoms
        2) Total_atomic_coordinates = Atomic coordinates of all atoms (local + images); Array size -> (Total no of atoms, 3)
        3) Force_vector = Pairwise interactions of all local atoms with all atoms (local + image) in all directions; Array size -> (3,No of atoms, No of neighbors) 
        4) Atomic_mass = Imported from the potential file for the given element
        5) Velocity_vector = Velocity vector imported from the LAMMPS dump file for every local atom; Array size -> (No of local atoms,3)
        6) Virial_volume_per_atom = Per atom voronoi volume based on the contributions from its neighbors
        7) Neighbors_list = Neighbor list for every local atom; Array size -> (No of atoms, depends on the no of neighbors)
        
        Outtut:
        ======
        1) Velocity_tensor = Velocity tensor for each local atom; Array size -> (No of atoms,3,3)
        2) Force_radius_tensor = Dyadic product of force vector and distance vector for every local atom; Array size -> (No of atoms,3,3)
        3) Total_virial_stress_tensor = Kinetic part + Virial part of the per atom stress tensor; Array size -> (No of atoms,3,3)
        4) Symmetric_virial_stress_tensor = Only the symmetric part of the total virial stress tensor; Array size -> (No of atoms,6)
    """
    
    Force_x = Force_vector[0]
    Force_y = Force_vector[1]
    Force_z = Force_vector[2]
    
    Force_radius_tensor, Potential_virial_stress_tensor = get_potential_contribution(No_of_atoms,Virial_volume_per_atom,Total_atomic_coordinates, Force_x, Force_y, Force_z, Neighbors_list)
    Velocity_tensor, Kinetic_virial_stress_tensor = get_kinetic_contribution(No_of_atoms,Velocity_vector,Atomic_mass,Virial_volume_per_atom) # Using virial volume per atom as the input argument since it is the averaging volume for the cut off radius    
    
    """
    =================================================================================================================================================================================================
                                                    LAMMPS Virial Stress tensor = Kinetic contribution + Potential contribution
    =================================================================================================================================================================================================
    """
    
    Total_virial_stress_tensor = Kinetic_virial_stress_tensor + Potential_virial_stress_tensor # Addition of potential & kinetic contribution

    #========================================Stress tensor symmetry check=========================================================================
    
    for i in range(No_of_atoms):
        assert(np.allclose(Total_virial_stress_tensor[i], Total_virial_stress_tensor[i].T, atol =1e-6)), "Stress tensor is un-symmetric ! Check the inputs !!" # tolerance = 1e-6 arbitrary
    
    #========================================Returning only the symmetric part of the tensor as an 1D array========================================
    
    Symmetric_virial_stress_tensor = np.zeros((No_of_atoms,6))
    for i,tensor in enumerate(Total_virial_stress_tensor):
        Symmetric_virial_stress_tensor[i] = np.concatenate([np.array([tensor[0][0]]), np.array([tensor[1][1]]), np.array([tensor[2][2]]), np.array([tensor[0][1]]), np.array([tensor[0][2]]), np.array([tensor[1][2]])]) # Taking only the independent components sigma_11, sigma_22, sigma_33, sigma_12, sigma_13, sigma_23
    return Velocity_tensor, Force_radius_tensor, Total_virial_stress_tensor, Symmetric_virial_stress_tensor