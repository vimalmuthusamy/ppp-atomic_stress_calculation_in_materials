# Program to import & use Voro++ package
"""
LAMMPS "metal" units are followed
mass = grams / mole
distance = Angstroms
time = picoseconds
energy = eV
force = eV / Angstrom
velocity = Angstrom / picosecond
volume = Angstrom^3
stress = ev / Angstrom^3
"""

import numpy as np
import tess as ts # Voro ++ library 

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient numerical computation. 
#    Computing in Science & Engineering, 13(2):22–30, 2011.
# 2) Chris Rycroft. Voro++: A three-dimensional voronoi cell library in c++. Technical report, Lawrence Berkeley National Lab.(LBNL), Berkeley, 
#    CA (United States), 2009.
####################################################################################################################################################################


def get_voronoi_tessellation(Coordinates, Box_dimensions, Boundary_conditions):
    """
        Description:
        ===========
        Discretizes the simulation box volume based on the atomic positions using Voronoi Tessellation procedure using Voro++ package.
        NOTE: If the box is periodic, the coordinates should also have to formatted accordingly !
        
        Input:
        =====
        1) Coordinates = Atomic coordinates; Array size -> (No of atoms, 3)
        2) Box_dimensions = Simulation box start & end dimensions along the 3 directions; Array size -> (No of directions, 2) 
        3) Boundary_conditions = Boundary conditions of the simulation box; Nomenclature as per LAMMPS format ("pp" - periodic); Array size -> (1,3)
        
        Output:
        ======
        1) Volume = Voronoi volume per atom; Array size -> (1, No of atoms)
        2) Face areas = Areas of each face around an atom; Array size -> (No of atoms, No of faces areas)
        3) Neighbors per atom = Immediate neighbors around an atom; Array size -> (No of atoms, ID's of neighbor atoms)
        4) Vertices per face information = Vertices per face of all the atoms; Array size -> (No of faces, Vertices of each face, Coordinates of each vertex) 
    """
    
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1]
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1]
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1]
        
    Boundary_condition_x = True if Boundary_conditions[0] == "pp" else False
    Boundary_condition_y = True if Boundary_conditions[1] == "pp" else False
    Boundary_condition_z = True if Boundary_conditions[2] == "pp" else False
    
    Voronoi_class = ts.Container(Coordinates, limits = ((Box_x_start,Box_y_start,Box_z_start),(Box_x_end,Box_y_end,Box_z_end)),periodic = (Boundary_condition_x,Boundary_condition_y, Boundary_condition_z)) # Creating a Class using Container
    Volume_per_atom = np.array([np.array(i.volume()) for i in Voronoi_class]) # Volume correspoding to each atom -> In an array
    Face_areas_per_atom = np.array([np.array(i.face_areas()) for i in Voronoi_class]) # For each atom, there is an innerlist representing the areas of each face. np.array(i.face_areas()) -> Bcoz created are lists
    Neighbors_per_atom = np.array([np.array(i.neighbors()) for i in Voronoi_class]) # Lists the ID's of the points near each point 2D array 1-> Each point 2-> ID's of neighbours
    Vertices_per_face = np.array([np.array(i.vertices()) for i in Voronoi_class]) # A 3D array. 1-> All faces 2-> Vertices of each face 3-> Coordinate of each vertex
    
    return Volume_per_atom, Face_areas_per_atom, Neighbors_per_atom, Vertices_per_face