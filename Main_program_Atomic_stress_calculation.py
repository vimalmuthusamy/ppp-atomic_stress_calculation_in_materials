# Main program for atomic stress calculation in metals
"""
LAMMPS "metal" units are followed
mass = grams / mole
distance = Angstroms
time = picoseconds
energy = eV
force = eV / Angstrom
velocity = Angstrom / picosecond
electron density -> Arbitrary unit
volume = Angstrom^3
stress = ev / Angstrom^3
"""

# Unit conversion from (g/mol) * (Angstrom^2)/(picosecond^2) -> eV ----- Kinetic energy
Unit_conv_ke_eV = ((1e-3)*(1e-10)*(1e-10) * 6.241509e18) / ((1e-12) * (1e-12) * (6.02214179 * 1e23)) # 1 J = 6.241509e18 eV; Avogadro constant, 1 mole = 6.02214179 * 1e23 molecules
Unit_conv_next_time_step = ((1.602176565e-19) / (1e-10*(6.02214179 * 1e23) * 1e-3)) * (1e10/(1e12*1e12)) # To convert eV / (A.g/mol) -> A/ps^2; second part to convert m/s^2 -> A/ps^2
Unit_conv_tsai_kin = ((1e-3*(6.02214179 * 1e23) * 1e-10 *6.241509e18 * 1e12) / (1e-12*1e10)) # To convert g/mol * A/ps -> eV/A in Tsai kinetic contribution

import numpy as np
import matplotlib.pyplot as plt 
import scipy as sp
import argparse
import sys
import os
import time
from pathlib import Path
from R1_Interatomic_force import get_interatomic_force
from R1_Interatomic_force import get_local_plus_image_atom_coordinates
from R2_Volume_discretization import get_voronoi_tessellation
from R3_LAMMPS_virial import get_virial_stress_tensor
from R4_ISODATA_clustering import get_clustered_data
from R5_Gaussian_mixture_model_stress import get_GMM_probability_stress
from R6_Hardy_stress import get_hardy_stress
from R7_Tsai_stress import get_tsai_traction

def get_command_line_arguments(argv):
    """
        Description:
        ===========
        Parses command line arguments using argparse
        
        Output:
        ======
        1) potential_filename = Interatomic potential file name; dtype -> str
        2) element = Species; dtype -> str
        3) atomic_coordinates_filename = File name of atomic coordinates;dtype -> str
        4) sd_threshold = Standard deviation threshold for a cluster; dtype -> float
        5) intercluster_threshold = Inter-cluster distance threshold; dtype -> float
        6) time_step = Molecular dynamics time step; dtype -> float
        7) ISODATA_mean_tolerance = Cluster mean convergence tolerance for ISODATA clustering; dtype -> float
        8) ISODATA_std_tolerance = Cluster standard deviation convergence tolerance for ISODATA clustering; dtype -> float
        9) min_atoms_per_cluster = Minimum no of atoms per cluster; dtype -> int
        10) init_clusters = Initial number of clusters; dtype -> int
        11) max_iterations = Maximum number of iterations for clustering algorithm; dtype -> int
        12) req_stress = Type of stress definition to be used (Hardy, Tsai, Local Virial, Combined (Tsai + Hardy))
    """
    
    # Argument parser
    parser = argparse.ArgumentParser(description='='*130+'\n\t\t\t\t\t\tAtomic stress calculation inputs\n'+'='*130,formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('potential_fname', type=str, help='EAM potential file name. "eam (funcfl) (or) eam.alloy (setfl) file format\n\n')
    parser.add_argument('element', type=str, help='Element symbol; eg: Ni for Nickel\n\n')
    parser.add_argument('coordinates_fname', type=str, help='Name of the file containing atomic coordinates and atomic velocities\nNOTE: In LAMMPS dump file format\n\n')
    parser.add_argument('sd_threshold', type=float, help='Standard deviation threshold for ISODATA clustering; eg: 0.5 or 0.8\n\n')
    parser.add_argument('-req_stress','--required_stress', type=str, help='Type of stress definition to be used. hardy = Hardy stress; tsai = Tsai stress;\nlocal_virial = Local virial stress; combined = Tsai + Hardy based on inhomogeneity\nDEFAULT = combined\n\n',default = "combined")
    parser.add_argument('-ic','--intercluster_threshold', type=float, help='Inter-cluster distance threshold (In standardized units) \nNOTE: Inter-Cluster distance < Standard deviation threshold or difference between them should be minimum\nDEFAULT = 0.6*sd_threshold\n\n', nargs = "?",default = None) # Using 60% of the standard deviation threshold
    parser.add_argument('-t','--time_step', type=float, help='Molecular dynamics time step for Tsai kinetic contribution calculation \nDEFAULT = 1e-3 ps\n\n', nargs= "?",default = 1e-3)
    parser.add_argument('-mtol','--isodata_mean_tolerance', type=float, help='Cluster mean convergence tolerance for ISODATA clustering\nDEFAULT = 1e-6\n\n', nargs= "?", default = 1e-6)
    parser.add_argument('-sdtol','--isodata_std_tolerance', type=float, help='Cluster standard deviation convergence tolerance for ISODATA clustering \nDEFAULT = 1e-6\n\n', nargs= "?", default = 1e-6)
    parser.add_argument('-min','--min_atoms_per_cluster', type=int, help='Minimum no of atoms per cluster \nDEFAULT = 1\n\n', nargs= "?", default = 1)
    parser.add_argument('-init','--init_clusters', type=int, help='Initial number of clusters \nDEFAULT = 3\n\n', nargs= "?", default = 3)
    parser.add_argument('-max','--max_iterations', type=int, help='Maximum number of iterations for clustering algorithm \nDEFAULT = 1000', nargs= "?", default = 1000)
    arguments = parser.parse_args()
    return arguments

def get_cluster_volume(Volume_per_atom, Cluster_atom_id):
    """
        Description:
        ===========
        Calculates the sum of the voronoi volume per atom for each cluster
        
        Input:
        =====
        1) Volume_per_atom = Per atom voronoi volume; Array size -> (No of local atoms)
        2) Cluster_atom_id = IDs of atoms in each cluster; Array size -> (No of clusters, No of atoms in each cluster)
        
        Output:
        ======
        1) Cluster_volume = Total volume of each cluster; Array size -> (No of clusters)
    """
    
    Cluster_volume = np.zeros(len(Cluster_atom_id)) # Volume per each cluster
    for i,id in enumerate(Cluster_atom_id):
        Cluster_volume[i] = np.sum(Volume_per_atom[id]) # Summing up the volume of each atom in the particular cluster; int -> since id is float like 1.0
    
    return Cluster_volume

def get_cluster_max_size(Atomic_coordinates, Cluster_volume):
    """
        Description:
        ===========
        Calculates the centroid for each cluster and returns the maximum distance between the atom in the cluster and the centroid of the cluster
        
        Input:
        =====
        1) Atomic_coordinates = Atomic cooridnates of the local atoms since only local atoms are clustered from Task_4; Array size -> (No of local atoms,3)
        2) Cluster_volume = Voronoi volume of each cluster; Array size -> (No of clusters)
        
        Output:
        ======
        1) Max_distance = Maximum radius from the maximum distances of each cluster; Array size -> Scalar
    """
    
    if len(Atomic_coordinates) == 1: # Segregating the cluster made of one atom
        Max_distance = ((3*Cluster_volume) / (4 * np.pi))**(1/3) # Considering the voronoi volume as the spherical volume and finding its radius
    else:
        Centroid = np.mean(Atomic_coordinates,axis=0) # Mean along every dimension
        Distance = np.linalg.norm((Atomic_coordinates - Centroid), axis = 1) # Since spherical volume to be considered, only radius is required
        Max_distance = np.max(Distance) # Maximum radius from the centroid
        
    return Max_distance

def get_spatial_averaging_volume_radius(Cluster_volume, Atomic_coordinates,Cluster_atom_id):
    """
        Description:
        ===========
        Calculates the spatial averaging volume radius required for both Hardy stress tensor and the Tsai traction calculations
        Median of the cluster sizes from a sorted array in ascending order is considered as the spatial averaging radius; Ref:Modelling Simul. Mater. Sci. Eng. 20 (2012) 065009 (18pp)
        
        Input:
        =====
        1) Atomic_coordinates = Atomic cooridnates of the local atoms since only local atoms are clustered from Task_4; Array size -> (No of local atoms,3)
        2) Cluster_volume = Voronoi volume of each cluster; Array size -> (No of clusters)
        3) Cluster_atom_id = Index of the atoms in the respective clusters; Array size -> List of indices grouped in to clusters
        
        Output:
        ======
        1) Spatial_averaging_volume_radius = Spatial averaging volume radius calculated from the clusters; Array size -> Scalar
        2) Cluster_atom_id_flatten = Flattened array of cluster atom indices; Array size -> (1,No of atoms)
    """

    Cluster_id_flatten = np.array([]) # For the flattened cluster ID array to use it in for the "FOR" loop to calculate Hardy stress to only the required atoms

    Cluster_size = np.zeros(No_of_clusters)
    for j, id in (enumerate(Cluster_atom_id)):
        Cluster_size[j] = get_cluster_max_size(Atomic_coordinates[id], Cluster_volume[j]) # Getting only the maximum radius of a cluster
        Cluster_id_flatten = np.append(Cluster_id_flatten,id) # To flatten each cluster ID array

    Cluster_id_flatten = np.array(np.sort(Cluster_id_flatten),dtype = int) # dtype = int -> Since it is used as an index in the for loop
    Spatial_averaging_volume_radius = np.median(np.sort(Cluster_size)) 
    
    return Spatial_averaging_volume_radius, Cluster_id_flatten

def get_unique_bonds(Neighbors_list):
    """
        Description:
        ===========
        Filters only the unique bonds for all the local atoms with neighbors. (Without filtering, bond i-j & j-i will get double
        counted inside the same averaging volume). All the excluded neighbors are assigned a value of "-1"
        
        Input:
        =====
        1) Neighbors_list = List of neighbors of all atoms (Local + Images); Array size -> List of different sized arrays based on the number of neighbors to each atom
        
        Output:
        ======
        1) Neighbors_list_equal_size = Equal sized unique neighbors for each atom; Array size -> (No of atoms, No of neighbors); No of columns is chosen based on the maximum number of neighbors
    """

    Total_no_of_atoms = len(Neighbors_list) # Total no of atoms (local + images)
    
    for k in range(Total_no_of_atoms):
        Indices = Neighbors_list[k].astype(int)
        Indices = Indices[(Indices >= 0)] # Negative indices are avoided
        Neighbors_list[Indices, np.where(Neighbors_list[Indices] == k)[1]] = -1 # Assigning the repeated bond pairs with "-1". NOTE: Updated neighbor list is used for the subsequent iterations so that the upper indices remains unchanged !
    
    return Neighbors_list 

def get_unique_force_radius_tensor(Force_vector, Neighbors_list_unique, No_of_atoms, Total_atomic_coordinates):
    """
        Description:
        ===========
        Sorts only the distance vectors and force vectors of the unique neighbors (Non-repeating bonds) and returns the force radius tensor for all atoms with its neighbors
        Force and distance components for the negative neighbor indices are set "0"
        
        Input:
        =====
        1) Force vector = Force components of each with all other atoms; Array size -> (3,No of atoms,No of neighbors)
        2) Neighbors_list_unique = Equal sized unique neighbors for each atom; Array size -> (No of atoms, No of neighbors)
        3) No_of_atoms = No of local atoms; Array size -> Scalar
        4) Total_atomic_coordinates = Coordinates of all atoms (local + images); Array size -> (Total no of atoms,3)
        
        Output:
        ======
        1) Force_radius_tensor = Force radius tensor components for all atoms; Array size -> (Total no of atoms, No of neighbors, 9)
        2) Distance_x_vector_unique = Distance x values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        3) Distance_y_vector_unique = Distance y values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        4) Distance_z_vector_unique = Distance z values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        5) Force_x_vector_unique = Force x values of unique neighbors; Array size -> (Total no of atoms, No of unique neighbors)
        6) Force_y_vector_unique = Force y values of unique neighbors; Array size -> (Total no of atoms, No of unique neighbors)
        7) Force_z_vector_unique = Force z values of unique neighbors; Array size -> (Total no of atoms, No of unique neighbors)
    """

    # Initializing Unique distance and force arrays

    """==========================================Unique Distance vectors=========================================="""

    Negative_neighbor_indices = np.argwhere(Neighbors_list_unique < 0) # Indices of the repeating neighbors having the value "-1" in neighbor_list_unique

    Distance_x_vector_unique = np.array([Total_atomic_coordinates[i,0] - Total_atomic_coordinates[Neighbors_list_unique[i],0] for i in range(len(Total_atomic_coordinates))])
    Distance_x_vector_unique[Negative_neighbor_indices[:,0],Negative_neighbor_indices[:,1]] = 0 # Making the repeating neighbors where "-1" is used in neighbor_list_unique by "0"
    
    Distance_y_vector_unique = np.array([Total_atomic_coordinates[i,1] - Total_atomic_coordinates[Neighbors_list_unique[i],1] for i in range(len(Total_atomic_coordinates))])
    Distance_y_vector_unique[Negative_neighbor_indices[:,0],Negative_neighbor_indices[:,1]] = 0 # Making the repeating neighbors where "-1" is used in neighbor_list_unique by "0"
    
    Distance_z_vector_unique = np.array([Total_atomic_coordinates[i,2] - Total_atomic_coordinates[Neighbors_list_unique[i],2] for i in range(len(Total_atomic_coordinates))])
    Distance_z_vector_unique[Negative_neighbor_indices[:,0],Negative_neighbor_indices[:,1]] = 0 # Making the repeating neighbors where "-1" is used in neighbor_list_unique by "0"

    """==========================================Unique Force vectors=========================================="""

    # Local atoms
    Force_x_vector_unique = np.tile(Force_vector[0],(int(len(Total_atomic_coordinates)/No_of_atoms), 1)) # Extracting the Force x values of the correspoding neighbors; NOTE: For "-1" indices, last value in the correspoding row is included which has to be eliminated
    Force_y_vector_unique = np.tile(Force_vector[1],(int(len(Total_atomic_coordinates)/No_of_atoms), 1)) # Extracting the Force y values of the correspoding neighbors; NOTE: For "-1" indices, last value in the correspoding row is included which has to be eliminated
    Force_z_vector_unique = np.tile(Force_vector[2],(int(len(Total_atomic_coordinates)/No_of_atoms), 1)) # Extracting the Force z values of the correspoding neighbors; NOTE: For "-1" indices, last value in the correspoding row is included which has to be eliminated

    Force_x_vector_unique[Negative_neighbor_indices[:,0],Negative_neighbor_indices[:,1]] = 0 # Making the repeating neighbors where "-1" is used in neighbor_list_unique by "0"
    Force_y_vector_unique[Negative_neighbor_indices[:,0],Negative_neighbor_indices[:,1]] = 0 # Making the repeating neighbors where "-1" is used in neighbor_list_unique by "0"
    Force_z_vector_unique[Negative_neighbor_indices[:,0],Negative_neighbor_indices[:,1]] = 0 # Making the repeating neighbors where "-1" is used in neighbor_list_unique by "0"

    Distance_vector_unique = np.array([Distance_x_vector_unique.T, Distance_y_vector_unique.T, Distance_z_vector_unique.T]).T # Transposes are done for dimension compatibility -> (No of atoms, No of neighbors, 3)
    Force_vector_unique = np.array([Force_x_vector_unique.T, Force_y_vector_unique.T, Force_z_vector_unique.T]).T

    Force_radius_tensor = np.array([(Distance_vector_unique[atom_id][:,:,None] * Force_vector_unique[atom_id][:,None]).reshape(-1,9) for atom_id in range(len(Distance_vector_unique))]) # Calculating the force radius tensor of all neighbors of all atoms, Resulting dimension = (Total no of atoms, No of neighbors, 9); In reshape(-1,9) -> "-1" will take the appropriate value based on the input array size 

    return Force_radius_tensor, Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_x_vector_unique, Force_y_vector_unique, Force_z_vector_unique

def get_combined_hardy_tsai_stress(No_of_clusters,Clusters,Cluster_atom_id,No_of_atoms,Lattice_constant,Atomic_mass_ke,Time_step,Cutoff_radius,
                                           Atomic_coordinates,Force_vector,Velocity_vector,Cluster_volume, Total_atomic_coordinates, Neighbors_list,f,Required_stress,Total_atomic_coordinates_next_step, Minimum_no_of_atoms_per_cluster,Atomic_mass_tsai):
    """
        Description:
        ===========
        Calculates the no of atoms / cluster, maximum, minimum no of atoms in a cluster with respect to all the clusters
        and average no of atoms in clusters. Segregates the clusters based on the no of atoms and the clusters with no of 
        atoms less than the mode of all the no of atoms -> Hardy stress is calculated. For the rest, Tsai traction is 
        calculated. 
    
        Input:
        =====
        1) No_of_clusters = Total number of clusters; Array size -> Scalar
        2) Clusters = Position - stress cluster array; Array size -> (No of clusters, No of atoms in a cluster, Position-stress values)
        3) Cluster_atom_id = Atom IDs of the corresponding atoms in the cluster; Array size -> (No of clusters, No of atoms in a cluster, Atom ID)
        4) No_of_atoms = No of local atoms; Array size -> Scalar
        5) Lattice_constant = Lattice constant from the potential file; Array size -> Scalar
        6) Atomic_mass_ke = Imported from the potential file for the given element (Scaled for unit compatibility for kinetic energy); Array size -> Scalar
        7) Time_step = Time step used for the simulation; Array size -> Scalar
        8) Cutoff_radius = Imported from the potential file; Array size -> Scalar
        9) Atomic_coordinates = Coordinates of the atoms imported from LAMMPS dump file;  Array size -> (No of atoms, 3)
        10) Force_vector = Pairwise interactions of all local atoms with all atoms (local + image); Array size -> (3,No of atoms, No of neighbors) 
        11) Velocity_vector = Velocity components of all local atoms: Array size -> (No of atoms, 3)
        12) Cluster_volume = Voronoi volume of each cluster; Array size -> (No of clusters)
        13) Total_atomic_coordinates = Local atoms + Image atoms coordinates; Array size -> (Total no of atoms, 3)
        14) Neighbors_list = Neighbor list for every local atom; Array size -> (No of atoms, depends on the no of neighbors)
        15) f = Log file handle
        16) Required_stress = Type of stress definition to be used (Hardy, Tsai, Local Virial, Combined (Tsai + Hardy))
        17) Total_atomic_coordinates_next_step = Coordinates of the atoms from time integration for the given time step [Required for Tsai stress]; Array size -> (Total no of atoms, 3)
        18) Minimum_no_of_atoms_per_cluster = User defined minimum no of atoms per cluster for ISODATA clustering; Array size -> Scalar
        19) Atomic_mass_tsai = Imported from the potential file for the given element (Scaled for unit compatibility for momentum); Array size -> Scalar
        
        Output:
        ======
        1) Total_stress_tensor = Stress tensor based on the requested input stress (Combined (Hardy + Tsai), Hardy, Tsai, Local_virial); Array size -> (No of atoms, 9)
    """
    No_of_print_space = 100
    No_of_character_space = 40
    No_of_atoms_per_cluster = np.zeros(No_of_clusters) # Initializing the array
    for i,individual_cluster in enumerate(Clusters): # Calculates the no of atoms in all the clusters
        No_of_atoms_per_cluster[i] = len(individual_cluster)
        
    Maximum_atoms_in_clusters = np.amax(No_of_atoms_per_cluster) # np.amax() - Does not calculate the absolute maximum but the maximum
    Minimum_atoms_in_clusters = np.amin(No_of_atoms_per_cluster) # np.amin() - Does not calculate the absolute minimum but the minimum
    Maximum_atoms_cluster_id = np.where(Maximum_atoms_in_clusters == No_of_atoms_per_cluster)[0] # Returns an array consisting of all the clusters that has the maximum no of atoms
    Minimum_atoms_cluster_id = np.where(Minimum_atoms_in_clusters == No_of_atoms_per_cluster)[0] # Returns an array consisting of all the clusters that has the minimum no of atoms
    Mean_atoms_per_cluster = np.mean(No_of_atoms_per_cluster)
    Median_no_of_atoms = np.median(No_of_atoms_per_cluster)
    Mode_no_of_atoms = sp.stats.mode(No_of_atoms_per_cluster)[0][0] # [0][0] -> To extract the maximum number of occuring no of atoms in clusters

    print("Cluster statistics:\n" + "-"*int(np.char.str_len("Cluster statistics:")), file = f)
    print("Maximum_atoms_in_clusters :" + " "*(No_of_character_space-np.char.str_len("Maximum_atoms_in_clusters :")),Maximum_atoms_in_clusters,file = f)
    print("Minimum_atoms_in_clusters :" + " "*(No_of_character_space-np.char.str_len("Minimum_atoms_in_clusters :")),Minimum_atoms_in_clusters, file = f)
    print("Average_atoms_in_clusters :" + " "*(No_of_character_space-np.char.str_len("Average_atoms_in_clusters :")) + "{:.3f}".format(Mean_atoms_per_cluster), file = f)
    print("Median_atoms_in_clusters :" + " "*(No_of_character_space-np.char.str_len("Median_atoms_in_clusters :")),Median_no_of_atoms, file = f)
    print("Mode_atoms_in_clusters :" + " "*(No_of_character_space-np.char.str_len("Mode_atoms_in_clusters :")) + "",Mode_no_of_atoms, file = f)   
    
    Mean_no_of_atoms_per_cluster = np.mean(No_of_atoms_per_cluster)
    Standard_deviation_no_of_atoms_per_cluster = np.std(np.append(No_of_atoms_per_cluster, Minimum_no_of_atoms_per_cluster)) # Minimum no of atoms is added to segregate homogeneous clusters from non-homogeneous clusters
    
    Spatial_averaging_volume_radius, Cluster_atom_id_flatten = get_spatial_averaging_volume_radius(Cluster_volume, Atomic_coordinates,Cluster_atom_id)
    Neighbors_list_unique = get_unique_bonds(Neighbors_list).astype(int) # Getting only the unique bonds. This stays constant for all atoms    
    Neighbor_atomic_coordinates = np.array([Total_atomic_coordinates[atom_index] for atom_index in Neighbors_list_unique]) # For the "-1" indices, last atomic coordinate will be assigned its own atomic coordinate will be assigned for the corresponding atom
    Mask_coordinates = np.zeros_like(Neighbor_atomic_coordinates, bool)
    Mask_coordinates[Neighbors_list_unique < 0] = True # Masking all the negative neighbor indices
    Neighbor_atomic_coordinates_masked = np.ma.masked_array(Neighbor_atomic_coordinates, Mask_coordinates) # Masking the atomic coordinates whichever has the neighbor value = -1
    Force_radius_tensor, Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_x_vector_unique, Force_y_vector_unique, Force_z_vector_unique = get_unique_force_radius_tensor(Force_vector, Neighbors_list_unique, No_of_atoms, Total_atomic_coordinates)

    print("Spatial averaging volume size :" + " "*(No_of_character_space-np.char.str_len("Spatial averaging volume size :")) + "{:.5f} Angstrom".format(Spatial_averaging_volume_radius), file = f)

    ###############################################################################################################################################################################################
    #                                                                                   HARDY STRESS
    ###############################################################################################################################################################################################
    
    if (Required_stress == "hardy"):
        print("="*No_of_print_space,"\n\t\t\t\tHardy stress\n" + "="*No_of_print_space)
        Hardy_stress_tensor, Virial_stress_tensor_spatially_averaged = get_hardy_stress(No_of_atoms,Cluster_atom_id_flatten,Velocity_vector,
                                                                                    Atomic_mass_ke,Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_vector, Total_atomic_coordinates, Neighbors_list, Spatial_averaging_volume_radius, Force_radius_tensor, Neighbor_atomic_coordinates_masked) # Getting the Hardy stress
            
        return Hardy_stress_tensor
    
    ###############################################################################################################################################################################################
    #                                                                                   LOCAL VIRIAL STRESS
    ###############################################################################################################################################################################################
    
    elif (Required_stress == "local_virial"):
        print("="*No_of_print_space,"\n\t\t\t\tLocal virial stress\n" + "="*No_of_print_space)
        Hardy_stress_tensor, Virial_stress_tensor_spatially_averaged = get_hardy_stress(No_of_atoms,Cluster_atom_id_flatten,Velocity_vector,
                                                                                    Atomic_mass_ke,Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_vector, Total_atomic_coordinates, Neighbors_list, Spatial_averaging_volume_radius, Force_radius_tensor, Neighbor_atomic_coordinates_masked) # Getting the Hardy stress
            
        return Virial_stress_tensor_spatially_averaged
    
    ###############################################################################################################################################################################################
    #                                                                                   TSAI STRESS
    ###############################################################################################################################################################################################
    
    elif (Required_stress == "tsai"):
        print("="*No_of_print_space,"\n\t\t\t\tTsai stress\n" + "="*No_of_print_space)
        Tsai_stress_tensor = get_tsai_traction(No_of_atoms,Lattice_constant,Atomic_mass_tsai,Time_step,Cluster_atom_id_flatten,Total_atomic_coordinates,
                                                Force_vector,Velocity_vector, Spatial_averaging_volume_radius,Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_x_vector_unique, Force_y_vector_unique, Force_z_vector_unique,Neighbor_atomic_coordinates_masked, Total_atomic_coordinates_next_step)
        
        return Tsai_stress_tensor
    
    ###############################################################################################################################################################################################
    #                                                                                   COMBINED STRESS (TSAI + HARDY)
    ###############################################################################################################################################################################################
    
    elif (Required_stress == "combined"):
        Cluster_atom_id_hardy = np.array([], dtype = int) # int data type since they will be used in for loops
        Cluster_atom_id_tsai = np.array([], dtype = int) # Since only the atom ID's (1D) are required np.append() is used
        Cluster_volume_hardy = np.array([])
        Split_criteria = (Mean_no_of_atoms_per_cluster - Standard_deviation_no_of_atoms_per_cluster) if (Mean_no_of_atoms_per_cluster > Standard_deviation_no_of_atoms_per_cluster) else Mean_no_of_atoms_per_cluster # To avoid mean getting less than "0" 
        
        for i in range(No_of_clusters):
            if (No_of_atoms_per_cluster[i] < Split_criteria) or (No_of_atoms_per_cluster[i] <= (2*Minimum_no_of_atoms_per_cluster)): # Clusters with greater than the split criteria no of atoms can be regarded as homogeneous; # if the no of atoms in the cluster = 2*min of atoms per cluster, then this cluster is formed due to stringent SD threshold
                Cluster_atom_id_tsai = np.append(Cluster_atom_id_tsai,Cluster_atom_id[i]) 
            else:
                Cluster_atom_id_hardy = np.append(Cluster_atom_id_hardy, Cluster_atom_id[i])
                Cluster_volume_hardy = np.append(Cluster_volume_hardy, Cluster_volume[i])
        
        print("="*No_of_print_space,"\n\t\t\t\tCombined (Hardy) stress\n" + "="*No_of_print_space)
        Hardy_stress_tensor, Virial_stress_tensor_spatially_averaged = get_hardy_stress(No_of_atoms, Cluster_atom_id_hardy, Velocity_vector,
                                                                                    Atomic_mass_ke, Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_vector, Total_atomic_coordinates, Neighbors_list, Spatial_averaging_volume_radius, Force_radius_tensor, Neighbor_atomic_coordinates_masked) # Getting the Hardy stress for the appropriate atoms
        print("="*No_of_print_space,"\n\t\t\t\tCombined (Tsai) stress\n" + "="*No_of_print_space)
        Cluster_atom_id_tsai = np.array(Cluster_atom_id_tsai, dtype = int) # Since only integers can be used as indices
        Tsai_stress_tensor = get_tsai_traction(No_of_atoms,Lattice_constant,Atomic_mass_tsai,Time_step,Cluster_atom_id_tsai,Total_atomic_coordinates,
                                                    Force_vector,Velocity_vector,Spatial_averaging_volume_radius,Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_x_vector_unique, Force_y_vector_unique, Force_z_vector_unique,Neighbor_atomic_coordinates_masked, Total_atomic_coordinates_next_step) # Tsai stress tensor for the appropriate atoms
        Total_stress_tensor = Hardy_stress_tensor + Tsai_stress_tensor # It is not the addition of stresses because both Hardy & Tsai will have the same dimensions (No of atoms,3,3) but values are filled only at the appropriate indices rest are "0"

        return Total_stress_tensor
    
def get_time_integrated_coordinates(Atomic_coordinates, Time_step, Velocity_vector, Force_per_atom, Atomic_mass):
    """
        Description:
        ===========
        Integrates the current atomic coordinates with the given time step for the atomic coordinates at the next time step. This 
        new coordinate is required for Tsai stress calculation. Velocity-Verlet time integration scheme is utilized.
        
        Input:
        =====
        1) Atomic_coordinates = Coordinates of the local atoms; Array size -> (No of atoms, 3)
        2) Time_step = Time step used for the Molecular dynamics simulation in the current coordinate file; Array size -> Scalar
        3) Velocity_vector = Velocities of the local atoms; Array size -> (No of atoms, 3)
        4) Force_per_atom = Resultant force on each atom; Array size -> (No of atoms, 3)
        5) Atomic_mass = Scaled atomic mass according to the unit of velocity; Array size -> Scalar
        
        Output:
        ======
        1) Atomic_coordinates_next_step = Time integrated current atomic coordinates; Array size -> (No of atoms, 3)
    """
    
    # Velocity-Verlet time integration
    Velocity_vector_half_step = Velocity_vector + (Time_step/2) * (Force_per_atom / Atomic_mass)
    Atomic_coordinates_next_step = Atomic_coordinates + Time_step * Velocity_vector_half_step
    
    return Atomic_coordinates_next_step

if __name__=="__main__":
    
    No_of_print_space = 100
    No_of_character_space = 40 # For log file formatting
    Log_file_name = "log.Atomic_stress_program_" + str(time.ctime())
    Output_file_path = Path("Log_files", Log_file_name) # Creates a directory "Log_files"
    Output_file_path.parent.mkdir(exist_ok=True) # If the directory already exists, saves the log file in that directory 
    
    with open(str(Output_file_path),"w") as f:
        print("#"*100 + "\n\t\t\t\t\t\t\tAtomic stress calculation program\n" + "#"*100, file = f)
        print("#"*No_of_print_space + "\n\t\t\t\tAtomic stress calculation program\n" + "#"*No_of_print_space)
        Time_tuple = time.localtime()
        print(time.strftime("\nGenerated on: %d/%m/%Y - %H:%M:%S",Time_tuple),file = f)
        print("Created by: {}".format(os.getlogin()),file = f) # Login user name
        
        
        print("="*No_of_print_space,"\n\t\t\t\tData import\n" + "="*No_of_print_space)
        print("="*No_of_print_space,"\n\t\t\t\t\t\tData import\n" + "="*No_of_print_space,file = f)
        
        # Input Arguments
        args = get_command_line_arguments(sys.argv[1:])
        Atomic_coordinates_file_name = args.coordinates_fname # Atomsk files are taken as input -> Need to change to LAMMPS output file
        Name_split = ([name for name in Atomic_coordinates_file_name.split("/")]) # Splitting the string to get only the atomic coordinates file name if filename is provided as a path
        Atomic_coordinates_file_name = Name_split[-1] # The file name will be the last element in the list
        Potential_file_name = args.potential_fname
        Name_split = ([name for name in Potential_file_name.split("/")]) # Splitting the string to get only the atomic coordinates file name if filename is provided as a path
        Potential_file_name = Name_split[-1] # The file name will be the last element in the list
        Element = args.element
        Cluster_standard_deviation_threshold = args.sd_threshold
        Inter_cluster_distance_threshold = args.intercluster_threshold
        Inter_cluster_distance_threshold = Inter_cluster_distance_threshold if Inter_cluster_distance_threshold != None else 0.6*Cluster_standard_deviation_threshold
        Time_step = args.time_step
        ISODATA_mean_tol = args.isodata_mean_tolerance # Tolerance for convergence
        ISODATA_std_tol = args.isodata_std_tolerance # Tolerance for convergence
        Minimum_no_of_atoms_per_cluster = args.min_atoms_per_cluster # Minimum no of atoms per cluster -> ISODATA input parameter 
        Initial_no_of_clusters = args.init_clusters # Initial number of clusters for ISODATA
        Clustering_Max_iterations = args.max_iterations
        Required_stress = args.required_stress # Type of stress definition to be used
        
        # Sanity check of input arguments
        
        Available_stress = np.array(["hardy","tsai","local_virial","combined"])
        assert (np.isin(Available_stress,Required_stress)).any(), "The available stress definitions are hardy, tsai, local_virial, combined. Check the given required_stress argument !"
        assert Cluster_standard_deviation_threshold > 0, "Standard deviation threshold cannot be negative or zero !"
        assert Inter_cluster_distance_threshold >= 0, "Intercluster distance threshold cannot be negative !"
        assert Time_step > 0, "Time step cannot be negative or zero !"
        assert ISODATA_mean_tol >= 0, "Mean tolerance cannot be negative !"
        assert ISODATA_std_tol >= 0, "Standard deviation tolerance cannot be negative !"
        assert Minimum_no_of_atoms_per_cluster > 0, "Minimum number of atoms per cluster cannot be negative or zero !"
        assert Initial_no_of_clusters > 0, "Initial number of clusters cannot be negative or zero !"
        assert Clustering_Max_iterations > 0, "Maximum iterations for clustering cannot be negative or zero !"
        assert Minimum_no_of_atoms_per_cluster > 0, "Minimum number of atoms per cluster cannot be negative or zero !"
        assert np.char.str_len(Element) == 2, "Only element symbols should be given as input !"        
        
        print("\nInput Data:\n" + "-"*int(np.char.str_len("Input Data:")) + "\nInteratomic potential file :" + " "*(No_of_character_space - np.char.str_len("Interatomic potential file :")), Potential_file_name,file = f)
        print("Atom coordinates file :" + " "*(No_of_character_space - np.char.str_len("Atom coordinates file :")), Atomic_coordinates_file_name, file = f)
        print("Inter cluster distance threshold :" + " "*(No_of_character_space - np.char.str_len("Inter cluster distance threshold :")), Inter_cluster_distance_threshold,file = f)
        print("Cluster standard deviation threshold :" + " "*(No_of_character_space - np.char.str_len("Cluster standard deviation threshold :")),Cluster_standard_deviation_threshold,file = f)
        print("Maximum iterations (ISODATA) :" + " "*(No_of_character_space - np.char.str_len("Maximum iterations (ISODATA) :")),Clustering_Max_iterations,"\n",file = f)
        
        """
        ======================================================================================================================================================================================================================================================================
                                                                                                        Data Import
        ======================================================================================================================================================================================================================================================================
        """
        
        try:
            No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
            Atom_ID_file = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0), skip_header = 9, max_rows = No_of_atoms, dtype = int) # Getting the atom ID from the input file
            Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format; without unpack = True, Atom coordinates will have the shape (No of atoms, 3)
            Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3, dtype = float) # According to LAMMPS dump file format
            Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
            Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1]
            Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1]
            Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
            Velocity_vector = np.genfromtxt(Atomic_coordinates_file_name, usecols = (4,5,6), skip_header = 9, max_rows = No_of_atoms, dtype = float) # According to LAMMPS dump file format  
            Time_step_header = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 1, max_rows = 1)) # According to LAMMPS dump file format
        
        except:
            sys.exit("Error: Atomic coordinates file should follow the LAMMPS dump file format !")
        
        
        """
        ======================================================================================================================================================================================================================================================================
                                                                                                        Interatomic Force
        ======================================================================================================================================================================================================================================================================
        """
        print("="*No_of_print_space,"\n\t\t\t\tInteratomic force calculation\n" + "="*No_of_print_space)
        print("="*No_of_print_space,"\n\t\t\t\t\t\tInteratomic force calculation\n" + "="*No_of_print_space, file = f)
        
        Start_time = time.process_time() # Timer on for calculating routine call time
        Lattice_constant, Atomic_mass, Cutoff_radius, Force_vector, Force_per_atom, Potential_energy, Potential_plot, Neighbors_list, Total_atomic_coordinates = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_dimensions, Boundary_conditions)
        print("Interatomic force calculation time:" + " "*(No_of_character_space-np.char.str_len("Interatomic force calculation time:")) + "{:.2f} s".format(time.process_time() - Start_time), file = f)
        print("Potential energy of the system :" + " "*(No_of_character_space-np.char.str_len("Potential energy of the system :")) + "{:.5f} eV\n".format(Potential_energy),file = f)        

        Atomic_mass_next_step = Atomic_mass * Unit_conv_next_time_step # Scaling the atomic mass to get the right units for velocity in Velocity-Verlet time integration
        # Coordinates for next time step
        Atomic_coordinates_next_step = get_time_integrated_coordinates(Atomic_coordinates, Time_step, Velocity_vector, Force_per_atom, Atomic_mass_next_step) # Integrated atomic coordinates for the Tsai stress calculation
        Total_atomic_coordinates_next_step, Translation_vectors = get_local_plus_image_atom_coordinates(Atomic_coordinates_next_step,Boundary_conditions,Box_dimensions)
        
        Atomic_mass_ke = Atomic_mass * Unit_conv_ke_eV # Since atomic mass is involved in kinetic contribution, the unit conversions for correct energy units is embedded into the atomic mass
        Atomic_mass_tsai = Atomic_mass * Unit_conv_tsai_kin # For the correct momentum units in Tsai stress calculation

        """
        ======================================================================================================================================================================================================================================================================
                                                                                                        Volume discretization
        ======================================================================================================================================================================================================================================================================
        """
        
        print("="*No_of_print_space,"\n\t\t\t\tVolume discretization\n" + "="*No_of_print_space)
        print("="*No_of_print_space,"\n\t\t\t\t\t\tVolume discretization\n" + "="*No_of_print_space, file = f)
        
        Start_time = time.process_time()
        Volume_per_atom, Face_areas_per_atom, Neighbors_per_atom, Vertices_per_face = get_voronoi_tessellation(Atomic_coordinates, Box_dimensions, Boundary_conditions)
        print("Voronoi tessellation time :" + " "*(No_of_character_space-np.char.str_len("Voronoi tessellation time :")) + "{:.2f} s\n".format(time.process_time() - Start_time), file = f)
        
        assert np.allclose(np.sum(Volume_per_atom),((Box_x_end - Box_x_start) * (Box_y_end - Box_y_start) * (Box_z_end - Box_z_start)), atol = 1e-8) # Checks whether volume of the simulation box is consistent with the voronoi volume per atom

        
        """
        ======================================================================================================================================================================================================================================================================
                                                                                                        Virial Stress (Neighbor based)
        ======================================================================================================================================================================================================================================================================
        """
        
        print("="*No_of_print_space,"\n\t\t\t\tLAMMPS virial stress calculation\n" + "="*No_of_print_space)
        print("="*No_of_print_space,"\n\t\t\t\t\t\tLAMMPS virial stress calculation\n" + "="*No_of_print_space, file = f)
        
        Virial_volume_per_atom = np.array([(np.sum(Volume_per_atom[np.array(Neighbors_list[i]) % No_of_atoms]) + Volume_per_atom[i]) for i in range(No_of_atoms)]) # Summing up the volume of all atoms that are within the cut off region of the respective atom; Also adding itself since it is also a part of the characteristic volume; [Neighbors_list[i] % No_of_atoms] -> For image atoms indices
        Start_time = time.process_time()
        Velocity_tensor, Force_radius_tensor, Virial_stress_tensor, Symmetric_virial_stress_tensor = get_virial_stress_tensor(No_of_atoms, Total_atomic_coordinates, Force_vector, Atomic_mass_ke, Velocity_vector, Virial_volume_per_atom, Neighbors_list)
        print("Virial stress calculation time :" + " "*(No_of_character_space-np.char.str_len("Virial stress calculation time :")) + "{:.2f} s\n".format(time.process_time() - Start_time), file = f)

        
        """
        ======================================================================================================================================================================================================================================================================
                                                                                                            ISODATA Clustering
        ======================================================================================================================================================================================================================================================================
        """
        Position_stress_data = np.append(Atomic_coordinates,Symmetric_virial_stress_tensor, axis = 1) # 9D = 3D (Atom position) + 6D (Symmetric stress)

        print("="*No_of_print_space,"\n\t\t\t\tISODATA clustering\n" + "="*No_of_print_space)
        print("="*No_of_print_space,"\n\t\t\t\t\t\tISODATA clustering\n" + "="*No_of_print_space, file = f)
        
        Start_time = time.process_time()
        Position_stress_cluster, No_of_clusters, Cluster_atom_id, Log_message_ISODATA, SSE, Iterations = get_clustered_data(Position_stress_data,Inter_cluster_distance_threshold,Cluster_standard_deviation_threshold,Clustering_Max_iterations, ISODATA_mean_tol, ISODATA_std_tol, Minimum_no_of_atoms_per_cluster, Box_dimensions, Boundary_conditions, Initial_no_of_clusters)
        print("\nISODATA clustering time :" + " "*(No_of_character_space-np.char.str_len("ISODATA clustering time :")) + "{:.2f} s".format(time.process_time() - Start_time), file = f)
        print("No of clusters :" + " "*(No_of_character_space-np.char.str_len("No of clusters :")) + "",No_of_clusters,file = f)
        print(Log_message_ISODATA, "\n",file = f)
        Cluster_volume = get_cluster_volume(Volume_per_atom,Cluster_atom_id) # Total volume occupied by each cluster

        
        """
        ======================================================================================================================================================================================================================================================================
                                                                                                               GMM stress
        ======================================================================================================================================================================================================================================================================
        """
        
        print("="*No_of_print_space,"\n\t\t\t\tGMM stress calculation\n" + "="*No_of_print_space)
        print("="*No_of_print_space,"\n\t\t\t\t\t\tGMM stress calculation\n" + "="*No_of_print_space,file = f)
        
        Start_time = time.process_time()
        GMM_probability, GMM_stress_tensor = get_GMM_probability_stress(Position_stress_cluster,No_of_atoms) # Getting the probability from the GMM fit function for every atomic position; Stress tensor is actually flattened (6 D) symmetric stress
        print("GMM stress calculation time :" + " "*(No_of_character_space-np.char.str_len("GMM stress calculation time :")) + "{:.2f} s\n".format(time.process_time() - Start_time), file = f)

        
        """
        ======================================================================================================================================================================================================================================================================
                                                                                                     Combined Tsai and Hardy stress
        ======================================================================================================================================================================================================================================================================
        """
        
        print("="*No_of_print_space,"\n\t\t\t\t\t\t{} stress\n".format(Required_stress) + "="*No_of_print_space, file = f)
        Start_time = time.process_time()
        Total_stress_tensor = get_combined_hardy_tsai_stress(No_of_clusters,Position_stress_cluster,Cluster_atom_id,No_of_atoms,Lattice_constant,Atomic_mass_ke,Time_step,Cutoff_radius,
                                           Atomic_coordinates,Force_vector,Velocity_vector,Cluster_volume, Total_atomic_coordinates, Neighbors_list, f, Required_stress, Total_atomic_coordinates_next_step, Minimum_no_of_atoms_per_cluster,Atomic_mass_tsai) # One stress tensor based on the inhomogeneity in the input sample (Hardy & Tsai)
        print("\n\nTotal stress calculation time :" + " "*(No_of_character_space-np.char.str_len("Total stress calculation time :\t\t")) + "{:.2f} s\n".format(time.process_time() - Start_time), file = f)

        """
        ======================================================================================================================================================================================================================================================================
                                                                                                              Data Export
        ======================================================================================================================================================================================================================================================================
        """
        print("="*No_of_print_space,"\n\t\t\t\tData export\n" + "="*No_of_print_space)
        No_of_dimensions = 32 # No of dimensions in all the parameters
        Output_data_array_combined_stress = np.zeros((No_of_atoms,No_of_dimensions))
        Total_stress_tensor_flatten = np.zeros((No_of_atoms,9))
        
        for i in range(No_of_atoms):
            Total_stress_tensor_flatten[i] = Total_stress_tensor[i].flatten()
            Output_data_array_combined_stress[i] = np.concatenate([np.array([Atom_ID_file[i]]),Atomic_coordinates[i],Velocity_vector[i],Force_per_atom[i], Total_stress_tensor_flatten[i],GMM_stress_tensor[i],Symmetric_virial_stress_tensor[i],np.array([Volume_per_atom[i]])]) # np.array() for all 0D -> for np.concatenate compatibility
        
        File_name_combined = "Atomic_stress_output_{}_{}.eam".format(Element,Atomic_coordinates_file_name)
        Output_file_path_combined = Path("Result_files", File_name_combined) # Creates a directory "Result_files"
        Output_file_path_combined.parent.mkdir(exist_ok=True) # If the directory already exists, saves the result file in that directory 
        Potential_file_name = "Interatomic_potential_functions_{}.png".format(Potential_file_name)
        Output_file_path_plot = Path("Result_files", Potential_file_name) # Creates a directory "Result_files"
        Output_file_path_plot.parent.mkdir(exist_ok=True) # If the directory already exists, saves the result file in that directory 
        plt.savefig("{}".format(Output_file_path_plot), bbox_inches='tight', dpi = 300) # saving plot in the Results directory
        
        np.savetxt(Output_file_path_combined,(Output_data_array_combined_stress),fmt = " ".join(["%i"] + ["%.5f"] * 3 + ["%.5e"] * (28)),delimiter= " ", newline = "\n", 
                   header= "ITEM: TIMESTEP\n{}\nITEM: NUMBER OF ATOMS\n{}\nITEM: "
                   "BOX BOUNDS {} {} {}\n{} {}\n{} {}\n{} {}\nITEM: ATOMS id x y z velocity_x velocity_y velocity_z force_x force_y force_z"
                   " {}_stress[11] {}_stress[12] {}_stress[13] {}_stress[21] {}_stress[22] {}_stress[23] {}_stress[31] {}_stress[32] {}_stress[33]"
                   " gmm_stress[11] gmm_stress[22] gmm_stress[33] gmm_stress[12] gmm_stress[13] gmm_stress[23]"
                   " virial_stress[11] virial_stress[22] virial_stress[33] virial_stress[12] virial_stress[13] virial_stress[23]"
                   .format(Time_step_header,No_of_atoms,Boundary_conditions[0],Boundary_conditions[1],Boundary_conditions[2], 
                          Box_x_start, Box_x_end, Box_y_start, Box_y_end,Box_z_start, Box_z_end,Required_stress,Required_stress,
                          Required_stress,Required_stress,Required_stress,Required_stress, Required_stress,Required_stress,Required_stress), comments = "")

        plt.close()
        print("#"*100+"\n\t\t\t\t\tEnd of program\n"+"#"*100)

