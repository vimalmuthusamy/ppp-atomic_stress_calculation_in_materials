# Program to test Local virial stress calculation
# coding: utf-8
import pytest
import numpy as np
import scipy as sp
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
# from Task_1_Interatomic_force_calculation import get_interatomic_force
# from Task_2_Volume_discretization import get_voronoi_tessellation
# from Task_3_Virial_stress_calculation import get_virial_stress_tensor
from Task_6_Hardy_stress_calculation_test_code import get_cluster_max_size
from Task_6_Hardy_stress_calculation_test_code import get_weighting_factor
from Task_6_Hardy_stress_calculation_test_code import get_bond_fn_integration
from Task_6_Hardy_stress_calculation_test_code import get_bonding_factor
from Task_6_Hardy_stress_calculation_test_code import get_radius_of_intersections
from Task_6_Hardy_stress_calculation_test_code import get_minimum_distance_btw_linesegment_point
from Task_6_Hardy_stress_calculation_test_code import get_unique_bonds
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D
plt.rcParams.update({"font.size":30}) # Changes the size of the entire plot
np.set_printoptions(threshold=sys.maxsize)
from itertools import product


@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_cluster_max_size_equal_unit_cubic_volume(get_cluster_inputs):
    """
        Tests whether the cluster size is calculated to be the average of the maximum size
        of the clusters
    """
    print(test_cluster_max_size_equal_unit_cubic_volume.__doc__)
    
    All_points, Clusters = get_cluster_inputs
    Volume = 1 # dx = dy = 1

    Cluster_max_size = []
    for cluster in Clusters:
        Cluster_max_size.append(get_cluster_max_size(cluster, Volume*len(cluster)))
    
    fig, ax = plt.subplots(figsize = (20,20))
    for i,cluster in enumerate(Clusters):
        circle = plt.Circle((np.mean(cluster[0:,0]), np.mean(cluster[0:,1])), Cluster_max_size[i], fill = False)
        ax.scatter(cluster[0:,0],cluster[0:,1])#, label = "Cluster - {}".format(i))
        ax.add_artist(circle)
    ax.set(xlabel = "x coordinate " + r"$(\AA)$", ylabel = "y coordinate " + r"$(\AA)$")
    ax.grid()
    ax.legend()
    plt.suptitle("Test Cluster size for cluster volume equal unit cubic volume", weight = "bold")
    plt.savefig("Task_6_test_cluster_max_size_equal_unit_cubic_volume.png")
    plt.close()
    
    assert np.allclose(Cluster_max_size, 0.62035, atol = 1e-5) # 0.62035 - Radius of the sphere for the unit volume
    

@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_cluster_max_size_random_clusters():
    """
        Tests whether the cluster size is calculated for different sized random clusters and also plots the median of the cluster sizes
    """
    print(test_cluster_max_size_random_clusters.__doc__)
    
    Cluster_1 = np.random.randint(0,5,(30,2))
    Cluster_2 = np.random.randint(20,30,(5,2))
    Cluster_3 = np.random.randint(90,100,(10,2))
    Cluster_4 = np.random.randint(60,80,(4,2))
    Cluster_5 = np.random.randint(40,45,(5,2))
    Cluster_6 = np.random.randint(5,15,(8,2))
    Cluster_7 = np.random.randint(81,85,(2,2))
    Cluster_8_1 = np.random.randint(0,5,10)
    Cluster_8_2 = np.random.randint(75,80,10)
    Cluster_8 = np.zeros((2,10))
    Cluster_8[0] = Cluster_8_1
    Cluster_8[1] = Cluster_8_2
    Cluster_8 = Cluster_8.T
    Cluster_9_1 = np.random.randint(0,20,8)
    Cluster_9_2 = np.random.randint(75,80,8)
    Cluster_9 = np.zeros((2,8))
    Cluster_9[0] = Cluster_9_2
    Cluster_9[1] = Cluster_9_1
    Cluster_9 = Cluster_9.T
    Cluster_10_1 = np.random.randint(45,55,20)
    Cluster_10_2 = np.random.randint(77,84,20)
    Cluster_10 = np.zeros((2,20))
    Cluster_10[0] = Cluster_10_2
    Cluster_10[1] = Cluster_10_1
    Cluster_10 = Cluster_10.T
    Cluster_11_1 = np.random.randint(10,40,6)
    Cluster_11_2 = np.random.randint(60,84,6)
    Cluster_11 = np.zeros((2,6))
    Cluster_11[0] = Cluster_11_1
    Cluster_11[1] = Cluster_11_2
    Cluster_11 = Cluster_11.T
    
    Volume = 1 # dx = dy = 1
    Clusters = np.array([Cluster_1, Cluster_2, Cluster_3, Cluster_4, Cluster_5, Cluster_6, Cluster_7, Cluster_8, Cluster_9, Cluster_10, Cluster_11])

    Cluster_max_size = []
    for cluster in Clusters:
        Cluster_max_size.append(get_cluster_max_size(cluster, Volume*len(cluster)))
        
    Spatial_averaging_radius = np.median(np.sort(Cluster_max_size))
    Median_cluster_id = np.where(Spatial_averaging_radius == Cluster_max_size)[0]
    
    fig, ax = plt.subplots(figsize = (20,20))
    for i,cluster in enumerate(Clusters):
        if i == Median_cluster_id:
            circle1 = plt.Circle((np.mean(cluster[0:,0]), np.mean(cluster[0:,1])), Cluster_max_size[i], fill = False, color = "green", hatch = "/", linewidth = 2)
            ax.add_artist(circle1)
        else:
            circle = plt.Circle((np.mean(cluster[0:,0]), np.mean(cluster[0:,1])), Cluster_max_size[i], fill = False)
            ax.add_artist(circle)
        ax.scatter(cluster[0:,0],cluster[0:,1])#, label = "Cluster - {}".format(i))
        
    ax.set(xticks = range(0,100,2), yticks = range(0,100,2))
    ax.set_xlabel("x coordinate " + r"$(\AA)$", fontsize = 20)
    ax.set_ylabel("y coordinate " + r"$(\AA)$", fontsize = 20)
    ax.grid()
    ax.legend([circle1], ["Median of the cluster radius"])
    plt.suptitle("Test Cluster size for random clusters", weight = "bold", fontsize = 40)
    plt.savefig("Task_6_test_cluster_max_size_random_cluster_points.png")
    plt.close()


@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_weighting_factor():
    """
        Tests the weighting factor function for the incremental values of the radius within & beyond the spatial averaging volume radius.
        
        OBJECTIVE: The Localization function curve needs to be reproduced
    """
    print(test_weighting_factor.__doc__)
    
    Spatial_averaging_radius = 2
    Delta_radius = 0.12 * Spatial_averaging_radius # For the used mollifying function -> Ref: Admal MD Stress lab manual
    
    Radius_vector_x = np.arange(0,Spatial_averaging_radius*1.5,Spatial_averaging_radius*0.001) # 0.1% of the spatial averaging volume radius is considered as increment. 150% of radius is the max radius
    Radius_vector = np.zeros((len(Radius_vector_x),3)) # Only x component of the radius vector is considered
    Radius_vector[0:,0] = Radius_vector_x
    
    Total_weighting_factor, Constant_weighting_factor = get_weighting_factor(Spatial_averaging_radius, Delta_radius, Radius_vector)
    
    fig, ax = plt.subplots(figsize=(40,20))
    ax.plot(Radius_vector_x, Total_weighting_factor, label = "Weighting factor", c = "g", linewidth = 3, marker = "o", markerfacecolor = "orange")
    ax.axvline(Spatial_averaging_radius,label = "Spatial averaging volume radius", c = "red", linestyle = "--")
    ax.axvline(0.0, c = "black")
    ax.axhline(0.0, c = "black")
    ax.grid()
    ax.legend()
    ax.set(xlabel = "Radius "+r"$(\AA)$", ylabel = "Weighting factor")
    plt.suptitle("Weighting factor (Constant function + Mollifying function) for all radius vectors", weight = "bold")
    plt.savefig("Task_6_test_weighting_factor.png")
    plt.close()
    
    assert Constant_weighting_factor == 1 / ((4/3) * np.pi * Spatial_averaging_radius**3)    


@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_bond_function_integration():
    """
        Tests the integration of the bond function for the incremental values of the radius within & beyond the spatial averaging volume radius.
        
        OBJECTIVE: The integrated form of the localization function curve needs to be reproduced
    """
    print(test_bond_function_integration.__doc__)
    
    Spatial_averaging_radius = 1
    Delta_radius = 0.12 * Spatial_averaging_radius # For the used mollifying function -> Ref: Admal MD Stress lab manual
    
    Radius_vector_x = np.arange(0,Spatial_averaging_radius*1.5,Spatial_averaging_radius*0.001) # 0.1% of the spatial averaging volume radius is considered as increment. 150% of radius is the max radius
    Radius_vector = np.zeros((len(Radius_vector_x),3)) # Only x component of the radius vector is considered
    Radius_vector[0:,0] = Radius_vector_x
    
    Total_weighting_factor, Constant_weighting_factor = get_weighting_factor(Spatial_averaging_radius, Delta_radius, Radius_vector)
    
    Bonding_factor = get_bond_fn_integration(Radius_vector_x,Constant_weighting_factor,Spatial_averaging_radius, Delta_radius)
    
    fig, ax = plt.subplots(2,figsize=(40,20))
    ax[0].plot(Radius_vector_x, Total_weighting_factor, label = "Weighting factor", c = "g", linewidth = 3, marker = "o", markerfacecolor = "orange")
    ax[0].axvline(Spatial_averaging_radius,label = "Spatial averaging volume radius", c = "red", linestyle = "--")
    ax[0].axvline(0.0, c = "black")
    ax[0].axhline(0.0, c = "black")
    ax[0].grid()
    ax[0].legend()
    ax[0].set(xlabel = "Radius "+r"$(\AA)$", ylabel = "Weighting factor")
    
    ax[1].plot(Radius_vector_x, Bonding_factor, label = "Integrated bonding factor", c = "g", linewidth = 3, marker = "o", markerfacecolor = "orange")
    ax[1].axvline(Spatial_averaging_radius,label = "Spatial averaging volume radius", c = "red", linestyle = "--")
    ax[1].axvline(0.0, c = "black")
    ax[1].axhline(0.0, c = "black")
    ax[1].grid()
    ax[1].legend()
    ax[1].set(xlabel = "Radius "+r"$(\AA)$", ylabel = "Bonding factor")
    
    plt.suptitle("Bond integration function (Constant function + Mollifying function)", weight = "bold")
    plt.savefig("Task_6_test_bond_function_integrations.png")
    plt.close()

@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_bond_intersection_with_averaging_volume():
    """
        Tests the intersection of the bonds between atoms with the characteristic volume of an atom. 
        Calculates the bond fraction ie) bond integrated values
        
        OBJECTIVE: Atoms with bonds intersecting the characteristic volume should be highlighted based on their fraction within it !
    """
    print(test_bond_intersection_with_averaging_volume.__doc__)
    
    x_atom = np.array([0,0,0]) # ---- Atom at the center - x
    # Beta_atom_coordinates = np.array([[-4,0,0],[0,2,0],[-2,2,1],[2,3,-3],[0.3,0.3,1],[0.8,1,0],[0.78,1,0],[0.2,0.2,0.56]])
    Beta_atom_coordinates = np.array([[-4,0,0],[0,2,0],[-2,2,0],[2,3,0],[0.3,0.3,0],[0.8,1,0],[0.78,1,0],[0.2,0.2,0]])
    # Alpha_atoms = np.array([[-2,1,0],[3,-2,0]]) # Alpha atom
    Alpha_atoms = np.array([[-2,1,0],[0.5,-0.5,0]]) # Alpha atom

    Spatial_averaging_radius = 1
    Delta_radius = 0.12 * Spatial_averaging_radius # For the used mollifying function -> Ref: Admal MD Stress lab manual
    Constant_weighting_factor = 1 / ((4/3) * np.pi * Spatial_averaging_radius**3) 
    
    # 2D plot
    
    fig, ax = plt.subplots(figsize=(40,40))   
    for Alpha_atom in Alpha_atoms:
        Bonding_factor = get_bonding_factor(Alpha_atom, Beta_atom_coordinates, x_atom, Constant_weighting_factor, Delta_radius, Spatial_averaging_radius)
        print("bonding:",Bonding_factor)
        for i,coordinate in enumerate(Beta_atom_coordinates):
            ax.plot([Alpha_atom[0],coordinate[0]],[Alpha_atom[1],coordinate[1]], marker = "o", markersize = 20, linestyle = "--" if Bonding_factor[i] == 0 else "-", label = "Non Contributing bond" if Bonding_factor[i] == 0 else "Contributing bond", linewidth = 5)
        circle = plt.Circle((x_atom),Spatial_averaging_radius, fill = False, label = "Averaging volume")
        ax.add_artist(circle)
    ax.scatter(x_atom[0], x_atom[1], label = "x_atom", c = "red", s = 100)
    ax.set(xlabel = "x coordinate " + r"$(\AA)$", ylabel = "y coordinate " + r"$(\AA)$")
    plt.suptitle("Bonds intersecting with averaging volume", weight = "bold")
    ax.grid()
    plt.legend()
    plt.savefig("Task_6_test_bond_intersection_with_averaging_volume_2d.png")
    plt.close()
    
    # 3D plot
    
    fig, ax = plt.subplots(figsize=(40,40))
    ax = plt.axes(projection='3d')

    u = np.linspace(0, np.pi, 20)
    v = np.linspace(0, 2 * np.pi, 20)

    x = np.outer(np.sin(u), np.sin(v))
    y = np.outer(np.sin(u), np.cos(v))
    z = np.outer(np.cos(u), np.ones_like(v))

    ax.plot_wireframe(x, y, z, color = "red", label = "Averaging volume")
    
    for Alpha_atom in Alpha_atoms:
        Bonding_factor = get_bonding_factor(Alpha_atom, Beta_atom_coordinates, x_atom, Constant_weighting_factor, Delta_radius, Spatial_averaging_radius)      
        for i,coordinate in enumerate(Beta_atom_coordinates):
            ax.plot([Alpha_atom[0],coordinate[0]],[Alpha_atom[1],coordinate[1]],[Alpha_atom[2],coordinate[2]], marker = "o", markersize = 20, linestyle = "--" if Bonding_factor[i] == 0 else "-", label = "Non Contributing bond" if Bonding_factor[i] == 0 else "Contributing bond", linewidth = 5)
    ax.scatter(x_atom[0], x_atom[1],x_atom[2], label = "x_atom", c = "red", s = 100)
    ax.set(xlabel = "x coordinate " + r"$(\AA)$", ylabel = "y coordinate " + r"$(\AA)$", zlabel = "z coordinate " + r"$(\AA)$")
    plt.suptitle("Bonds intersecting with averaging volume", weight = "bold")
    ax.grid()
    ax.legend()
    plt.savefig("Task_6_test_bond_intersection_with_averaging_volume_3d.png")
    plt.close()
    
    assert 1==2

@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_bond_intersection_points_with_averaging_volume():
    
    x_vector = np.array([0,1,0])
    x_alpha = np.array([0.25,1,0])
    x_beta = np.array([[0.8,1.2,0],[0.8,2,0],[1.256,-0.134,0]])
    
    Spatial_averaging_radius = 0.5
    Delta_radius = 0.12*Spatial_averaging_radius
    Constant_weighting_factor = 1 / ((4/3) * np.pi * Spatial_averaging_radius**3) 
    
    Radius_1, Radius_2, Removed_indices = get_radius_of_intersections(x_vector, x_alpha, x_beta, Spatial_averaging_radius)
    
    print("rad-1:",Radius_1,"\nRad-2:",Radius_2,"\nRemoved indices:",Removed_indices)
    
    fig, ax = plt.subplots(figsize=(40,40))
    ax.scatter(x_vector[0],x_vector[1],label = "x_vector")
    ax.scatter(x_alpha[0],x_alpha[1],label = "x_alpha")
    for i,coordinate in enumerate(x_beta):
        ax.plot([x_alpha[0],coordinate[0]],[x_alpha[1],coordinate[1]], label = "x_beta {}".format(i), marker = "o")
    circle = plt.Circle((x_vector),Spatial_averaging_radius, fill = False, label = "Averaging volume")
    ax.add_artist(circle)
    ax.legend()
    ax.grid()
    plt.savefig("Task_6_bond_intersection_points.png")

    assert 1==2
    
@pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_get_minimum_distance_btw_linesegment_point():
    """
        Tests whether the minimum distance calculated from the function matches with 
        the geometrical value using Geogebra --> https://www.geogebra.org
    """
    
    Line_point_1 = np.array([[-4,5,0],
                             [-4,5,0],
                             [-4,5,0],
                             [2,3,0],
                             [2,3,0],
                             [2,3,0],
                             [0.5,-6,0],
                             [0.5,-6,0],
                             [0.5,-6,0]])
    Line_point_2 = np.array([[0.34,-0.78,0],
                             [-0.23,1,0],
                             [-9,-4.4,0],
                             [0.34,-0.78,0],
                             [-0.23,1,0],
                             [-9,-4.4,0],
                             [0.34,-0.78,0],
                             [-0.23,1,0],
                             [-9,-4.4,0]])  
    
    Point = np.array([0,0,0])
        
    Minimum_distance = get_minimum_distance_btw_linesegment_point(Line_point_1, Line_point_2, Point)
    Geometrical_value = [0.2,1.03,5.88,0.62,1.03,1.37,0.85,0.13,5.83] # Calculated using Geobra geometrical tool 
    
    fig, ax = plt.subplots(figsize=(40,40))
    ax.scatter(range(len(Line_point_1)), Minimum_distance, label = "Geometrically calculated using Geogebra", s = 1000)
    ax.scatter(range(len(Line_point_1)), Geometrical_value, label = "Calculated minimum distance", s = 500)
    ax.set(xlabel = "x coordinate " + r"$(\AA)$", ylabel = "y coordinate " + r"$(\AA)$")
    plt.suptitle("Minimum distance between a point and a line segment", weight = "bold")
    ax.grid()
    ax.legend()
    plt.savefig("Task_6_test_get_minimum_distance_btw_linesegment_point.png")

# @pytest.mark.skip("Task - 6 -- Currently not testing this")
def test_get_unique_bonds():
    """
        Tests unique pairs of atom neighbors from the array of neighbors list
    """
    
    Coordinates = np.array([[0,0],
                            [-1,1],
                            [1,-1],
                            [1,1],
                            [-1,-1]])
    
    Neighbors_list = np.array([[1,2,3,4],
                               [0,3,4],
                               [0,3,4],
                               [0,1,2],
                               [0,1,2]]) # Non-periodic chain
    
    Neighbors_list_unique = get_unique_bonds(Neighbors_list)
        
    fig, ax = plt.subplots(2,figsize=(40,40))
    for i,pair in enumerate(Neighbors_list):
        line_style = ["-","--",":","-.","dotted"]
        line_width = [15,12,10,8,5]
        if len(pair) > 1:
            for pair_i in pair:
                ax[0].plot([Coordinates[i,0],Coordinates[pair_i,0]], [Coordinates[i,1],Coordinates[pair_i,1]], label = "Pair - {}".format(i), marker = "o", linewidth = line_width[i], linestyle = line_style[i], markersize = 50)
        else:
            ax[0].plot([Coordinates[i,0],Coordinates[pair,0]], [Coordinates[i,1],Coordinates[pair,1]], label = "Pair - {}".format(i), marker = "o", linewidth = line_width[i], linestyle = line_style[i], markersize = 50)
        
    for j,pair_unique in enumerate(Neighbors_list_unique.astype(int)):
        pair = pair_unique[pair_unique>0]
        
        if len(pair) > 1:
            for pair_i in pair:
                ax[1].plot([Coordinates[j,0],Coordinates[pair_i,0]], [Coordinates[j,1],Coordinates[pair_i,1]], label = "Pair - {}".format(i), marker = "o", linewidth = line_width[j], linestyle = line_style[j], markersize = 50)
        elif len(pair) != 0:
            ax[1].plot([Coordinates[j,0],Coordinates[pair,0]], [Coordinates[j,1],Coordinates[pair,1]], label = "Pair - {}".format(i), marker = "o", linewidth = line_width[j], linestyle = line_style[j], markersize = 50)
    circle_1 = plt.Circle((0,0),1, fill = False, label = "Averaging volume")
    circle_2 = plt.Circle((0,0),1, fill = False, label = "Averaging volume")
    ax[0].set_title("Bonds of all atoms", weight = "bold")
    ax[1].set_title("Unique bonds within the averaging volume", weight = "bold")
    ax[0].set(xlabel = "x coordinate " + r"$(\AA)$", ylabel = "y coordinate " + r"$(\AA)$")
    ax[1].set(xlabel = "x coordinate " + r"$(\AA)$", ylabel = "y coordinate " + r"$(\AA)$")
    ax[0].add_artist(circle_1)
    ax[1].add_artist(circle_2)
    ax[0].grid()
    plt.suptitle("Unique bonds function from neighbors list", weight = "bold")
    # ax[0].legend()
    ax[1].grid()
    # ax[1].legend()
    plt.savefig("Task_6_test_get_unique_bonds.png")
    
    assert 1==2