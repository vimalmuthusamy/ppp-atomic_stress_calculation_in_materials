# coding: utf-8
# Test cases for Task_2_Volume_discretization routine
import pytest
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from Task_2_Volume_discretization import get_voronoi_tessellation
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
import tess as ts # Voro ++ library 
np.set_printoptions(threshold=sys.maxsize)

"""===========================PBC + 3D periodic points test cases==========================="""

@pytest.mark.skip("Not currently testing this")
def test_3d_volume_equal_distant_points_all_pbc_tessellation():
    """
        Description:
        ------------        
        Calculates the volume per point for a regularly spaced (equal distance between points in x,y & z) 3D points and the box is set to be periodic
        
        OBJECTIVE: 
        1) All the volume per point values should be same
        2) Computed volume per point should be equal to the analytical cuboid volume per point
    """
    print(test_3d_volume_equal_distant_points_all_pbc_tessellation.__doc__)
    
    Box_x = [0.0,3.0]
    Box_y = [0.0,3.0]
    Box_z = [0.0,3.0]
    Distance_between_points_x = 1
    Distance_between_points_y = 1
    Distance_between_points_z = 1
    Boundary_conditions = ["pp","pp","pp"]
    
    x = np.arange(Box_x[0],(Box_x[1]),Distance_between_points_x)
    y = np.arange(Box_y[0],(Box_y[1]),Distance_between_points_y)
    z = np.arange(Box_z[0],(Box_z[1]),Distance_between_points_z)
    
    Analytical_volume_per_point = Distance_between_points_x * Distance_between_points_y * Distance_between_points_z
    
    No_of_points = len(x) * len(y) * len(z)
    x_coordinates, y_coordinates, z_coordinates = np.meshgrid(x,y,z)
    x_coordinates, y_coordinates, z_coordinates = x_coordinates.flatten(), y_coordinates.flatten(), z_coordinates.flatten()
    
    Coordinates = np.zeros((No_of_points,3))
    
    for i in range(No_of_points):
        Coordinates[i] = np.array([x_coordinates[i],y_coordinates[i],z_coordinates[i]])

    Volume_per_point, Face_areas_per_point, Neighbors_per_point, Vertices_per_face = get_voronoi_tessellation(Coordinates,Box_x[0],Box_x[1],Box_y[0],
                                                                                                              Box_y[1],Box_z[0],Box_z[1], Boundary_conditions)
    
    assert np.equal(Volume_per_point, Volume_per_point[0]).all() # Checks whether all the values of the array are the same
    assert Volume_per_point[0] == Analytical_volume_per_point 

@pytest.mark.skip("Not currently testing this")
def test_3d_volume_unequal_distant_points_all_pbc_tessellation():
    """
        Description:
        ------------        
        Calculates the volume per point for a regularly spaced (equal distance between points in x,y & z) 3D points and the box is set to be periodic
        
        OBJECTIVE: 
        1) All the volume per point values should be same
        2) Computed volume per point should be equal to the analytical cuboid volume per point
    """
    print(test_3d_volume_unequal_distant_points_all_pbc_tessellation.__doc__)
    
    Box_x = [0.0,3.0]
    Box_y = [0.0,6.0]
    Box_z = [0.0,9.0]
    Distance_between_points_x = 1
    Distance_between_points_y = 2
    Distance_between_points_z = 3
    Boundary_conditions = ["pp","pp","pp"]
    
    x = np.arange(Box_x[0],(Box_x[1]),Distance_between_points_x)
    y = np.arange(Box_y[0],(Box_y[1]),Distance_between_points_y)
    z = np.arange(Box_z[0],(Box_z[1]),Distance_between_points_z)
    
    Analytical_volume_per_point = Distance_between_points_x * Distance_between_points_y * Distance_between_points_z
    
    No_of_points = len(x) * len(y) * len(z)
    x_coordinates, y_coordinates, z_coordinates = np.meshgrid(x,y,z)
    x_coordinates, y_coordinates, z_coordinates = x_coordinates.flatten(), y_coordinates.flatten(), z_coordinates.flatten()
    
    Coordinates = np.zeros((No_of_points,3))
    
    for i in range(No_of_points):
        Coordinates[i] = np.array([x_coordinates[i],y_coordinates[i],z_coordinates[i]])

    Volume_per_point, Face_areas_per_point, Neighbors_per_point, Vertices_per_face = get_voronoi_tessellation(Coordinates,Box_x[0],Box_x[1],Box_y[0],
                                                                                                              Box_y[1],Box_z[0],Box_z[1], Boundary_conditions)
    
    assert np.equal(Volume_per_point, Volume_per_point[0]).all() # Checks whether all the values of the array are the same
    assert Volume_per_point[0] == Analytical_volume_per_point 

"""===========================3D periodic points test case + Free boundary condition along z direction==========================="""

@pytest.mark.skip("Not currently testing this")
def test_3d_volume_equal_distant_points_x_y_pbc_tessellation():
    """
        Description:
        ------------        
        Calculates the volume per point for a regularly spaced (equal distance between points in x,y & z) 3D points and the box is set to be periodic
        
        OBJECTIVE: 
        1) Volume for points at the boundary along the z direction should be equal to half the volume of the interior points
    """
    print(test_3d_volume_equal_distant_points_x_y_pbc_tessellation.__doc__)
    
    Distance_between_points_x = 1
    Distance_between_points_y = 1
    Distance_between_points_z = 1
    
    Box_x = [0,10]
    Box_y = [0,10]
    Box_z = [0,(10 + 1e-15)] # Box dimension along z is added with the small value 1e-15 to have equal contribution to the pointw at the bottom & top of the box along z; Error results when the final z dimension lies on the boundary
    Boundary_conditions = ["pp","pp","f"]
    
    x = np.arange(Box_x[0],(Box_x[1]),Distance_between_points_x)
    y = np.arange(Box_y[0],(Box_y[1]),Distance_between_points_y)
    z = np.arange(Box_z[0],(Box_z[1]),Distance_between_points_z) # Box end dimension is changed due to non-pbc in z
    
    No_of_points = len(x) * len(y) * len(z)
    x_coordinates, y_coordinates, z_coordinates = np.meshgrid(x,y,z)
    x_coordinates, y_coordinates, z_coordinates = x_coordinates.flatten(), y_coordinates.flatten(), z_coordinates.flatten()
    
    Coordinates = np.zeros((No_of_points,3))
    
    for i in range(No_of_points):
        Coordinates[i] = np.array([x_coordinates[i],y_coordinates[i],z_coordinates[i]])
    
    Volume_per_point, Face_areas_per_point, Neighbors_per_point, Vertices_per_face = get_voronoi_tessellation(Coordinates,Box_x[0],Box_x[1],Box_y[0],
                                                                                                              Box_y[1],Box_z[0],Box_z[1], Boundary_conditions)       
    fig, ax = plt.subplots(figsize = (40,20))
    vol_plot = ax.scatter(Coordinates[0:,0],Coordinates[0:,2], c = Volume_per_point, s = 500, cmap = plt.cm.get_cmap("rainbow"))
    cbar = plt.colorbar(vol_plot)
    cbar.set_label("Volume per point ("+r"$\AA^3$"+")", fontsize = 30)
    ax.grid()
    ax.set_title("Free boundary condition along z", fontsize = 40, fontweight = "bold")
    ax.set_ylabel("z coordinate" + r"($\AA$)", fontsize = 30)
    ax.set_xlabel("x coordinate" + r"($\AA$)", fontsize = 30)
    cbar.ax.tick_params(labelsize = 20)
    ax.set(xticks = x, yticks = z)
    plt.tick_params(labelsize = 20)
    plt.savefig("test_3d_volume_equal_distant_points_x_y_pbc_tessellation.png")
    
    assert np.equal(Volume_per_point, Volume_per_point[0]).all() == False # Checks whether all the values of the array are not the same due to non-periodic boundary conditions in z direction

"""===========================Volume computed in lammps vs Volume from Task_2 function using tess==========================="""


# test_3d_volume_equal_distant_points_all_pbc_tessellation()
# test_3d_volume_unequal_distant_points_all_pbc_tessellation()
# test_3d_volume_equal_distant_points_x_y_pbc_tessellation()