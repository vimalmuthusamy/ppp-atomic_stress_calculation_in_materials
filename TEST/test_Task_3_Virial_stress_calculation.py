# Program to test Local virial stress calculation
# coding: utf-8
import pytest
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from Task_1_Interatomic_force_calculation import get_interatomic_force
from Task_2_Volume_discretization import get_voronoi_tessellation
from Task_3_Virial_stress_calculation import get_virial_stress_tensor
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D
np.set_printoptions(threshold=sys.maxsize) 

@pytest.mark.skip("Task - 3 -- Currently not testing this")
def get_force_routine_inputs_Ni_lammps_comparison():
    Atomic_coordinates_file_name = "dump.Ni_SX_no_pbc_l_3.custom.100" 
    # Atomic_coordinates_file_name = "dump.Ni_SX_cohesive_energy_l_3.custom.2"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    Velocity_components = np.genfromtxt(Atomic_coordinates_file_name, usecols = (4,5,6), skip_header = 9) # According to LAMMPS dump file format
    Force_components = np.genfromtxt(Atomic_coordinates_file_name, usecols = (7,8,9), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    Stress_components = np.genfromtxt(Atomic_coordinates_file_name, usecols = (10,11,12,13,14,15), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    Volume_per_atom = np.genfromtxt(Atomic_coordinates_file_name, usecols = (16), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    # Box_x_start, Box_x_end = round(Box_dimensions[0][0],5), round(Box_dimensions[0][1],5)
    # Box_y_start, Box_y_end = round(Box_dimensions[1][0],5), round(Box_dimensions[1][1],5) 
    # Box_z_start, Box_z_end = round(Box_dimensions[2][0],5), round(Box_dimensions[2][1],5) 
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1]
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions, Force_components, Stress_components, Volume_per_atom, Velocity_components, Box_x_start, Box_y_start, Box_z_start


# @pytest.mark.skip("Task - 3 -- Currently not testing this")
# def test_Ni_stress_lammps_comparison(get_force_routine_inputs_Ni_lammps_comparison):
# def test_Ni_stress_lammps_comparison():
    
#     """
#         Description:
#         ------------
#         Compares the computed stress components with the LAMMPS results for a single crystal Ni sample with free boundary conditions on all sides.
#         The input crystal is not relaxed since only the values are compared.
#     """
#     print(test_Ni_stress_lammps_comparison.__doc__)
    
#     Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
#     Element = "Ni"

#     Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions,Force_lammps, Stress_lammps, Volume_per_atom_lammps, Velocity_vector, Box_x_start, Box_y_start, Box_z_start = get_force_routine_inputs_Ni_lammps_comparison()

#     # Task - 1
#     Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
#     # Task - 2
#     Atomic_coordinates_voronoi = Atomic_coordinates.T
#     Volume_per_atom, Face_areas_per_atom, Neighbors_per_atom, Vertices_per_face = get_voronoi_tessellation(Atomic_coordinates_voronoi,Box_x_start,Box_x_end,Box_y_start,Box_y_end,Box_z_start,Box_z_end, Boundary_conditions)
    
#     fig, ay = plt.subplots()
#     ay.scatter(range(No_of_atoms),Volume_per_atom_lammps, label = "Lammps", s = 100)
#     ay.scatter(range(No_of_atoms),Volume_per_atom,label = "Computed")
#     ay.grid()
#     ay.legend()
    
    
#     Velocity_tensor, Force_radius_tensor, Virial_stress_tensor, Symmetric_virial_stress_tensor = get_virial_stress_tensor(No_of_atoms,Force_vector,Distance_vector,Atomic_mass,Velocity_vector,Volume_per_atom)
#     print("Stress:",Symmetric_virial_stress_tensor[:4,0])
#     print("Lammps stress:",Stress_lammps[0][:4]//Volume_per_atom_lammps[:4])
    
    
#     fig, ax = plt.subplots(3,2)
#     ax[0,0].scatter(range(No_of_atoms),Stress_lammps[0]/Volume_per_atom_lammps,label = "Lammps")
#     ax[0,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,0],label = r"$\sigma_{11}")
#     ax[0,1].scatter(range(No_of_atoms),Stress_lammps[1]/Volume_per_atom_lammps,label = "Lammps")
#     ax[0,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,1],label = r"$\sigma_{22}")
#     ax[1,0].scatter(range(No_of_atoms),Stress_lammps[2]/Volume_per_atom_lammps,label = "Lammps")
#     ax[1,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,2],label = r"$\sigma_{33}")
#     ax[1,1].scatter(range(No_of_atoms),Stress_lammps[3]/Volume_per_atom_lammps,label = "Lammps")
#     ax[1,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,3],label = r"$\sigma_{12}")
#     ax[2,0].scatter(range(No_of_atoms),Stress_lammps[4]/Volume_per_atom_lammps,label = "Lammps")
#     ax[2,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,4],label = r"$\sigma_{13}")
#     ax[2,1].scatter(range(No_of_atoms),Stress_lammps[5]/Volume_per_atom_lammps,label = "Lammps")
#     ax[2,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,5],label = r"$\sigma_{23}")
#     ax[0,0].grid()
#     ax[0,0].legend()
#     ax[0,1].grid()
#     ax[0,1].legend()
#     ax[1,0].grid()
#     ax[1,0].legend()
#     ax[1,1].grid()
#     ax[1,1].legend()
#     ax[2,0].grid()
#     ax[2,0].legend()
#     ax[2,1].grid()
#     ax[2,1].legend()
#     plt.show()

@pytest.mark.skip("Task - 3 -- Currently not testing this")
def test_Ni_stress_lammps_comparison():
    
    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Ni"

    Atomic_coordinates = np.array([[0,0,0],
                                   [1.76,0,0],
                                   [3.52,0,0],
                                   [5.28,0,0],
                                   [7.04,0,0],
                                   [8.8,0,0],
                                   [10.56,0,0],
                                   [10.56+1.76,0,0],
                                   [10.56+1.76*2,0,0],])
    
    # Box_x_end, Box_y_end, Box_z_end = (10.56+1.76) * 1.1,(10.56+1.76) * 1.1,(10.56+1.76) * 1.1
    Box_x_end, Box_y_end, Box_z_end = (10.56+1.76*3) ,(10.56+1.76*3) ,(10.56+1.76*3) 
    No_of_atoms = len(Atomic_coordinates)
    Boundary_conditions = ["f","pp","pp"]
    
    Atomic_coordinates = Atomic_coordinates.T
    # Atomic_coordinates[0] *= 1.1
    print("Coordinates:",Atomic_coordinates)
    # Task - 1
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy,Plot, Neighbors = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    # Task - 2
    # Atomic_coordinates_voronoi = Atomic_coordinates.T
    # Volume_per_atom, Face_areas_per_atom, Neighbors_per_atom, Vertices_per_face = get_voronoi_tessellation(Atomic_coordinates_voronoi,Box_x_start,Box_x_end,Box_y_start,Box_y_end,Box_z_start,Box_z_end, Boundary_conditions)
    
    # fig, ay = plt.subplots()
    # ay.scatter(range(No_of_atoms),Volume_per_atom_lammps, label = "Lammps", s = 100)
    # ay.scatter(range(No_of_atoms),Volume_per_atom,label = "Computed")
    # ay.grid()
    # ay.legend()
    
    Volume_per_atom = np.ones((No_of_atoms))
    Velocity_vector = np.zeros((No_of_atoms,3))
    Velocity_tensor, Force_radius_tensor, Virial_stress_tensor, Symmetric_virial_stress_tensor = get_virial_stress_tensor(No_of_atoms,Force_vector,Distance_vector,Atomic_mass,Velocity_vector,Volume_per_atom, Neighbors)    
    
    print("Distance vector:",Distance_vector,"\nForce vector:",Force_vector,"\nVirial stress:",Symmetric_virial_stress_tensor)
    # fig, ax = plt.subplots(3,2)
    # ax[0,0].scatter(range(No_of_atoms),Stress_lammps[0]/Volume_per_atom_lammps,label = "Lammps")
    # ax[0,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,0],label = r"$\sigma_{11}")
    # ax[0,1].scatter(range(No_of_atoms),Stress_lammps[1]/Volume_per_atom_lammps,label = "Lammps")
    # ax[0,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,1],label = r"$\sigma_{22}")
    # ax[1,0].scatter(range(No_of_atoms),Stress_lammps[2]/Volume_per_atom_lammps,label = "Lammps")
    # ax[1,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,2],label = r"$\sigma_{33}")
    # ax[1,1].scatter(range(No_of_atoms),Stress_lammps[3]/Volume_per_atom_lammps,label = "Lammps")
    # ax[1,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,3],label = r"$\sigma_{12}")
    # ax[2,0].scatter(range(No_of_atoms),Stress_lammps[4]/Volume_per_atom_lammps,label = "Lammps")
    # ax[2,0].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,4],label = r"$\sigma_{13}")
    # ax[2,1].scatter(range(No_of_atoms),Stress_lammps[5]/Volume_per_atom_lammps,label = "Lammps")
    # ax[2,1].scatter(range(No_of_atoms),Symmetric_virial_stress_tensor[0:,5],label = r"$\sigma_{23}")
    # ax[0,0].grid()
    # ax[0,0].legend()
    # ax[0,1].grid()
    # ax[0,1].legend()
    # ax[1,0].grid()
    # ax[1,0].legend()
    # ax[1,1].grid()
    # ax[1,1].legend()
    # ax[2,0].grid()
    # ax[2,0].legend()
    # ax[2,1].grid()
    # ax[2,1].legend()
    # plt.show()
    
