# coding: utf-8
# Testing program for Interatomic force calculation routine
import pytest
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
# from Task_1_Interatomic_force_calculation_test_code import get_interatomic_force
from Task_1_Interatomic_force_calculation_test_code import get_interatomic_force
from Task_1_Interatomic_force_calculation_test_code import get_periodic_boundary_condition
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D
np.set_printoptions(threshold=sys.maxsize)

# Unit conversion constants
ev_to_joule = 1.602176565e-19
Angstrom_to_m = 1e-10

"""===========================Cohesive energy & Surface energy (100), (110), (111)==========================="""

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ag_SX_cohesive_and_surface_energies(get_force_routine_inputs_Ag,get_force_routine_inputs_Ag_surface_energy_100,get_force_routine_inputs_Ag_surface_energy_110,get_force_routine_inputs_Ag_surface_energy_111):
    """
        Description:
        ------------
        Calculates the Cohesive energy and the Surface energies (100), (110), (111) of Ag single crystal and compares it with the values in the reference paper.
        Compares the Lattice_constant, Cutoff_radius and Atomic_mass values to the values in the EAM/ALLOY potential file.
        
        Reference Paper : An embedded-atom potential for the Cu–Ag system, P L Williams , Y Mishin and J C Hamilton; Modelling Simul. Mater. Sci. Eng. 14 (2006) 817–833 – Published 30 May 2006
    """
    print(test_Ag_SX_cohesive_and_surface_energies.__doc__)

    Potential_file_name = "Ag.Mishin09.eam.alloy"
    Element = "Ag"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Cohesive_energy = (Potential_energy / No_of_atoms)
    Cohesive_energy_from_potential = -2.85 # eV
    print("Cohesive energy calculated: {} eV --- Cohesive energy from paper: {} eV --- difference = {} %".format(Cohesive_energy, Cohesive_energy_from_potential,((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)))
    
    """-------------------Surface - 100-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag_surface_energy_100
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_100 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_100_from_potential = 940 # mJ/m^2
    print("Surface energy (100) calculated: {} mJ/m^2 --- Surface energy (100) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_100, Surface_energy_100_from_potential,((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag_surface_energy_110
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_x_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_110 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_110_from_potential = 1017 # mJ/m^2
    print("Surface energy (110) calculated: {} mJ/m^2 --- Surface energy (110) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_110, Surface_energy_110_from_potential,((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)))
    
    """-------------------Surface - 111-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag_surface_energy_111
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_x_end) # 2 * len(y) * len(z)
        
    Surface_energy_111 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_111_from_potential = 862 # mJ/m^2
    print("Surface energy (111) calculated: {} mJ/m^2 --- Surface energy (111) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_111, Surface_energy_111_from_potential,((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)))    
    
    assert Lattice_constant == 0.4090000000E+01
    assert Cutoff_radius == 0.5995011000293092E+01
    assert Atomic_mass == 0.1078680000E+03
    assert Cohesive_energy == Cohesive_energy_from_potential, "% difference = {} %".format((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)
    assert Surface_energy_100 == Surface_energy_100_from_potential, "% difference = {} %".format((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)
    assert Surface_energy_110 == Surface_energy_110_from_potential, "% difference = {} %".format((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)
    assert Surface_energy_111 == Surface_energy_111_from_potential, "% difference = {} %".format((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Al_SX_cohesive_and_surface_energies(get_force_routine_inputs_Al,get_force_routine_inputs_Al_surface_energy_100,get_force_routine_inputs_Al_surface_energy_110,get_force_routine_inputs_Al_surface_energy_111):
    """
        Description:
        ------------
        Calculates the Cohesive energy and the Surface energies (100), (110), (111) of Al single crystal and compares it with the values in the reference paper.
        Compares the Lattice_constant, Cutoff_radius and Atomic_mass values to the values in the EAM/ALLOY potential file.
        
        Reference Paper : Development of an interatomic potential for the Ni-Al system, G.P. Purja Pun and Y. Mishin, Vol. 89, Nos. 34–36, 1–21 December 2009, 3245–3267
        Al in "Mishin-Ni-Al-2009.eam.alloy" file is located as the second element -> To test the file import correctness for effective charge function array, Embedding 
        function array and Electron density function array
    """
    print(test_Al_SX_cohesive_and_surface_energies.__doc__)

    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Al"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Al
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name,Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Cohesive_energy = (Potential_energy / No_of_atoms)
    Cohesive_energy_from_potential = -3.36 # eV
    print("Cohesive energy calculated: {} eV --- Cohesive energy from paper: {} eV --- difference = {} %".format(Cohesive_energy, Cohesive_energy_from_potential,((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)))
    
    """-------------------Surface - 100-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Al_surface_energy_100
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_100 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_100_from_potential = 943.59 # mJ/m^2
    print("Surface energy (100) calculated: {} mJ/m^2 --- Surface energy (100) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_100, Surface_energy_100_from_potential,((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Al_surface_energy_110
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_x_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_110 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_110_from_potential = 1006.03 # mJ/m^2
    print("Surface energy (110) calculated: {} mJ/m^2 --- Surface energy (110) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_110, Surface_energy_110_from_potential,((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)))
    
    """-------------------Surface - 111-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Al_surface_energy_111
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_x_end) # 2 * len(y) * len(z)
        
    Surface_energy_111 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_111_from_potential = 870.52 # mJ/m^2
    print("Surface energy (111) calculated: {} mJ/m^2 --- Surface energy (111) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_111, Surface_energy_111_from_potential,((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)))    
    
    assert Cohesive_energy == Cohesive_energy_from_potential, "% difference = {} %".format((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)
    assert Surface_energy_100 == Surface_energy_100_from_potential, "% difference = {} %".format((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)
    assert Surface_energy_110 == Surface_energy_110_from_potential, "% difference = {} %".format((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)
    assert Surface_energy_111 == Surface_energy_111_from_potential, "% difference = {} %".format((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)
        
@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ni_SX_cohesive_and_surface_energies(get_force_routine_inputs_Ni,get_force_routine_inputs_Ni_surface_energy_100,get_force_routine_inputs_Ni_surface_energy_110,get_force_routine_inputs_Ni_surface_energy_111):
    """
        Description:
        ------------
        Calculates the Cohesive energy and the Surface energies (100), (110), (111) of Ni single crystal and compares it with the values in the reference paper.
        Compares the Lattice_constant, Cutoff_radius and Atomic_mass values to the values in the EAM/ALLOY potential file.
        
        Reference Paper : Development of an interatomic potential for the Ni-Al system, G.P. Purja Pun and Y. Mishin, Vol. 89, Nos. 34–36, 1–21 December 2009, 3245–3267
    """
    print(test_Ni_SX_cohesive_and_surface_energies.__doc__)
    
    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Ni"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ni
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Cohesive_energy = (Potential_energy / No_of_atoms)
    Cohesive_energy_from_potential = -4.45 # eV
    print("Cohesive energy calculated: {} eV --- Cohesive energy from paper: {} eV --- difference = {} %".format(Cohesive_energy, Cohesive_energy_from_potential,((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)))
    
    """-------------------Surface - 100-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ni_surface_energy_100
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_100 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_100_from_potential = 1936.04 # mJ/m^2
    print("Surface energy (100) calculated: {} mJ/m^2 --- Surface energy (100) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_100, Surface_energy_100_from_potential,((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ni_surface_energy_110
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_x_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_110 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_110_from_potential = 2087.08 # mJ/m^2
    print("Surface energy (110) calculated: {} mJ/m^2 --- Surface energy (110) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_110, Surface_energy_110_from_potential,((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)))
    
    """-------------------Surface - 111-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ni_surface_energy_111
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_x_end) # 2 * len(y) * len(z)
        
    Surface_energy_111 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_111_from_potential = 1759.1 # mJ/m^2
    print("Surface energy (111) calculated: {} mJ/m^2 --- Surface energy (111) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_111, Surface_energy_111_from_potential,((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)))    
    
    assert Cohesive_energy == Cohesive_energy_from_potential, "% difference = {} %".format((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)
    assert Surface_energy_100 == Surface_energy_100_from_potential, "% difference = {} %".format((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)
    assert Surface_energy_110 == Surface_energy_110_from_potential, "% difference = {} %".format((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)
    assert Surface_energy_111 == Surface_energy_111_from_potential, "% difference = {} %".format((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)
    
@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Pt_SX_cohesive_and_surface_energies(get_force_routine_inputs_Pt,get_force_routine_inputs_Pt_surface_energy_100,get_force_routine_inputs_Pt_surface_energy_110,get_force_routine_inputs_Pt_surface_energy_111):
    """
        Description:
        ------------
        Calculates the Cohesive energy and the Surface energies (100), (110), (111) of Pt single crystal and compares it with the values in the reference paper.
        Compares the Lattice_constant, Cutoff_radius and Atomic_mass values to the values in the EAM potential file. 
        
        Reference Paper : Embedded-atom-method functions for the fcc metals Cu, Ag, Au, Ni, Pd, Pt, and their alloys - S. M. Foiles, M. I. Baskes, and M. S. Daw; PHYSICAL REVIEW B, VOLUME 33, NUMBER 12, 15 JUNE 1986
    """
    print(test_Pt_SX_cohesive_and_surface_energies.__doc__)
    
    Potential_file_name = "Pt_u3.eam"
    Element = "Pt"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Pt
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Cohesive_energy = (Potential_energy / No_of_atoms)
    Cohesive_energy_from_potential = -5.77 # eV
    print("Cohesive energy calculated: {} eV --- Cohesive energy from paper: {} eV --- difference = {} %".format(Cohesive_energy, Cohesive_energy_from_potential,((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)))

    """-------------------Surface - 100-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Pt_surface_energy_100
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_100 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_100_from_potential = 1650 # mJ/m^2
    print("Surface energy (100) calculated: {} mJ/m^2 --- Surface energy (100) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_100, Surface_energy_100_from_potential,((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Pt_surface_energy_110
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_x_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_110 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_110_from_potential = 1750 # mJ/m^2
    print("Surface energy (110) calculated: {} mJ/m^2 --- Surface energy (110) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_110, Surface_energy_110_from_potential,((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)))
    
    """-------------------Surface - 111-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Pt_surface_energy_111
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_x_end) # 2 * len(y) * len(z)
        
    Surface_energy_111 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_111_from_potential = 1440 # mJ/m^2
    print("Surface energy (111) calculated: {} mJ/m^2 --- Surface energy (111) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_111, Surface_energy_111_from_potential,((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)))    

    assert Lattice_constant == 3.9200
    assert Cutoff_radius == 5.3000000000000114e+00
    assert Atomic_mass == 195.09    
    assert Cohesive_energy == Cohesive_energy_from_potential, "% difference = {} %".format((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)
    assert Surface_energy_100 == Surface_energy_100_from_potential, "% difference = {} %".format((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)
    assert Surface_energy_110 == Surface_energy_110_from_potential, "% difference = {} %".format((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)
    assert Surface_energy_111 == Surface_energy_111_from_potential, "% difference = {} %".format((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)
    
@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Au_SX_cohesive_and_surface_energies(get_force_routine_inputs_Au,get_force_routine_inputs_Au_surface_energy_100,get_force_routine_inputs_Au_surface_energy_110,get_force_routine_inputs_Au_surface_energy_111):
    """
        Description:
        ------------
        Calculates the Cohesive energy and the Surface energies (100), (110), (111) of Au single crystal and compares it with the values in the reference paper.
        Compares the Lattice_constant, Cutoff_radius and Atomic_mass values to the values in the EAM potential file.
        
        Reference Paper : Embedded-atom-method functions for the fcc metals Cu, Ag, Au, Ni, Pd, Pt, and their alloys - S. M. Foiles, M. I. Baskes, and M. S. Daw; PHYSICAL REVIEW B, VOLUME 33, NUMBER 12, 15 JUNE 1986
    """
    print(test_Au_SX_cohesive_and_surface_energies.__doc__)
    
    Potential_file_name = "Au_u3.eam"
    Element = "Au"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Au
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Cohesive_energy = (Potential_energy / No_of_atoms)    
    Cohesive_energy_from_potential = -3.93 # eV
    print("Cohesive energy calculated: {} eV --- Cohesive energy from paper: {} eV --- difference = {} %".format(Cohesive_energy, Cohesive_energy_from_potential,((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)))

    """-------------------Surface - 100-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Au_surface_energy_100
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_100 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_100_from_potential = 918 # mJ/m^2
    print("Surface energy (100) calculated: {} mJ/m^2 --- Surface energy (100) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_100, Surface_energy_100_from_potential,((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)))
    
    """-------------------Surface - 110-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Au_surface_energy_110
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_x_end*Box_z_end) # 2 * len(y) * len(z)
        
    Surface_energy_110 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_110_from_potential = 980 # mJ/m^2
    print("Surface energy (110) calculated: {} mJ/m^2 --- Surface energy (110) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_110, Surface_energy_110_from_potential,((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)))
    
    """-------------------Surface - 111-------------------"""
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Au_surface_energy_111
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy_surface = get_interatomic_force(Potential_file_name,Element,Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    Surface_area = 2 * (Box_y_end*Box_x_end) # 2 * len(y) * len(z)
        
    Surface_energy_111 = ((Potential_energy_surface - (Cohesive_energy * No_of_atoms)) / (Surface_area)) * ((ev_to_joule * 1000) / (Angstrom_to_m * Angstrom_to_m))
    Surface_energy_111_from_potential = 790 # mJ/m^2
    print("Surface energy (111) calculated: {} mJ/m^2 --- Surface energy (111) from paper: {} mJ/m^2 --- difference = {} %".format(Surface_energy_111, Surface_energy_111_from_potential,((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)))    
      
    assert Cohesive_energy == Cohesive_energy_from_potential, "% difference = {} %".format((Cohesive_energy_from_potential - Cohesive_energy) / Cohesive_energy_from_potential * 100)
    assert Surface_energy_100 == Surface_energy_100_from_potential, "% difference = {} %".format((Surface_energy_100_from_potential - Surface_energy_100) / Surface_energy_100_from_potential * 100)
    assert Surface_energy_110 == Surface_energy_110_from_potential, "% difference = {} %".format((Surface_energy_110_from_potential - Surface_energy_110) / Surface_energy_110_from_potential * 100)
    assert Surface_energy_111 == Surface_energy_111_from_potential, "% difference = {} %".format((Surface_energy_111_from_potential - Surface_energy_111) / Surface_energy_111_from_potential * 100)


"""===========================Interatomic force==========================="""

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ag_zero_force(get_force_routine_inputs_Ag):
    """
        Description:
        ------------
        Test case for verifying "0" resultant force output for all the atoms of a single homogeneous crystal. Atomic coordinates & Box dimensions are
        rounded off to 5 decimals due to floating point errors in LAMMPS dump file output.
    """
    print(test_Ag_zero_force.__doc__)
    
    Potential_file_name = "Ag.Mishin09.eam.alloy"
    Element = "Ag"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag
    Atomic_coordinates = np.around(Atomic_coordinates,5)
    Box_x_end = round(Box_x_end,5)
    Box_y_end = round(Box_y_end,5)
    Box_z_end = round(Box_z_end,5)
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    fig, ax = plt.subplots(nrows = 3, figsize = (20,20))
    ax[0].scatter(range(No_of_atoms), Force_per_atom[0:,0], label = "Force - x component")
    ax[1].scatter(range(No_of_atoms), Force_per_atom[0:,1], label = "Force - y component", c = "r")
    ax[2].scatter(range(No_of_atoms), Force_per_atom[0:,2], label = "Force - z component", c = "g")
    ax[0].set(title = "Force x component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[1].set(title = "Force y component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[2].set(title = "Force z component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    plt.suptitle("Interatomic force calculation - {}".format(Potential_file_name))
    plt.subplots_adjust(hspace = 0.3)
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.savefig("test_Ag_zero_force.png")

    assert np.allclose(Force_per_atom[0:,0],10000, atol = 1e-8), "x-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,1],0.0, atol = 1e-8), "y-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,2],0.0, atol = 1e-8), "z-component of the force is greater than the tolerance limit of 1e-8"

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ag_diatom_force(get_force_routine_inputs_diatom):
    """
        Description:
        ------------
        Calculates the energy and force components for a 2 atom system and compares it with the computed di-atom properties for the potential. Force for the reference is 
        calculated by taking the derivative of the given potential energy.

        Reference URL: https://www.ctcms.nist.gov/potentials/entry/2006--Williams-P-L-Mishin-Y-Hamilton-J-C--Ag/2006--Williams-P-L--Ag--LAMMPS--ipr1/diatom.txt 
    """
    print(test_Ag_diatom_force.__doc__)
    
    Potential_file_name = "Ag.Mishin09.eam.alloy"
    Element = "Ag"   

    """----------Data import from diatom potential energy file----------"""

    Radius, Energy = np.genfromtxt("Ag_diatom_potential_energy.txt", comments="#",unpack = True)[0:,1:] # [0:,1:] is to avoid the radius & element strings in the file
    Delta_radius = np.gradient(Radius)[0] # Since all the values are the same
    Force_computed = np.gradient(Energy,0.02, edge_order = 2)

    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_diatom
    Potential_energy_diatom = np.zeros(len(Radius))
    Force_diatom = np.zeros(len(Radius))
    
    for i,distance in (enumerate(Radius)):
        Atomic_coordinates[0][0] = distance
        Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
        Potential_energy_diatom[i] = Potential_energy
        Force_diatom[i] = Force_per_atom[1,0] # Taking only the x component of the second atom
        
    fig, ax = plt.subplots(nrows = 2, figsize = (15,15))
    ax[0].plot(Radius,Energy, label = "Potential data")
    ax[0].plot(Radius,Potential_energy_diatom, label = "Computed")
    ax[1].plot(Radius,Force_computed, label = "Potential data")
    ax[1].plot(Radius,Force_diatom, label = "Computed")
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    plt.savefig("test_Ag_diatom_force.png")
    
@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ag_force_z(get_force_routine_inputs_Ag_force_z):
    """
        Description:
        ------------
        Calculates the force components of the single crystal with periodic boundary conditions in x & y directions and free boundary condition in the z-direction. 
        Qualitative verification of the force calculation routine with the plots showing "0" x & y force components and force component-z values rising from
        "0" to non-zero from the interior of the crystal to the surface in z-direction.
    """
    print(test_Ag_force_z.__doc__)

    Potential_file_name = "Ag.Mishin09.eam.alloy"
    Element = "Ag"   

    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag_force_z
    Atomic_coordinates = np.around(Atomic_coordinates,5)
    Box_x_end = round(Box_x_end,5)
    Box_y_end = round(Box_y_end,5)
    Box_z_end = round(Box_z_end,5)
    Boundary_conditions[2] = "ff"
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)

    fig = plt.figure(figsize = (20,20))
    ax = Axes3D(fig)
    plot = ax.scatter(Atomic_coordinates[0],Atomic_coordinates[1],Atomic_coordinates[2], c = Force_per_atom[0:,2], vmin=np.amin(Force_per_atom[0:,2]),vmax=np.amax(Force_per_atom[0:,2]), cmap = "rainbow", s = 50)
    cbar = plt.colorbar(plot)
    cbar.set_label("Force y component ("+r"$\frac{eV}{\AA}$"+")")
    ax.legend()
    ax.grid()
    plt.savefig("test_Ag_force_z-Force_z_3d.png")

    fig, ax = plt.subplots(figsize = (20,20))
    plot = ax.scatter(Atomic_coordinates[0],Atomic_coordinates[1], c = Force_per_atom[0:,0], vmin=np.amin(Force_per_atom[0:,2]),vmax=np.amax(Force_per_atom[0:,2]), cmap = "rainbow")
    cbar = plt.colorbar(plot)
    cbar.set_label("Force x component ("+r"$\frac{eV}{\AA}$"+")")
    ax.legend()
    ax.grid()
    ax.set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")", title = "Force x component")
    plt.savefig("test_Ag_force_z-Force_x.png")

    fig, ay = plt.subplots(figsize = (20,20))
    plot = ay.scatter(Atomic_coordinates[0],Atomic_coordinates[1], c = Force_per_atom[0:,1], vmin=np.amin(Force_per_atom[0:,2]),vmax=np.amax(Force_per_atom[0:,2]), cmap = "rainbow")
    cbar = plt.colorbar(plot)
    cbar.set_label("Force y component ("+r"$\frac{eV}{\AA}$"+")")
    ay.legend()
    ay.grid()
    ay.set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")", title = "Force y component")
    plt.savefig("test_Ag_force_z-Force_y.png")
    
    fig, az = plt.subplots(figsize = (20,20))
    plot = az.scatter(Atomic_coordinates[0],Atomic_coordinates[2], c = Force_per_atom[0:,2], vmin=np.amin(Force_per_atom[0:,2]),vmax=np.amax(Force_per_atom[0:,2]), cmap = "rainbow")
    cbar = plt.colorbar(plot)
    cbar.set_label("Force z component ("+r"$\frac{eV}{\AA}$"+")")
    az.legend()
    az.grid()
    az.set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "z coordinate ("+r"$\AA$"+")", title = "Force z component")
    plt.savefig("test_Ag_force_z-Force_z.png")
        
@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_periodic_boundary_conditions(get_force_routine_inputs_Ag_pbc_test):
    """
        Description:
        ------------
        Testing the periodic boundary conditions function by calculating the distance between the atom at (0,0,0) (corner atom) to all atoms.
        Output -> Atoms at a distance more than half the box dimensions is displaced to the other side of the reference atom thus
        making the reference atom inside the bulk.
    """
    print(test_periodic_boundary_conditions.__doc__)

    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ag_pbc_test
    Reference_atom_x = Atomic_coordinates[0,0]
    Reference_atom_y = Atomic_coordinates[1,0]
    New_atom_coordinates = np.zeros_like(Atomic_coordinates)
    New_atom_coordinates[0] = get_periodic_boundary_condition((Atomic_coordinates[0] - Reference_atom_x),Box_x_end) 
    New_atom_coordinates[1] = get_periodic_boundary_condition((Atomic_coordinates[1] - Reference_atom_y),Box_y_end) 
    
    fig, ax = plt.subplots(figsize = (20,20))
    ax.add_patch(Rectangle((Reference_atom_x, Reference_atom_y), Box_x_end, Box_y_end, facecolor="grey",  alpha = 0.5, label = "Simulation box"))
    ax.scatter(Atomic_coordinates[0], Atomic_coordinates[1], label = "Actual coordinates")
    ax.scatter(New_atom_coordinates[0], New_atom_coordinates[1], label = "Coordinates after PBC", s = 10)
    ax.scatter(Reference_atom_x,Reference_atom_y, label = "Reference atom", c = "r", s = 50)
    ax.set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")", title = "Periodic Boundary Conditions test case")
    ax.legend()
    ax.grid()
    plt.savefig("test_periodic_boundary_conditions.png")
    
@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Al_zero_force(get_force_routine_inputs_Al):
    """
        Description:
        ------------
        Test case for verifying "0" resultant force output for all the atoms of a single homogeneous crystal. Atomic coordinates & Box dimensions are
        rounded off to 5 decimals due to floating point errors in LAMMPS dump file output.
    """
    print(test_Al_zero_force.__doc__)

    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Al"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Al
    Atomic_coordinates = np.around(Atomic_coordinates,5)
    Box_x_end = round(Box_x_end,5)
    Box_y_end = round(Box_y_end,5)
    Box_z_end = round(Box_z_end,5)
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    fig, ax = plt.subplots(nrows = 3, figsize = (20,20))
    ax[0].scatter(range(No_of_atoms), Force_per_atom[0:,0], label = "Force - x component")
    ax[1].scatter(range(No_of_atoms), Force_per_atom[0:,1], label = "Force - y component", c = "r")
    ax[2].scatter(range(No_of_atoms), Force_per_atom[0:,2], label = "Force - z component", c = "g")
    ax[0].set(title = "Force x component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[1].set(title = "Force y component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[2].set(title = "Force z component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    plt.suptitle("Interatomic force calculation - {}".format(Potential_file_name))
    plt.subplots_adjust(hspace = 0.3)
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.savefig("test_Al_zero_force.png")
    
    assert np.allclose(Force_per_atom[0:,0],0.0, atol = 1e-8), "x-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,1],0.0, atol = 1e-8), "y-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,2],0.0, atol = 1e-8), "z-component of the force is greater than the tolerance limit of 1e-8"

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ni_zero_force(get_force_routine_inputs_Ni):
    """
        Description:
        ------------
        Test case for verifying "0" resultant force output for all the atoms of a single homogeneous crystal. Atomic coordinates & Box dimensions are
        rounded off to 5 decimals due to floating point errors in LAMMPS dump file output.
    """
    print(test_Ni_zero_force.__doc__)
    
    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Ni"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Ni
    Atomic_coordinates = np.around(Atomic_coordinates,5)
    Box_x_end = round(Box_x_end,5)
    Box_y_end = round(Box_y_end,5)
    Box_z_end = round(Box_z_end,5)
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    fig, ax = plt.subplots(nrows = 3, figsize = (20,20))
    ax[0].scatter(range(No_of_atoms), Force_per_atom[0:,0], label = "Force - x component")
    ax[1].scatter(range(No_of_atoms), Force_per_atom[0:,1], label = "Force - y component", c = "r")
    ax[2].scatter(range(No_of_atoms), Force_per_atom[0:,2], label = "Force - z component", c = "g")
    ax[0].set(title = "Force x component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[1].set(title = "Force y component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[2].set(title = "Force z component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    plt.suptitle("Interatomic force calculation - {}".format(Potential_file_name))
    plt.subplots_adjust(hspace = 0.3)
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    # plt.savefig("test_Ni_zero_force.png")
    plt.savefig("test_Ni_zero_force_FinDiff.png")
    
    assert np.allclose(Force_per_atom[0:,0],0.0, atol = 1e-8), "x-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,1],0.0, atol = 1e-8), "y-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,2],0.0, atol = 1e-8), "z-component of the force is greater than the tolerance limit of 1e-8"

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Ni_force_lammps_comparison(get_force_routine_inputs_Ni_lammps_comparison):
    """
        Description:
        ------------
        Compares the computed force components with the LAMMPS results for a single crystal Ni sample with free boundary conditions on all sides.
        The input crystal is not relaxed since only the values are compared.
    """
    print(test_Ni_force_lammps_comparison.__doc__)
    
    Potential_file_name = "Mishin-Ni-Al-2009.eam.alloy"
    Element = "Ni"

    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions,Force_lammps, Stress_lammps, Volume_lammps, Velocity_lammps = get_force_routine_inputs_Ni_lammps_comparison
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    fig, ax = plt.subplots(nrows = 3, figsize = (20,20))
    ax[0].scatter(range(No_of_atoms), Force_per_atom[0:,0], label = "Computed", s = 80)
    ax[0].scatter(range(No_of_atoms), Force_lammps[0], label = "LAMMPS")
    ax[1].scatter(range(No_of_atoms), Force_per_atom[0:,1], label = "Computed", s = 80)
    ax[1].scatter(range(No_of_atoms), Force_lammps[1], label = "LAMMPS")
    ax[2].scatter(range(No_of_atoms), Force_per_atom[0:,2], label = "Computed", s = 80)
    ax[2].scatter(range(No_of_atoms), Force_lammps[2], label = "LAMMPS")
    ax[0].set(title = "Force x component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")") 
    ax[1].set(title = "Force y component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")") 
    ax[2].set(title = "Force z component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")") 
    plt.suptitle("Interatomic force calculation - {}".format(Potential_file_name))
    plt.subplots_adjust(hspace = 0.3)
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.savefig("test_Ni_force_lammps_comparison.png")

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Pt_zero_force(get_force_routine_inputs_Pt):
    """
        Description:
        ------------
        Test case for verifying "0" resultant force output for all the atoms of a single homogeneous crystal. Atomic coordinates & Box dimensions are
        rounded off to 5 decimals due to floating point errors in LAMMPS dump file output.
    """
    print(test_Pt_zero_force.__doc__)

    Potential_file_name = "Pt_u3.eam"
    Element = "Pt"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Pt
    Atomic_coordinates = np.around(Atomic_coordinates,5)
    Box_x_end = round(Box_x_end,5)
    Box_y_end = round(Box_y_end,5)
    Box_z_end = round(Box_z_end,5)
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    fig, ax = plt.subplots(nrows = 3, figsize = (20,20))
    ax[0].scatter(range(No_of_atoms), Force_per_atom[0:,0], label = "Force - x component")
    ax[1].scatter(range(No_of_atoms), Force_per_atom[0:,1], label = "Force - y component", c = "r")
    ax[2].scatter(range(No_of_atoms), Force_per_atom[0:,2], label = "Force - z component", c = "g")
    ax[0].set(title = "Force x component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[1].set(title = "Force y component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[2].set(title = "Force z component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    plt.suptitle("Interatomic force calculation - {}".format(Potential_file_name))
    plt.subplots_adjust(hspace = 0.3)
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.savefig("test_Pt_zero_force.png")
    
    assert np.allclose(Force_per_atom[0:,0],0.0, atol = 1e-8), "x-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,1],0.0, atol = 1e-8), "y-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,2],0.0, atol = 1e-8), "z-component of the force is greater than the tolerance limit of 1e-8"


@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Au_zero_force(get_force_routine_inputs_Au):
    """
        Description:
        ------------
        Test case for verifying "0" resultant force output for all the atoms of a single homogeneous crystal. Atomic coordinates & Box dimensions are
        rounded off to 5 decimals due to floating point errors in LAMMPS dump file output.
    """
    print(test_Au_zero_force.__doc__)
    
    Potential_file_name = "Au_u3.eam"
    Element = "Au"
    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_Au
    Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
    
    fig, ax = plt.subplots(nrows = 3, figsize = (20,20))
    ax[0].scatter(range(No_of_atoms), Force_per_atom[0:,0], label = "Force - x component")
    ax[1].scatter(range(No_of_atoms), Force_per_atom[0:,1], label = "Force - y component", c = "r")
    ax[2].scatter(range(No_of_atoms), Force_per_atom[0:,2], label = "Force - z component", c = "g")
    ax[0].set(title = "Force x component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[1].set(title = "Force y component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    ax[2].set(title = "Force z component", xlabel = "Atom ID", ylabel = "Force ("+r"$\frac{eV}{\AA}$"+")", ylim = [-1e-6,1e-6]) 
    plt.suptitle("Interatomic force calculation - {}".format(Potential_file_name))
    plt.subplots_adjust(hspace = 0.3)
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[2].grid()
    ax[2].legend()
    plt.savefig("test_Au_zero_force.png")
    
    assert np.allclose(Force_per_atom[0:,0],0.0, atol = 1e-8), "x-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,1],0.0, atol = 1e-8), "y-component of the force is greater than the tolerance limit of 1e-8"
    assert np.allclose(Force_per_atom[0:,2],0.0, atol = 1e-8), "z-component of the force is greater than the tolerance limit of 1e-8"

@pytest.mark.skip("Task - 1 -- Currently not testing this")
def test_Au_diatom_force(get_force_routine_inputs_diatom):
    """
        Description:
        ------------
        Calculates the energy and force components for a 2 atom system and compares it with the computed di-atom properties for the potential. Force for the reference is 
        calculated by taking the derivative of the given potential energy.

        Reference URL: https://www.ctcms.nist.gov/potentials/entry/1986--Foiles-S-M-Baskes-M-I-Daw-M-S--Ag-Au-Cu-Ni-Pd-Pt/1986--Foiles-S-M--Ag-Au-Cu-Ni-Pd-Pt--LAMMPS--ipr1/diatom.txt      
    """
    print(test_Au_diatom_force.__doc__)
    
    Potential_file_name = "Au_u3.eam"
    Element = "Au"   

    """----------Data import from diatom potential energy file----------"""

    Radius, Energy = np.genfromtxt("Au_eam_diatom_potential_energy.txt", comments="#",unpack = True, usecols = (0,7))[0:,1:] # [0:,1:] is to avoid the radius & element strings in the file
    Delta_radius = np.gradient(Radius)[0] # Since all the values are the same
    Force_computed = np.gradient(Energy,0.02, edge_order = 2)

    Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_force_routine_inputs_diatom
    Potential_energy_diatom = np.zeros(len(Radius))
    Force_diatom = np.zeros(len(Radius))
    
    for i,distance in (enumerate(Radius)):
        Atomic_coordinates[0][0] = distance
        Lattice_constant, Atomic_mass, Cutoff_radius, Distance_vector, Force_vector, Force_per_atom, Potential_energy = get_interatomic_force(Potential_file_name, Element, Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions)
        Potential_energy_diatom[i] = Potential_energy
        Force_diatom[i] = Force_per_atom[1,0] # Taking only the x component of the second atom
        
    fig, ax = plt.subplots(nrows = 2, figsize = (20,20))
    ax[0].plot(Radius,Energy, label = "Potential data")
    ax[0].plot(Radius,Potential_energy_diatom, label = "Computed")
    ax[1].plot(Radius,Force_computed, label = "Potential data")
    ax[1].plot(Radius,Force_diatom, label = "Computed")
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    plt.savefig("test_Au_diatom_force.png")