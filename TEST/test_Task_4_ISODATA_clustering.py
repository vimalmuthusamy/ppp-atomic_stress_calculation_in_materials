# Program to test Local virial stress calculation
# coding: utf-8
import pytest
import numpy as np
import scipy as sp
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
# from Task_1_Interatomic_force_calculation import get_interatomic_force
# from Task_2_Volume_discretization import get_voronoi_tessellation
# from Task_3_Virial_stress_calculation import get_virial_stress_tensor
from Task_4_ISODATA_clustering_test_code import get_clustered_data
from Task_4_ISODATA_clustering_test_code import get_mean_sd 
from Task_4_ISODATA_clustering_test_code import clean_empty_array 
from Task_4_ISODATA_clustering_test_code import get_cluster_sorted
from Task_4_ISODATA_clustering_test_code import get_average_distance
from Task_4_ISODATA_clustering_test_code import get_cluster_split
from Task_4_ISODATA_clustering_test_code import get_cluster_merge
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import sklearn
from sklearn.datasets import make_blobs # To generate synthetic data sets
from mpl_toolkits.mplot3d import Axes3D
plt.rcParams.update({"font.size":30}) # Changes the size of the entire plot
np.set_printoptions(threshold=sys.maxsize)
from itertools import product
from textwrap import wrap
from scipy import stats

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_1D_clustered_data():
    x = np.random.randint(0,2,(300,2))
    x[:,1] = 0 # Explicitly making it as only 1D
    
    Input_data_unstandardized = x
    Inter_cluster_radius_threshold = 0.1
    Standard_deviation_threshold = 0.5
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0]])
    Boundary_conditions = ["f","f"]
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions)

    fig, ax = plt.subplots(2,figsize = (40,40))
    ax[0].scatter(range(len(x)),x[:,0], label = "Original cluster", s = 200, c = "g")

    x_plot = np.arange(len(x))
    x_plot_1 = x_plot[:len(Clustered_data[0])]
    x_plot_2 = x_plot[len(Clustered_data[0]):]

    x_plots = np.array([x_plot_1,x_plot_2])

    for j,cluster in enumerate(Clustered_data):
        ax[1].scatter(x_plots[j],cluster[:,0],label = "Predicted cluster - {}".format(j), s= 200)
        
    ax[0].grid()
    ax[0].legend()
    ax[1].grid()
    ax[1].legend()
    ax[0].set(xlabel = "Point ID", ylabel = "Values")
    ax[1].set(xlabel = "Point ID", ylabel = "Values")
    plt.subplots_adjust(hspace = 0.4)
    plt.suptitle("Original clusters vs Predicted clusters --- 1D data", weight = "bold")
    plt.savefig("test_1D_clustered_data.png", dpi = 150)


@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_2D_clustered_data():
    x,y = make_blobs(10000,2, centers=10,center_box = (-30,20), random_state = 14654) # y - cluster numbers
    
    Input_data_unstandardized = x
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6*Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0]])
    Boundary_conditions = ["f","f","f"]    
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions)
    print("log message:",Log_message)

    Original_clusters_list = []
    fig, ax = plt.subplots(figsize = (15,15))
    for i in (np.unique(y)):
        index = np.argwhere(y == i).flatten()
        ax.scatter(x[index,0],x[index,1], label = "O-{}".format(i), s = 400, alpha = 0.8)
        Original_clusters_list.append(x[index])
    
    for j,cluster in enumerate(Clustered_data):
        ax.scatter(cluster[:,0],cluster[:,1],label = "Predicted cluster - {}".format(j), s= 20)

    """**************************Checking whether all points are clustered as per original cluster**************************"""
    # for predicted_cluster_index in Cluster_indices:
    #     print(y[predicted_cluster_index])
    #     assert 1 == 2
    #     # assert (y[predicted_cluster_index] == y[predicted_cluster_index]).all(), "Not all points in the cluster are equal to the points in original cluster !!"

    ax.grid()
    # ax.legend()
    ax.set(xlabel = "x coordinate", ylabel = "y coordinate", title = "\n".join(wrap("Original clusters vs Predicted clusters - 2D data; Predicted No:{} Thresholds SD : {} IC distance : {} ".format(len(Clustered_data),Standard_deviation_threshold, Inter_cluster_radius_threshold),60)))
    plt.savefig("test_2D_clustered_data.png", dpi = 150)
    plt.show()

    assert 1==2
    

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_3D_clustered_data():
    x,y = make_blobs(3000,3,centers=8, center_box = (-20,20), random_state = 4587)
    
    Input_data_unstandardized = x
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6*Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 2
    Box_dimensions = np.array([[0,0],[0,0],[0,0]])
    Boundary_conditions = ["f","f","f"]
        
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions)
    
    fig = plt.figure(figsize = (40,40))
    ax = fig.add_subplot(111,projection = "3d")

    for i in (np.unique(y)):
        index = np.argwhere(y == i).flatten()
        ax.scatter(x[index,0],x[index,1],x[index,2], label = "Original cluster - {}".format(i), s = 500)
    
    for j,cluster in enumerate(Clustered_data):
        ax.scatter(cluster[:,0],cluster[:,1],cluster[:,2],label = "Predicted cluster - {}".format(j), s= 100)
        
    ax.grid()
    # ax.legend(loc = (0,0))
    ax.set_title("\n".join(wrap("Original clusters vs Predicted clusters - 3D data; Predicted No:{} Thresholds SD : {} IC distance : {} ".format(No_of_clusters,Standard_deviation_threshold, Inter_cluster_radius_threshold),60)), weight = "bold")
    ax.set_xlabel("x coordinate", labelpad = 50)
    ax.set_ylabel("y coordinate", labelpad = 50)
    ax.set_zlabel("z coordinate", labelpad = 50)
    plt.savefig("test_3D_clustered_data_clusters_{}.png".format(No_of_clusters), dpi = 250)
    plt.show()

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_get_mean_sd():
    x,y = make_blobs(10,2, centers=3, random_state=560) # y - cluster numbers

    No_of_dimensions = 2
    x = np.array([[-1,0],[2,2],[1,0],[2,0]])
    x_input = np.array_split(x,No_of_dimensions,axis = 0)

    print("x_input:",x_input)

    
    No_of_clusters,Mean, Standard_deviation = get_mean_sd(x_input,No_of_dimensions)

    print("Input cluster:",x_input)
    print("Mean:",Mean,"\nSD:",Standard_deviation,"\nNo of clusters:",No_of_clusters)

    fig, ax = plt.subplots()
    for i,cluster in enumerate(x_input):
        ax.scatter(cluster[:,0],cluster[:,1], label =i )
    ax.scatter(Mean[:,0],Mean[:,1],label = "Mean", s = 100)
    ax.grid()
    ax.legend()
    plt.savefig("test_get_mean_sd.png")

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_clean_empty_array():
    x = np.array([[-1,0],[2,2],[1,0],[2,0]])

    No_splits = 10
    x_input = np.array_split(x,No_splits,axis = 0)

    print("x_input before:",x_input)

    Cleaned_x_input = clean_empty_array(x_input)

    print("Cleaned x_input:",Cleaned_x_input)

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def testget_cluster_sorted():

    No_of_dimensions = 2
    x,y = make_blobs(100,2, centers=2) # y - cluster numbers
    No_of_clusters = 2
    np.random.shuffle(x)
    Cluster = np.array_split(x,No_of_clusters,axis = 0)
    Cluster_unsorted = np.copy(Cluster)
    Mean = np.array([[-5,-5],[5,5]])

    Sorted_cluster = get_cluster_sorted(No_of_clusters, Cluster, Mean)
    fig, ax = plt.subplots(figsize = (40,40))

    for i,cluster in enumerate(Cluster_unsorted):
        ax.scatter(cluster[:,0],cluster[:,1], label = "Unsorted - {}".format(i), s = 100*5)
    
    for j,sort_cluster in enumerate(Sorted_cluster):
        ax.scatter(sort_cluster[:,0],sort_cluster[:,1], label = "Sorted - {}".format(i), s = 100*2)
    ax.scatter(Mean[:,0],Mean[:,1], label = "Centroid",c = "r", s = 200*5)
    ax.legend()
    ax.grid()
    plt.savefig("get_cluster_sorted.png")
    plt.show()

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_get_average_distance():

    Cluster = [np.array([[1,0],[2,0],[3,0],[4,0],[5,0]]), np.array([[1,0],[2,0],[3,0],[4,0],[5,0]]) * -1]

    Mean = np.array([[2.5,0],[20,0]])
    No_of_clusters = 2
    No_of_data = 10

    Total_average_distance, No_of_data_per_cluster,Cluster_average_distance = get_average_distance(Cluster,No_of_clusters, Mean, No_of_data)

    print("Total_average_distance:",Total_average_distance)
    print("No_of_data_per_cluster:",No_of_data_per_cluster)
    print("Cluster_average_distance:",Cluster_average_distance)

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_get_cluster_split():

    Cluster = [np.array([[1,0],[2,0],[3,0],[4,0],[5,0]]), np.array([[1,0],[2,0],[3,0],[4,0],[5,0]]) * -1]

    Cluster_unsplit = np.copy(Cluster)
    Cluster_average_distance = np.array([2.5,1.8])
    Minimum_no_of_data_per_cluster = 2
    No_of_data_per_cluster = np.array([5,5])
    Standard_deviation = np.array([[21,1.5],[1.3,1.8]])
    Standard_deviation_threshold = 1.5
    Total_average_distance = 2

    Cluster_split = get_cluster_split(Standard_deviation, Standard_deviation_threshold, Cluster, Cluster_average_distance, No_of_data_per_cluster, Minimum_no_of_data_per_cluster, Total_average_distance)

    print("SD threshold:",Standard_deviation_threshold)
    print("Standard_deviation of the clusters:",Standard_deviation)
    print("Cluster_average_distance:",Cluster_average_distance)
    print("Cluster Unsplit:",Cluster_unsplit)
    print("Cluster after split:",Cluster_split)

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_get_cluster_merge():

    Cluster = [np.array([[1,0],[2,0],[3,0],[4,0],[5,0]]), np.array([[1,0],[2,0],[3,0],[4,0],[5,0]]) * -1, np.arange(10).reshape(-1,2)*3]
    Cluster_unmerged = np.copy(Cluster)
    No_of_clusters = 3
    Inter_cluster_radius_threshold = 30
    Mean = np.array([[-100,0],[-2.5,0],[15,0]])
    Box_bound = 0
    Boundary_conditions = 0

    Cluster_merged = get_cluster_merge(Mean, Inter_cluster_radius_threshold, No_of_clusters, Cluster, Box_bound = Box_bound, Boundary_conditions = Boundary_conditions)

    print("Cluster_unmerged:",Cluster_unmerged)
    print("Cluster_merged:",Cluster_merged)

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_IC_SD_thresholds():
    No_of_dimensions = 2
    x1,y1 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (-10,2) ,random_state=560)
    x2,y2 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (10,20), random_state=560)
    x3,y3 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (20,30), random_state=560)
    x4,y4 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (30,40), random_state=560)
    x5,y5 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (40,50), random_state=560)
    x6,y6 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (50,60), random_state=560)
    x7,y7 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (60,70), random_state=560)
    x8,y8 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (70,80), random_state=560)
    x9,y9 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (80,90), random_state=560)
    x10,y10 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (-10,3), random_state=560)
    x11,y11 = make_blobs(1000,No_of_dimensions, centers=1,cluster_std=2,center_box = (1,2) ,random_state=560)

    No_of_clusters = 11
    Input_data = stats.zscore(np.concatenate((x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11)),axis = 0)
    Cluster = np.array_split(Input_data,No_of_clusters)
    No_of_clusters,Mean,Standard_deviation = get_mean_sd(Cluster,No_of_dimensions)
    Inter_cluster_distance = np.repeat(Mean[np.newaxis,:],len(Mean),axis = 0) - Mean[:,np.newaxis]

    fig, ax = plt.subplots(figsize = (20,20))

    for i,cluster in enumerate(Cluster):
        ax.scatter(cluster[:,0],cluster[:,1], label = "Cluster - {}".format(i))
    
    Inter_cluster_radius = np.zeros((No_of_clusters,No_of_clusters))
    
    for i in range(No_of_clusters):
        Inter_cluster_radius[i] = np.linalg.norm(Inter_cluster_distance[i],axis = 1) # Finds the straight line distance of cluster to cluster distance (EUCLIDEAN norm)

    print("Standard_deviation:",Standard_deviation)
    print("Mean:",Mean)
    print("Inter_cluster_radius:",Inter_cluster_radius)

    Cluster_1_10_11 = [np.concatenate((Cluster[0],Cluster[-1],Cluster[-2]))]
    No_of_clusters,Mean_1_10_11,Standard_deviation_1_10_11 = get_mean_sd(Cluster_1_10_11,No_of_dimensions)

    Cluster_1_10 = [np.concatenate((Cluster[0],Cluster[-1]))]
    No_of_clusters,Mean_1_10,Standard_deviation_1_10 = get_mean_sd(Cluster_1_10,No_of_dimensions)

    Cluster_1_11 = [np.concatenate((Cluster[0],Cluster[-2]))]
    No_of_clusters,Mean_1_11,Standard_deviation_1_11 = get_mean_sd(Cluster_1_11,No_of_dimensions)

    print("Mean_1_10_11:",Mean_1_10_11)
    print("Standard_deviation_1_10_11:",Standard_deviation_1_10_11)

    print("Mean_1_10:",Mean_1_10)
    print("Standard_deviation_1_10:",Standard_deviation_1_10)

    print("Mean_1_11:",Mean_1_11)
    print("Standard_deviation_1_11:",Standard_deviation_1_11)
    
    ax.set(xlabel = "x coordinate", ylabel = "y coordinate ", title = "Inter_cluster_distance vs Standard_deviation relation")
    ax.grid()
    ax.legend()
    plt.savefig("test_IC_SD_thresholds.png")
    # plt.show()


    # Input_data = stats.zscore(np.concatenate((x1,x2)),axis = 0)
    # Cluster = np.array_split(Input_data,2)
    # Cluster_without_split = np.array_split(Input_data,1)
    # No_of_clusters = len(Cluster)
    # No_of_clusters_wo_split = len(Cluster_without_split)

    # No_of_clusters,Mean, Standard_deviation = get_mean_sd(Cluster,No_of_dimensions)
    # No_of_clusters_wo_split,Mean_wo_split, Standard_deviation_wo_split = get_mean_sd(Cluster_without_split,No_of_dimensions)
    # Inter_cluster_distance = np.repeat(Mean[np.newaxis,:],len(Mean),axis = 0) - Mean[:,np.newaxis]
    # Inter_cluster_distance_wo_split = np.repeat(Mean_wo_split[np.newaxis,:],len(Mean_wo_split),axis = 0) - Mean_wo_split[:,np.newaxis]
    # Inter_cluster_radius = np.zeros((No_of_clusters,No_of_clusters))
    
    # for i in range(No_of_clusters):
    #     Inter_cluster_radius[i] = np.linalg.norm(Inter_cluster_distance[i],axis = 1) # Finds the straight line distance of cluster to cluster distance (EUCLIDEAN norm)
    
    # Inter_cluster_radius_wo_split = np.zeros((No_of_clusters_wo_split,No_of_clusters_wo_split))
    
    # for i in range(No_of_clusters_wo_split):
    #     Inter_cluster_radius_wo_split[i] = np.linalg.norm(Inter_cluster_distance_wo_split[i],axis = 1) # Finds the straight line distance of cluster to cluster distance (EUCLIDEAN norm)

    # Inter_cluster_radius_threshold = 1
    
    # print("Cluster:",Cluster)
    # print("Mean:",Mean)
    # print("Standard_deviation:",Standard_deviation)
    # print("Inter-cluster radius:",Inter_cluster_radius)

    # print("\n\Mean_wo_split:",Mean_wo_split)
    # print("Standard_deviation_wo_split:",Standard_deviation_wo_split)
    # print("Inter_cluster_radius_wo_split:",Inter_cluster_radius_wo_split)

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_IC_SD_threshold_relation():

    Cluster_centers = np.arange(-5,5,0.01)
    Standard_deviation = 2

    No_of_points = 100
    Cluster_1 = np.random.normal(0,Standard_deviation,No_of_points)
    fig, ax = plt.subplots(2,figsize=(40,40))
    Mean = np.zeros_like(Cluster_centers)
    Std = np.zeros_like(Cluster_centers)
    for i,mean in enumerate(Cluster_centers):
        Data = np.random.normal(mean,Standard_deviation,No_of_points)
        Merged_cluster = np.concatenate((Cluster_1,Data))
        Mean[i] = np.mean(mean)
        Std[i] = np.std(Merged_cluster)
    ax[0].plot(Mean,Std, marker = "o")
    ax[1].plot(Mean,Std, marker = "o", c = "g")
    ax[0].axhline(Standard_deviation, label = "Cluster 1 Standard deviation", linestyle = "--", color = "red")
    ax[0].set(xlabel = "Inter-cluster distance", ylabel = "Standard deviation", title = "Inter_cluster_distance vs Standard_deviation for the merged clusters (No of data points: {})".format(No_of_points),xticks = np.arange(-5,5,0.5))
    ax[0].grid()
    ax[0].legend()
    ax[1].axhline(Standard_deviation, label = "Cluster 1 Standard deviation", linestyle = "--", color = "red")
    ax[1].set(xlabel = "Inter-cluster distance", ylabel = "Standard deviation", xticks = np.arange(-1,1,0.2), xlim = [-1,1], ylim = [1.8,2.2])# , ylim = [1.95,2.1])
    ax[1].grid()
    ax[1].legend()
    plt.savefig("test_IC_SD_threshold_relation.png")
    plt.show()


@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_9D_zero_stress(get_fcc_atomic_coordinates):
    
    Atomic_cooridnates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_fcc_atomic_coordinates
    Stress_data = np.zeros((No_of_atoms,6)) # 6D symmetric zero stress tensor
    Input_data_unstandardized = np.append(Atomic_cooridnates, Stress_data, axis = 1)
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6 * Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 20
    Box_dimensions = np.array([[0,Box_x_end],[0,Box_y_end],[0,Box_z_end]])
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions)

    fig1 = plt.figure(figsize = (40,40))
    ax = fig1.add_subplot(111,projection = "3d")
    ax.scatter(Atomic_cooridnates[:,0],Atomic_cooridnates[:,1],Atomic_cooridnates[:,2], label = "Al fcc coordinates")
    ax.set(xlabel = "x coordinate" + r"$(\AA)$", ylabel = "y coordinate" + r"$(\AA)$", zlabel = "z coordinate" + r"$(\AA)$")
    ax.grid()
    ax.legend()
    ax.set_title("Al FCC atomic_cooridnates", weight = "bold")
    ax.set_xlabel("x coordinate" + r"$(\AA)$", labelpad = 50)
    ax.set_ylabel("y coordinate" + r"$(\AA)$", labelpad = 50)
    ax.set_zlabel("z coordinate" + r"$(\AA)$", labelpad = 50)
    fig1.savefig("test_9D_fcc_atomic_coordinates.png", dpi = 250)

    fig2 = plt.figure(figsize = (40,40))
    fig3, az = plt.subplots(figsize=(40,40))
    ay = fig2.add_subplot(111,projection = "3d")
    for i,cluster in enumerate(Clustered_data):
        ay.scatter(cluster[:,0],cluster[:,1],cluster[:,2], label = "Cluster - {}".format(i), s = 2000)
        az.scatter(cluster[:,0],cluster[:,1],s = 2000)
    
    ay.grid()
    ay.legend(loc = (0,0))
    az.grid()
    az.legend()
    ay.set_title("\n".join(wrap("Atomic_cooridnates + Zero stress (9D);Thresholds SD : {} IC distance : {} ".format(Standard_deviation_threshold, Inter_cluster_radius_threshold),60)), weight = "bold")
    az.set_title("\n".join(wrap("Atomic_cooridnates + Zero stress (9D);Thresholds SD : {} IC distance : {} ".format(Standard_deviation_threshold, Inter_cluster_radius_threshold),60)), weight = "bold")
    ay.set_xlabel("x coordinate" + r"$(\AA)$", labelpad = 50)
    ay.set_ylabel("y coordinate" + r"$(\AA)$", labelpad = 50)
    ay.set_zlabel("z coordinate" + r"$(\AA)$", labelpad = 50)
    fig2.savefig("test_9D_fcc_atomic_coordinates_zero_stress_3D_{}.png".format(No_of_clusters), dpi = 250)
    az.set_xlabel("x coordinate" + r"$(\AA)$", labelpad = 50)
    az.set_ylabel("y coordinate" + r"$(\AA)$", labelpad = 50)
    fig3.savefig("test_9D_fcc_atomic_coordinates_zero_stress_2D_{}.png".format(No_of_clusters), dpi = 250)
    
    print("No of clusters: ",No_of_clusters,"Iterations:",Iteration)
    plt.show()

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_initial_no_of_clusters(get_fcc_atomic_coordinates):
    
    Atomic_cooridnates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_fcc_atomic_coordinates
    Stress_data = np.zeros((No_of_atoms,6)) # 6D symmetric zero stress tensor
    Input_data_unstandardized = np.append(Atomic_cooridnates, Stress_data, axis = 1)
    Standard_deviation_threshold = 0.8
    Inter_cluster_radius_threshold = 0.6 * Standard_deviation_threshold
    # Inter_cluster_radius_threshold = 0.4
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 20
    Box_dimensions = np.array([[0,Box_x_end],[0,Box_y_end],[0,Box_z_end]])
    # Box_dimensions = np.array([[0,0],[0,0],[0,0]])
    
    fig, ax = plt.subplots(figsize=(40,40))

    Total_no_of_clusters = np.zeros(10)
    for initial_no_of_clusters in range(1,11):
        Clustered_data,Total_no_of_clusters[initial_no_of_clusters-1], Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions, initial_no_of_clusters)
        print("Initial no of clusters:",initial_no_of_clusters,"SSE diff:",np.diff(SSE), "Iteration:",Iteration)
    ax.plot(range(1,11),Total_no_of_clusters, marker = "o")
    ax.set(xlabel = "Initial no of clusters", ylabel = "No of resulting clusters", title = "Initial no of clusters vs No of clusters after convergence")
    ax.grid()
    ax.legend()
    plt.savefig("test_initial_no_of_clusters.png")
 
    plt.show()

    assert 1==2

@pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_pbc():
    
    No_of_dimensions = 2
    Cluster1, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (1,2) ,random_state=7987)
    Cluster2, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (48,2) ,random_state=7987)
    Cluster3, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (0,52) ,random_state=7987)
    Cluster4, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (48,48) ,random_state=7987)
    Cluster5, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (25,2) ,random_state=7987)
    Cluster6, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (25,48) ,random_state=7987)
    Cluster7, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (48,25) ,random_state=7987)
    Cluster8, y = make_blobs(100,No_of_dimensions, centers=1,cluster_std=1,center_box = (2,25) ,random_state=7987)


    Input_data = np.concatenate((Cluster1,Cluster2,Cluster3,Cluster4,Cluster5,Cluster6,Cluster7,Cluster8), axis = 0)
    Input_data = np.append(Input_data, np.zeros(len(Input_data)).reshape(-1,1), axis = 1)
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6 * Standard_deviation_threshold
    Inter_cluster_radius_threshold = 0.8
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 20
    Box_x_end = 50
    Box_y_end = 50
    Box_z_end = 0
    Box_dimensions = np.array([[0,Box_x_end],[0,Box_y_end],[0,Box_z_end]])

    Boundary_conditions = [["ff","ff","ff"],["pp","pp","pp"]]

    fig, ax = plt.subplots(2,figsize = (40,40))
    for j,boundary_condition in enumerate(Boundary_conditions):
        Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, boundary_condition)

        for i,cluster in enumerate(Clustered_data):
            ax[j].scatter(cluster[:,0], cluster[:,1], label = "Cluster - {}".format(i))
    
    ax[0].grid()
    # ax[0].legend()
    ax[1].grid()
    # ax[1].legend()
    ax[0].set(xlabel = "x coordinate ", ylabel = "y coordinate", title = "Free boundary conditions")
    ax[1].set(xlabel = "x coordinate ", ylabel = "y coordinate ", title = "Periodic boundary conditions")
    plt.suptitle("Thresholds SD : {} IC distance : {} ".format(Standard_deviation_threshold, Inter_cluster_radius_threshold), weight = "bold")
    plt.subplots_adjust(hspace = 0.2)
    plt.savefig("test_pbc.png", dpi = 150)
 
    plt.show()

    assert 1==2

# @pytest.mark.skip("Task - 4 -- Currently not testing this")
def test_9D_local_inhomogeneous_stress(get_fcc_atomic_coordinates):
    
    Atomic_cooridnates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions = get_fcc_atomic_coordinates
    Stress_data = np.zeros((No_of_atoms,6)) # 6D symmetric zero stress tensor
    Stress_data[0,0:3] = 1000 # Assigning 100 stress value to all the principal stress components of atom "20"
    Stress_data[105418,0:3] = 500 # Assigning 100 stress value to all the principal stress components of atom "20"
    Stress_data[31954,0:3] = 2000 # Assigning 100 stress value to all the principal stress components of atom "20"
    Input_data_unstandardized = np.append(Atomic_cooridnates, Stress_data, axis = 1)
    Standard_deviation_threshold = 0.5
    Inter_cluster_radius_threshold = 0.6 * Standard_deviation_threshold
    Max_iterations = 500
    Mean_tolerance = 1e-8
    Std_tolerance = 1e-8
    Minimum_no_of_data_per_cluster = 0
    Box_dimensions = np.array([[0,Box_x_end],[0,Box_y_end],[0,Box_z_end]])
    
    Clustered_data,No_of_clusters, Cluster_indices, Log_message, SSE, Iteration = get_clustered_data(Input_data_unstandardized,Inter_cluster_radius_threshold,Standard_deviation_threshold,Max_iterations, Mean_tolerance, Std_tolerance, Minimum_no_of_data_per_cluster, Box_dimensions, Boundary_conditions)

    fig, ax = plt.subplots(figsize = (40,40))

    for i,cluster in enumerate(Clustered_data):
        print("i",i,"len(cluster:)",len(cluster))
        if len(cluster) == 1:
            ax.scatter(i,len(cluster), s = 400, label = "ID - {} ".format(i) + r"$\sigma_{11} - $" + "{}".format(cluster[0,3]))
            ax.annotate("{}".format(len(cluster)), (i,len(cluster)))
            print("len 1 cluster coordinate:",cluster)
        else:
            ax.scatter(i,len(cluster), s = 400)
        
        
    ax.grid()
    ax.legend()
    ax.set(xlabel = "Cluster ID", ylabel = "No of atoms in the cluster", xticks = range(24))
    ax.set_title("No of atoms in every cluster", weight = "bold")
    plt.savefig("test_9D_local_inhomogeneous_stress_num_atoms_in_cluster.png")

    assert Iteration < Max_iterations

    assert 1 == 2

    plt.show()
