# Program to test Local virial stress calculation
# coding: utf-8
import pytest
import numpy as np
import scipy as sp
from scipy.stats import norm
from scipy.stats import multivariate_normal
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
# from Task_1_Interatomic_force_calculation import get_interatomic_force
# from Task_2_Volume_discretization import get_voronoi_tessellation
# from Task_3_Virial_stress_calculation import get_virial_stress_tensor
from Task_5_Gaussian_Mixture_Model_test_code import get_GMM_probability_stress
from Task_5_Gaussian_Mixture_Model_test_code import get_multivariate_distribution_probability
os.chdir(cwd) # Changing back to the current directory
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D
np.set_printoptions(threshold=sys.maxsize)

from  itertools import product 


@pytest.mark.skip("Task - 5 -- Currently not testing this")
def test_multi_variate_probability_function_check(get_multi_variate_distribution_inputs):
    """
        Creates "n" normal unimodal 1D distributions and tests whether the multi_variate 
        distribution probability function returns the multi-modal distribution
    """
    print(test_multi_variate_probability_function_check.__doc__)

    Clusters, No_of_data, All_data_points = get_multi_variate_distribution_inputs

    GMM_probability, GMM_stress = get_GMM_probability_stress(Clusters,No_of_data)

    fig, ax = plt.subplots()
    for i in range(len(Clusters)):
        ax.scatter(Clusters[i],norm.pdf(Clusters[i], np.mean(Clusters[i]), np.std(Clusters[i])), label = "Probability - {}".format(i), marker = ",", s = 3)

    ax.scatter(All_data_points,GMM_probability, label = "GMM probability", marker = "o", s = 5)
    ax.legend()
    ax.grid()
    plt.savefig("Task_5_test_multi_variate_probability_function_check.png")

@pytest.mark.skip("Task - 5 -- Currently not testing this")
def test_multi_variate_with_scipy():
    """
        Creates a 1D array and tests the multi-variate probability values with the scipy multi-variate probability output
    """
    print(test_multi_variate_with_scipy.__doc__)

    Data_points = np.random.normal(2,2,1000)
    Mean = 2
    Co_variance =  2
    
    Multivariate_probability = []
    for point in Data_points:
        Multivariate_probability.append((get_multivariate_distribution_probability(point, Mean, Co_variance, 1).item()))
    Scipy_probability = multivariate_normal.pdf(Data_points, Mean, Co_variance)
    
    fig, ax = plt.subplots()
    ax.scatter(Data_points,Multivariate_probability, label = "Multi-variate probability", s = 50)
    ax.scatter(Data_points, Scipy_probability, label = "Scipy probability", s = 10)
    ax.legend()
    ax.grid()
    plt.savefig("Task_5_test_multi_variate_with_scipy.png")
    
@pytest.mark.skip("Task - 5 -- Currently not testing this")
def test_conditional_stress_distribution(get_stress_conditional_distribution_inputs):
    
    """
        Test the distribution of stress around the atoms having the peak stress based on conditional probability
    """
    print(test_conditional_stress_distribution.__doc__)

    Clusters, No_of_data, All_points = get_stress_conditional_distribution_inputs
    
    GMM_probability, GMM_stress = get_GMM_probability_stress(Clusters, No_of_data)
    
    fig, ax = plt.subplots(2, figsize = (40,20))
    plot1 = ax[0].scatter(All_points[0:,0],All_points[0:,1], c = All_points[0:,3], label = "Before GMM", cmap = "rainbow")
    plot2 = ax[1].scatter(All_points[0:,0],All_points[0:,1], c = GMM_stress[0:,0], label = "After GMM", cmap = "rainbow")
    ax[0].grid()
    ax[1].grid()
    cbar1 = fig.colorbar(plot1, ax = ax[0])
    cbar1.set_label(r"$\sigma_{11} \ \frac{ev}{\AA^3}$")
    cbar2 = fig.colorbar(plot2, ax = ax[1])
    cbar2.set_label(r"$\sigma_{11} \ \frac{ev}{\AA^3}$")
    ax[0].set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")")
    ax[0].set_title("Before applying GMM conditional probability", weight = "bold")
    ax[1].set(xlabel = "x coordinate ("+r"$\AA$"+")", ylabel = "y coordinate ("+r"$\AA$"+")")
    ax[1].set_title("After applying GMM conditional probability", weight = "bold")
    plt.suptitle("GMM - Test for conditional probability distribution on " + r"$\sigma_{11}$" + " component", weight = "bold")
    plt.subplots_adjust(hspace = 0.5)
    plt.savefig("Task_5_test_conditional_stress_distribution.png")
    