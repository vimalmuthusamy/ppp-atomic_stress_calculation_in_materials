# Configuration file for pytest test files to access input data
import pytest,os
import numpy as np
from py.xml import html
from itertools import product

def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend([html.p("Project: Atomic stress calculation program")])
    prefix.extend([html.p("Task 1 Interatomic force calculation routine")])

@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    Test_case = str(item.function).split(" ")[1]
    if report.when == 'call':
        if Test_case == "test_periodic_boundary_conditions":
            extra.append(pytest_html.extras.image("./test_periodic_boundary_conditions.png"))
        elif Test_case == "test_Ag_zero_force":
            extra.append(pytest_html.extras.image("./test_Ag_zero_force.png"))
        elif Test_case == "test_Ag_diatom_force":
            extra.append(pytest_html.extras.image("./test_Ag_diatom_force.png"))
        elif Test_case == "test_Ag_force_z":
            extra.append(pytest_html.extras.image("./test_Ag_force_z-Force_z_3d.png"))
            extra.append(pytest_html.extras.image("./test_Ag_force_z-Force_x.png"))
            extra.append(pytest_html.extras.image("./test_Ag_force_z-Force_y.png"))
            extra.append(pytest_html.extras.image("./test_Ag_force_z-Force_z.png"))
        elif Test_case == "test_Al_zero_force":
            extra.append(pytest_html.extras.image("./test_Al_zero_force.png"))
        elif Test_case == "test_Ni_zero_force":
            extra.append(pytest_html.extras.image("./test_Ni_zero_force.png"))
        elif Test_case == "test_Ni_force_lammps_comparison":
            extra.append(pytest_html.extras.image("./test_Ni_force_lammps_comparison.png"))
        elif Test_case == "test_Pt_zero_force":
            extra.append(pytest_html.extras.image("./test_Cu_zero_force.png"))
        elif Test_case == "test_Au_zero_force":
            extra.append(pytest_html.extras.image("./test_Au_zero_force.png"))
        elif Test_case == "test_Au_diatom_force":
            extra.append(pytest_html.extras.image("./test_Au_diatom_force.png"))

        report.extra = extra
        
@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ag():
    Atomic_coordinates_file_name = "dump.Ag_SX_cohesive_energy_l_3.custom.2"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ag_surface_energy_100():
    Atomic_coordinates_file_name = "dump.Ag_SX_surface_energy_x_100_l_3.custom.13"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ag_surface_energy_110():
    Atomic_coordinates_file_name = "dump.Ag_SX_surface_energy_y_110_l_3.custom.18"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ag_surface_energy_111():
    Atomic_coordinates_file_name = "dump.Ag_SX_surface_energy_z_111_l_3.custom.83"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_diatom():
    Atomic_coordinates = np.zeros((2,3)).T
    Boundary_conditions = ["ff","ff","ff"] # Free boundary conditions
    Box_x_end = 0
    Box_y_end = 0
    Box_z_end = 0
    No_of_atoms = 2
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ag_force_z():
    Atomic_coordinates_file_name = "dump.Ag_SX_force_z_l_6.custom.2"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1]
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ag_pbc_test():
    Atomic_coordinates_file_name = "dump.Ag_SX_force_z_l_10.custom.119"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Al():
    Atomic_coordinates_file_name = "dump.Al_SX_cohesive_energy_l_3.custom.1"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Al_surface_energy_100():
    Atomic_coordinates_file_name = "dump.Al_SX_surface_energy_x_100_l_3.custom.12"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Al_surface_energy_110():
    Atomic_coordinates_file_name = "dump.Al_SX_surface_energy_y_110_l_3.custom.24"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Al_surface_energy_111():
    Atomic_coordinates_file_name = "dump.Al_SX_surface_energy_z_111_l_3.custom.81"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions
    
@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ni():
    Atomic_coordinates_file_name = "dump.Ni_SX_cohesive_energy_l_3.custom.2"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ni_lammps_comparison():
    Atomic_coordinates_file_name = "dump.Ni_SX_no_pbc_l_3.custom.100"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    Velocity_components = np.genfromtxt(Atomic_coordinates_file_name, usecols = (4,5,6), skip_header = 9) # According to LAMMPS dump file format
    Force_components = np.genfromtxt(Atomic_coordinates_file_name, usecols = (7,8,9), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    Stress_components = np.genfromtxt(Atomic_coordinates_file_name, usecols = (10,11,12,13,14,15), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    Volume_per_atom = np.genfromtxt(Atomic_coordinates_file_name, usecols = (16), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions, Force_components, Stress_components, Volume_per_atom, Velocity_components

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ni_surface_energy_100():
    Atomic_coordinates_file_name = "dump.Ni_SX_surface_energy_x_100_l_3.custom.6"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ni_surface_energy_110():
    Atomic_coordinates_file_name = "dump.Ni_SX_surface_energy_y_110_l_3.custom.28"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Ni_surface_energy_111():
    Atomic_coordinates_file_name = "dump.Ni_SX_surface_energy_z_111_l_3.custom.27"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions


@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Pt():
    Atomic_coordinates_file_name = "dump.Pt_SX_cohesive_energy_l_3.custom.1"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Pt_surface_energy_100():
    Atomic_coordinates_file_name = "dump.Pt_SX_surface_energy_x_100_l_3.custom.9"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Pt_surface_energy_110():
    Atomic_coordinates_file_name = "dump.Pt_SX_surface_energy_y_110_l_3.custom.16"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Pt_surface_energy_111():
    Atomic_coordinates_file_name = "dump.Pt_SX_surface_energy_z_111_l_3.custom.18"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Au():
    Atomic_coordinates_file_name = "dump.Au_SX_cohesive_energy_l_3.custom.1"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Au_surface_energy_100():
    Atomic_coordinates_file_name = "dump.Au_SX_surface_energy_x_100_l_3.custom.21"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Au_surface_energy_110():
    Atomic_coordinates_file_name = "dump.Au_SX_surface_energy_y_110_l_3.custom.22"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_force_routine_inputs_Au_surface_energy_111():
    Atomic_coordinates_file_name = "dump.Au_SX_surface_energy_z_111_l_3.custom.33"
    Atomic_coordinates = np.genfromtxt(Atomic_coordinates_file_name, usecols = (1,2,3), unpack = True, skip_header = 9) # According to LAMMPS dump file format
    No_of_atoms = int(np.genfromtxt(Atomic_coordinates_file_name, skip_header = 3, max_rows = 1)) # According to LAMMPS dump file format
    Box_dimensions = np.genfromtxt(Atomic_coordinates_file_name, usecols = (0,1), skip_header = 5, max_rows = 3) # According to LAMMPS dump file format
    Box_x_start, Box_x_end = Box_dimensions[0][0], Box_dimensions[0][1] 
    Box_y_start, Box_y_end = Box_dimensions[1][0], Box_dimensions[1][1] 
    Box_z_start, Box_z_end = Box_dimensions[2][0], Box_dimensions[2][1] 
    Boundary_conditions = (np.genfromtxt(Atomic_coordinates_file_name, skip_header = 4, max_rows = 1, dtype = str))[-3:] # According to LAMMPS dump file format
    
    return Atomic_coordinates, No_of_atoms, Box_x_end, Box_y_end, Box_z_end, Boundary_conditions

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_multi_variate_distribution_inputs():
    No_of_clusters = np.random.randint(2,20,1)[0]
    No_of_clusters = 4
    
    Mean = np.random.randint(0,20,No_of_clusters)
    Standard_deviation = np.random.uniform(1,5,No_of_clusters)
    No_of_points = np.random.randint(10,10000,No_of_clusters)
    
    No_of_data = np.sum(No_of_points)
    Clusters = []
    Complete_data_points = np.array([])
    for i in range(No_of_clusters):
        Clusters.append(np.random.normal(Mean[i], Standard_deviation[i], No_of_points[i]))
        Complete_data_points = np.append(Complete_data_points, Clusters[i])
    
    return np.array(Clusters), No_of_data, Complete_data_points

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_stress_conditional_distribution_inputs():
    x = np.arange(50)
    No_of_clusters = 10
    Coordinates = np.array(list(product(x, repeat = 2))) # Combinations for x & y axis
    Coordinates = np.append(Coordinates, np.zeros((len(Coordinates), 10)), axis = 1) # Adding zeros to all z coordinates & 6 stress components
    Clusters = np.array_split(Coordinates, No_of_clusters) # Splitting coordinates into 10 clusters
    Clusters[3][0:,3] = np.repeat(5,len(Clusters[3])) # Adding only the stress_11 component equal to "3" to all atoms in the cluster
    Clusters[-3][0:,3] = np.repeat(10,len(Clusters[-3])) # Adding only the stress_11 component equal to "3" to all atoms in the cluster
    No_of_data = len(Coordinates)
    
    return Clusters, No_of_data, Coordinates

@pytest.fixture # Fixtures are functions, which will run before each test function to which it is applied -> To feed data to tests
def get_cluster_inputs():
    x = np.arange(10)
    Coordinates = np.array(list(product(x, repeat = 2))) # Combinations for x & y axis
    Clusters = np.array_split(Coordinates, len(Coordinates)) # Splitting each atom in to a individual cluster

    return Coordinates, Clusters
