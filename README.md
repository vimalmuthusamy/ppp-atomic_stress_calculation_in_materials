Python program to calculate Local virial, Hardy and Tsai traction stress measures and also stress prediction based on Gaussian Mixture Model (GMM).

# OBJECTIVE: 
To combine the advantages of all the stress measure in to a single analysis and to use clustering techniques to find out the lower bound for the spatial averaging size for the Hardy stress so as to output the most accurate stress tensor per atom based on the inhomogeneties in the system.

# TASK #1 - "Interatomic force calculation"
-> Calculates the Interatomic force and energy between atoms using EAM potential (DYNAMO file format for EAM, Currently accepts Atomsk atomic coordinates) 

# TASK #2 - "Volume discretization"
-> Calculates the voronoi volume per atom using the VORO++ package

# TASK #3 - "Virial stress calculation"
-> Calculates the local virial stress per atom by taking the volume per atom and interatomic force input 

# TASK #4 - "ISODATA clustering"
-> Clusters the Position - Stress (9D) data using the Iterative Self Organising Data Analysis Technique (ISODATA) based on the Inter-cluster distance threshold and 
Standard deviation threshold for the cluster merge and split respectively

# TASK #5 - "Gaussian Mixture Model"
-> Calculates the posterior probability for every cluster and calculates the expected stress of the conditional distribution for every atomic position.

# TASK #6 - "Hardy stress"
-> Calculates the characteristic size of the spatial averaging volume from ISODATA clustering data and calculates the Hardy stress measure for every atom

# TASK #7 - "Tsai traction"
-> Calculates the Tsai traction ie) stress along the rectangular planes around an atom. 

# Main_program_atomic_stress_calculation
-> Obtains input data (EAM potential file path, Atomic coordinates file path, Inter-cluster distance threshold, Standard deviation threshold and Maximum number of 
iterations for clustering) as command line arguments

-> Outputs the atomic position and all the stress components from the calculation in an output file format that can be visualized using OVITO
