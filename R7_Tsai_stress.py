# Program to calculate the Tsai traction along 3 perpendicular planes around an atom
"""
LAMMPS "metal" units are followed
mass = grams / mole
distance = Angstroms
time = picoseconds
energy = eV
force = eV / Angstrom
velocity = Angstrom / picosecond
electron density -> Arbitrary unit
volume = Angstrom^3
stress = ev / Angstrom^3
"""

import numpy as np
from tqdm import tqdm

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient numerical computation. 
#    Computing in Science & Engineering, 13(2):22–30, 2011.
# 2) Nikhil Chandra Admal and Ellad B Tadmor. A unified interpretation of stress in molecular systems. Journal of elasticity, 100(1-2):63–143, 2010.
# 3) DH Tsai. The virial theorem and stress calculation in molecular dynamics. The Journal of Chemical Physics, 70(3):1375–1382, 1979. 
# 4) Paul Bourke. Spheres, equations and terminology, http://paulbourke.net/geometry/, April 1992
####################################################################################################################################################################

Epsilon = 1e-8 # Small value to avoid floating point errors during comparison

def check_bond_cross_plane(alpha, beta, Plane_position, Plane_1st_start, Plane_1st_end, Plane_2nd_start, Plane_2nd_end, Plane_no):
    """
        Description:
        ===========
        Calculates the slope of the bond line segment along two different axis of the plane. Interpolates the bond segment until the 
        Plane intersection point to check whether the bond intersects the plane. Extrapolation is checked and eliminated. Returns "TRUE"
        if bond intersects the plane else returns "FALSE"
        NOTE:
        ---- 
        -> The length of the square planes are considered to be 2.ravg by Admal !!!!
        -> Additional ((Maximum_point - Minimum_point) > (Distance_to_plane)) condition is to avoid 
           extrapolation of the bond -> finds if the bond does not cross the plane but lies near to the plane.
           (Distance_to_plane) >= 0 -> To eliminate its consideration if both the points lies right to the plane !

        Input:
        =====
        1) alpha = Total atomic coordinates in the system; Array size -> (No of atoms, 3)
        2) beta = Neighbor atomic coordinates of each alpha atom; Array size -> (No of atoms, No of neighbors, 3)
        3) Plane_position = Intercept of the plane along the correspoding axis; Array size -> Scalar
        4) Plane_1st_start = Left boundary of the plane; Array size -> Scalar
        5) Plane_1st_end = Right boundary of the plane; Array size -> Scalar
        6) Plane_2nd_start = Bottom boundary of the plane; Array size -> Scalar
        7) Plane_2nd_end = Top boundary of the plane; Array size -> Scalar
        8) Plane_no = Plane number across which bond crossing has to be determined; 0 - x plane, 1 - y plane, 2 - z plane; Array size -> Scalar

        Output:
        ======
        1) Boolean array = "True" if bond crosses the plane else "False"; Array size -> (1,No of bonds)
    """

    Plane_numbers = [0,1,2] # All the plane numbers in a list -> Easy to use them in slope calculations for all 3 perpendicular planes
    del Plane_numbers[Plane_no] # Deleting the plane number across which is cheked for crossing of bonds
    
    if len(beta.shape) == 3: # Only required if it is called for bond cross check (not for momentum flux)
        alpha = np.repeat(alpha, repeats = beta.shape[1], axis = 0) # Reshaping to (No of atoms, 3) -> Here same alpha value repeats for all its correspoding neighbors in beta
        beta = beta.reshape(np.prod(beta.shape[:2]),beta.shape[2]) # Reshaping to (No of atoms, 3)

    Slope_1 = np.divide((alpha[:,Plane_numbers[0]] - beta[:,Plane_numbers[0]]),  (alpha[:,Plane_no] - beta[:,Plane_no]), out = np.zeros_like((alpha[:,Plane_numbers[0]] - beta[:,Plane_numbers[0]])), where = (alpha[:,Plane_no] - beta[:,Plane_no]) != 0) # Switch of alpha and beta values will not affect the sign & value of the slope !!; np.divide() -> To take care of the bonds parallel to the plane where alpha[plane axis] = beta[plane axis]
    Slope_2 = np.divide((alpha[:,Plane_numbers[1]] - beta[:,Plane_numbers[1]]),  (alpha[:,Plane_no] - beta[:,Plane_no]), out = np.zeros_like((alpha[:,Plane_numbers[1]] - beta[:,Plane_numbers[1]])), where = (alpha[:,Plane_no] - beta[:,Plane_no]) != 0) # Plane_numbers[] will change according to the plane calculation so that the index remains "0" & "1"
    Minimum_point = np.minimum(alpha[:,Plane_no], beta[:,Plane_no]) # Calculates the minimum value in the calculation plane intersection axis
    Maximum_point = np.maximum(alpha[:,Plane_no], beta[:,Plane_no]) # Calculates the maximum value in the calculation plane intersection axis
    Value_at_slope1_start = np.select([(alpha[:,Plane_no] <= beta[:,Plane_no]), (alpha[:,Plane_no] > beta[:,Plane_no])], [alpha[:,Plane_numbers[0]], beta[:,Plane_numbers[0]]])
    Value_at_slope2_start = np.select([(alpha[:,Plane_no] <= beta[:,Plane_no]), (alpha[:,Plane_no] > beta[:,Plane_no])], [alpha[:,Plane_numbers[1]], beta[:,Plane_numbers[1]]]) # To get the starting point of the slope from where th line descents or ascents
    Distance_to_plane = (Plane_position - Minimum_point) # Calculates the distance from the minimum value to the plane intersection value in that axis
    Point_1st_direction = (Slope_1 * Distance_to_plane) + Value_at_slope1_start # Calculates the intersection point in-plane's other axis; Minimum to get the lowest point even if alpha and beta switches between them
    Point_2nd_direction = (Slope_2 * Distance_to_plane) + Value_at_slope2_start # Calculates the intersection point in-plane's other axis; NOTE: np.minimum() not np.min()
    
    return ((((Plane_1st_start - Epsilon) <= Point_1st_direction) & (Point_1st_direction <= (Plane_1st_end + Epsilon))) & (((Maximum_point - Minimum_point + Epsilon) >= (Distance_to_plane)) & ((Distance_to_plane) >= (0 - Epsilon))) & (((Plane_2nd_start - Epsilon) <= Point_2nd_direction) & (Point_2nd_direction <= (Plane_2nd_end + Epsilon))))

def get_plane_coordinates(Coordinates,Lattice_constant, Plane_size):
    """
        Description:
        ===========
        Calculates the boundary coordinates of the planes surrounding the atom. Cubic volume element with a distance of Lattice constant/2 
        from the atom is considered. x-Plane -> 0; y-Plane -> 1; z-Plane -> 2
        
        Input:
        =====
        1) Coordinates = Coordinate of the atom around which 3 perpendicular planes need to be constructed; Array size -> (1,3)
        2) Lattice_constant = Lattice constant of the element for the given system conditions

        Output:
        ======
        1) Plane_position = Intercepts of the plane along the correspoding axes; Array size -> (1,3)
        2) Plane_start_dir_1 = Left boundaries of the 3  perpendicular planes; Array size -> (1,3)
        3) Plane_end_dir_1 = Right boundaries of the 3  perpendicular planes; Array size -> (1,3)
        4) Plane_start_dir_2 = Bottom boundaries of the 3  perpendicular planes ; Array size -> (1,3)
        5) Plane_end_dir_2 = Top boundaries of the 3  perpendicular planes; Array size -> (1,3)
    """ 
    Plane_position = (Coordinates + (Lattice_constant/2)) # Intercept of the plane across the axis of interest; Planes are placed to the right side of the atom; (Lattice_constant/2) -> Has better convergence than other distances ! But the mean value of the stress is independent of the position of the plane !!
    Plane_start_dir_1 = np.array([Coordinates[1], Coordinates[0], Coordinates[0]]) - (Plane_size) # Plane -> Start point in the one of the direction; Starting coordinates of all 3 planes are assigned; Plane - x = (1&2); Plane - y = (0&2); Plane - z = (0&1); Plane size is the spatial averaging radius used in Hardy stress; Total plane length is 2*Plane size
    Plane_end_dir_1 = np.array([Coordinates[1], Coordinates[0], Coordinates[0]]) + (Plane_size) # Plane -> End point in the above direction; Ending coordinates of all 3 planes are assigned
    Plane_start_dir_2 = np.array([Coordinates[2], Coordinates[2], Coordinates[1]]) - (Plane_size)  # Plane -> Start point in the other the direction
    Plane_end_dir_2 = np.array([Coordinates[2], Coordinates[2], Coordinates[1]]) + (Plane_size)  # Plane -> End point in the above direction
    
    return Plane_position, Plane_start_dir_1, Plane_end_dir_1, Plane_start_dir_2, Plane_end_dir_2

def get_tsai_traction(No_of_atoms, Lattice_constant, Atomic_mass, Time_step, Atom_ids ,Total_atomic_coordinates, Force_vector, Velocity_vector, Plane_size, Distance_x_vector_unique, Distance_y_vector_unique, Distance_z_vector_unique, Force_x_vector_unique, Force_y_vector_unique, Force_z_vector_unique, Neighbor_atomic_coordinates_masked, Total_atomic_coordinates_next_step):
    """
        Description:
        ===========
        Calculates the mechanical definition of Cauchy (True stress) tensor in the atomic scale by calculating the force
        across 3 perpendicular planes surrounding an atom. Cubic volume element is used with side length = Lattice constant.

        Potential contribution:
        ----------------------
        All the bonds that crosses the volume element are accounted and weighted by the Plane area. Each plane calculates
        3 components of the stress tensor. Returns the per atom 9 components localized stress tensor. 

        Kinetic contribution:
        --------------------
        Calculates the momentum flux across the plane by taking the velocities of the atoms crossing the 3 perpendicular
        planes during a time step delta_T. Temporal averaging can also be performed !
        Time step in the kinetic contribution is the molecular dynamics time increment. Unit of stress (eV/A^3) in kinetic contribution only if multiplied by time step !

        Input:
        =====
        1) No_of_atoms = Number of local atoms; Array size -> Scalar
        2) Lattice_constant = Lattice constant of the element for the given system conditions
        3) Atomic_mass = Atomic mass for the given particle type
        4) Time_step = Time step used for the simulation
        5) Atom_ids = Atom indices for which Tsai stress measure needs to be calculated; Array size -> (1, No of atoms considered)
        6) Total_atomic_coordinates = Atom coordinates of all atoms (Local + Image); Array size -> (Total no of atoms, 3)
        7) Force_vector = Force x, y & z vectors of all atoms with all other atoms in the system; Array size -> (3,No of atoms, No of atoms)
        8) Velocity_vector = Velocity components of all local atoms: Array size -> (No of atoms, 3)
        9) Plane_size = Spatial averaging radius used in Hardy stress measure (got from the average cluster size); Actual plane length = 2*Plane_size; Array_size -> Scalar
        10) Distance_x_vector_unique = Distance x values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        11) Distance_y_vector_unique = Distance y values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        12) Distance_z_vector_unique = Distance z values of unique neighbors; Array size -> (No of atoms, No of unique neighbors)
        13) Force_x_vector_unique = Force x values of unique neighbors; Array size -> (Total no of atoms, No of unique neighbors)
        14) Force_y_vector_unique = Force y values of unique neighbors; Array size -> (Total no of atoms, No of unique neighbors)
        15) Force_z_vector_unique = Force z values of unique neighbors; Array size -> (Total no of atoms, No of unique neighbors)
        16) Neighbor_atomic_coordinates_masked = Atomic coordinates of the neighbors of all atoms with non-unique neighbors masked; Array size -> (Total no of atoms, No of unique neighbors, 3)
        17) Total_atomic_coordinates_next_step = Atom coordinates of all atoms (Local + Image) after time integration with the given time step; Array size -> (Total no of atoms, 3)
        
        Output:
        ======
        1) Tsai_stress_tensor = Total 9 components stress tensor (Potential + Kinetic) for all local atoms; Array size -> (No of atoms, 3, 3)
    """

    No_of_boxes = 27 # Total number of boxes (local + image)
    Plane_area = (2 * Plane_size)**2 # Square plane; Plane size is the total plane length / 2
    
    # Initialization of the 3 traction vector components 
    # t1
    Traction_vector_x_1_global = (Force_x_vector_unique * np.sign(Distance_x_vector_unique)).flatten() # Traction_vector is flattened to be (1,Total_no_of_atoms * No of neighbors)
    Traction_vector_x_2_global = (Force_x_vector_unique * np.sign(Distance_y_vector_unique)).flatten() # Force x vector in the normal 2 direction: NOTE: Radius vector . normal vector is simplified by using sign function of the corresponding distance vector (Normalized)
    Traction_vector_x_3_global = (Force_x_vector_unique * np.sign(Distance_z_vector_unique)).flatten()

    # t2    
    Traction_vector_y_1_global = (Force_y_vector_unique * np.sign(Distance_x_vector_unique)).flatten() # All the traction components are the sum of the forces acting on all the planes along a particular direction !!
    Traction_vector_y_2_global = (Force_y_vector_unique * np.sign(Distance_y_vector_unique)).flatten() # Force x vector in the normal 2 direction: NOTE: Radius vector . normal vector is simplified by using sign function of the corresponding distance vector (Normalized)
    Traction_vector_y_3_global = (Force_y_vector_unique * np.sign(Distance_z_vector_unique)).flatten()

    # t3
    Traction_vector_z_1_global = (Force_z_vector_unique * np.sign(Distance_x_vector_unique)).flatten() # Traction_vector is flattened to be (1,Total_no_of_atoms * No of neighbors)
    Traction_vector_z_2_global = (Force_z_vector_unique * np.sign(Distance_y_vector_unique)).flatten() # Force x vector in the normal 2 direction: NOTE: Radius vector . normal vector is simplified by using sign function of the corresponding distance vector (Normalized)
    Traction_vector_z_3_global = (Force_z_vector_unique * np.sign(Distance_z_vector_unique)).flatten()

    Tsai_stress_tensor = np.zeros((No_of_atoms,3,3)) # Initializing the 9-components stress tensor 
    
    for i in tqdm(Atom_ids): # Only for the atoms that requires Tsai stress calculation
                
        """***********************************3 Perpendicular plane coordinates initialization***********************************"""
        
        Plane_position, Plane_start_dir_1, Plane_end_dir_1, Plane_start_dir_2, Plane_end_dir_2 = get_plane_coordinates(Total_atomic_coordinates[i], Lattice_constant, Plane_size)
        x_plane_position, y_plane_position, z_plane_position = Plane_position[:]
        x_plane_1_start, y_plane_1_start, z_plane_1_start = Plane_start_dir_1[:]
        x_plane_1_end, y_plane_1_end, z_plane_1_end = Plane_end_dir_1[:]
        x_plane_2_start, y_plane_2_start, z_plane_2_start = Plane_start_dir_2[:]
        x_plane_2_end, y_plane_2_end, z_plane_2_end = Plane_end_dir_2[:]

        """================================================================================================================================================================
                                                                                    Potential contribution
           ================================================================================================================================================================"""

        Bond_cross_x_flag = check_bond_cross_plane(Total_atomic_coordinates, Neighbor_atomic_coordinates_masked,x_plane_position, x_plane_1_start, x_plane_1_end, x_plane_2_start, x_plane_2_end, 0).astype(int) # Total_atomic_coordinates -> Alpha atoms; Neighbor_atomic_coordinates_masked -> Beta atoms

        Bond_cross_y_flag = check_bond_cross_plane(Total_atomic_coordinates, Neighbor_atomic_coordinates_masked, y_plane_position, y_plane_1_start, y_plane_1_end, y_plane_2_start, y_plane_2_end, 1).astype(int) # ().astype(int) -> Converts the boolean array into integers (1s & 0s); True = 1, False = 0
        Bond_cross_z_flag = check_bond_cross_plane(Total_atomic_coordinates, Neighbor_atomic_coordinates_masked, z_plane_position, z_plane_1_start, z_plane_1_end, z_plane_2_start, z_plane_2_end, 2).astype(int)

        # t1
        Traction_vector_x_1 = Traction_vector_x_1_global * Bond_cross_x_flag # If bond crosses the plane, "1" is multiplied else "0" ie) Its contribution is removed !
        Traction_vector_x_2 = Traction_vector_x_2_global * Bond_cross_y_flag # All the components of the traction vector t1 should be accounted for bond crossing their corresponding normal direction planes
        Traction_vector_x_3 = Traction_vector_x_3_global * Bond_cross_z_flag

        # t2
        Traction_vector_y_1 = Traction_vector_y_1_global * Bond_cross_x_flag
        Traction_vector_y_2 = Traction_vector_y_2_global * Bond_cross_y_flag
        Traction_vector_y_3 = Traction_vector_y_3_global * Bond_cross_z_flag

        # t3
        Traction_vector_z_1 = Traction_vector_z_1_global * Bond_cross_x_flag
        Traction_vector_z_2 = Traction_vector_z_2_global * Bond_cross_y_flag
        Traction_vector_z_3 = Traction_vector_z_3_global * Bond_cross_z_flag

        """================================================================================================================================================================
                                                                                    Kinetic contribution
           ================================================================================================================================================================"""

        Atom_cross_x_plane_flag = check_bond_cross_plane(Total_atomic_coordinates, Total_atomic_coordinates_next_step,x_plane_position, x_plane_1_start, x_plane_1_end, x_plane_2_start, x_plane_2_end, 0).astype(int) # Total_atomic_coordinates -> Alpha atoms; Total atomic coordinates next step -> Beta atoms
        Atom_cross_y_plane_flag = check_bond_cross_plane(Total_atomic_coordinates, Total_atomic_coordinates_next_step, y_plane_position, y_plane_1_start, y_plane_1_end, y_plane_2_start, y_plane_2_end, 1).astype(int) # ().astype(int) -> Converts the boolean array into integers (1s & 0s); True = 1, False = 0
        Atom_cross_z_plane_flag = check_bond_cross_plane(Total_atomic_coordinates, Total_atomic_coordinates_next_step, z_plane_position, z_plane_1_start, z_plane_1_end, z_plane_2_start, z_plane_2_end, 2).astype(int)

        Relative_velocity = (Velocity_vector[i] - Velocity_vector) * np.sign((Velocity_vector[i] - Velocity_vector)) # Momentum_x across x plane; (Velocity_vector[i] - Velocity_vector) - Relative velocities; np.sign((Velocity_vector[i] - Velocity_vector)[:,0]) - Normal vector; Normal vector (n1) of plane-x 

        Momentum_x = Atomic_mass * np.tile(Relative_velocity[:,0],No_of_boxes) * Atom_cross_x_plane_flag # np.tile() -> To assign the local atom velocities to all image atoms
        Momentum_y = Atomic_mass * np.tile(Relative_velocity[:,1],No_of_boxes) * Atom_cross_y_plane_flag # If the trajectory of the atom ie) Straight line connecting the current coordinate and next step coordinate, crosses the plane, it is included in the momentum flux calculation
        Momentum_z = Atomic_mass * np.tile(Relative_velocity[:,2],No_of_boxes) * Atom_cross_z_plane_flag

        """================================================================================================================================================================
                                                                        Tsai stress = Kinetic contribution + Potential contribution
           ================================================================================================================================================================"""

        Tsai_stress_tensor[i] = - (1 / Plane_area) * (-(1/Time_step)* (np.array([np.sum(Momentum_x, axis = 0),np.sum(Momentum_y, axis = 0),np.sum(Momentum_z, axis = 0)]).reshape(-1,3)) + 
                                                   (np.array([np.sum(Traction_vector_x_1), np.sum(Traction_vector_x_2), np.sum(Traction_vector_x_3),
                                                              np.sum(Traction_vector_y_1), np.sum(Traction_vector_y_2), np.sum(Traction_vector_y_3),
                                                              np.sum(Traction_vector_z_1), np.sum(Traction_vector_z_2), np.sum(Traction_vector_z_3)]).reshape(-1,3))) # Traction vector notation; ti = sigma_ji . nj -> i = Direction of force; j = Direction of the area (plane normal); "-" sign is multiplied such that tensile stress is positive and compressive stress is negative convention since Tsai used the opposite convention

    return Tsai_stress_tensor