# Program to generate a Multi-variate Gaussian stress using the Gaussian Mixture Model (GMM) by using the Maximum Likelihood Estimation (MLE) method

import numpy as np
from tqdm import tqdm

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Stéfan van der Walt, S Chris Colbert, and Gael Varoquaux. The numpy array: a structure for efficient numerical computation. 
#    Computing in Science & Engineering, 13(2):22–30, 2011.
# 2) Manfred H Ulz and Sean J Moran. A gaussian mixture modelling approach to the data-driven estimation of atomistic support for continuum stress. 
#    Modelling and simulation in materials science and engineering, 20(6):065009, 2012.
####################################################################################################################################################################

def get_multivariate_distribution_probability(Data_point,Mean,Covariance,No_of_dimensions):
    """
        Description:
        ===========
        Calculates the probability of a data point from the multi-variate distribution of a particular
        cluster using cluster's mean and covariance matrix
        
        Input:
        =====
        1) Data_point = Data point for which probability has to be determined; Array size -> (1, No of dimensions)
        2) Mean = Mean of the cluster to which the data point belongs to; Array size -> (1, No of dimensions)
        3) Covariance = Co-variance matrix of the cluster to which the data point belongs to; Array size -> (No of dimensions, No of dimensions)
        4) No_of_dimensions = No of dimensions of the data point
        
        Output:
        ======
        1) Probability = Multi-variate Gaussian distribution probability for the input data point
    """ 
    Covariance_norm = np.linalg.norm(Covariance)
    Denominator_term = 1 / (((2*np.pi)**(No_of_dimensions/2)) * (Covariance_norm**(1/2))) if (Covariance_norm > 0) else 0 # Denominator term in the Multi-variate Gaussian distribution function
    if No_of_dimensions != 1:
        Exponent_term = np.matrix(Data_point - Mean) @ np.linalg.pinv(Covariance) @ np.matrix(Data_point - Mean).T # 1st term (1 x n) @ 2nd term (n x n) @ 3rd term (n x 1)
        """np.linalg.pinv() -> Pseudo inverse (least squares solution) is used instead of direct inverse due to the possibility of singular matrices. Moore-Penrose pseudo-inverse of a matrix
            used here using Singular Value Decomposition (SVD). It returns the direct inverse whenever is available and if singular returns the pseudo inverse"""
    else:
        Exponent_term = (Data_point - Mean) * (1/(Covariance)) * (Data_point - Mean) # 1st term (1 x n) @ 2nd term (n x n) @ 3rd term (n x 1)
    Probability = Denominator_term * np.exp(-Exponent_term/2) # Probability of the data point in the cluster
    
    return Probability

def get_gmm_fit_probability(No_of_clusters,Clusters,No_of_dimensions,No_of_data,Mixing_coefficient,Cluster_mean,Cluster_covariance_matrix):
    """
        Description:
        ===========
        Calculates the GMM probability of the all data points in all the clusters where each cluster represents a probability density function
        
        Input:
        =====
        1) No_of_clusters = Total number of clusters; Array size -> Scalar
        2) Clusters = Array of individual clusters of "N" dimensions (All data points should have equal no of dimensions); Array size -> (No of clusters, No of data points in a cluster, No of dimensions) ----- dtype = list / object
        3) No_of_dimensions = No of dimensions of the data point
        4) No_of_data = Sum of the number of data points in all the clusters
        5) Mixing_coefficient = Weight of each cluster based on the number of data points; Array size -> (1,No of clusters)
        6) Cluster_mean = Mean of the clusters; Array size -> (No of clusters, No of dimensions)
        7) Cluster_covariance_matrix = Co-variance matrix of the clusters; Array size -> (No of clusters, No of dimensions, No of dimensions)
        
        Output:
        ======
        1) Total_data_points = Flattened array of data points in all clusters; Array size -> (1, No of data points)
        2) GMM_probability = Multi-variate Gaussian distribution probability of all data points in all clusters; Array size -> (1, No of data points)
    """ 
    
    Total_data_points = np.array([])
    for i in range(No_of_clusters):
        Total_data_points = np.append(Total_data_points,Clusters[i].flatten()) # Taking only the data points; Direct flatten() is not possible due to different no of data points in a cluster
    Total_data_points = Total_data_points.reshape(No_of_data,No_of_dimensions) # All the data in the 1D array reshaped into appropriate rows & columns 

    GMM_probability = np.zeros(No_of_data) # Probability of a data point from the GMM probability function; Its sum != 1 since only discrete data point probabilities are taken

    for i in range(No_of_data):
        for j in range(No_of_clusters):
            GMM_probability[i] += Mixing_coefficient[j] * get_multivariate_distribution_probability(Total_data_points[i],Cluster_mean[j],Cluster_covariance_matrix[j],No_of_dimensions) # Calculating the GMM fit probability for a data point with all the clusters

    return Total_data_points, GMM_probability

def get_GMM_probability_stress(Clusters,No_of_data):
    """ 
        Description:
        ===========
        GMM parameters like Mixing coefficient (pi), Mean (mu) and Co-variance (Sigma^2).
        These parameters are directly calculated using the Maximum Likelihood Estimation (MLE)
        procedure since No of clusters is already predicted from ISODATA. 
        
        NOTE: A cluster should have more than one data point since fitting a Gaussian to a single
        data point using maximum likelihood, results in a spiky Gaussian that "collapses" to that
        point (Standard deviation = 0) -> Singular co-variance matrix -> Likelihood of the Gaussian
        components goes to "infinity" (This is problem in the case of a mixture of Gaussian)
        
        SINGULARITY PROBLEM: Only in the case of "0" stress condition where the stress components
        have "0" standard deviation. But since due to floating point errors in force & virial stress
        calculations,the stress values will be non-zero (1e-6) and does not raise an error !
        
        NOTE:
        ----
        GMM stress is actually a multivariate Gaussian distribution, which gives the conditional probability stress (Output is the actual stress not probability since it has the mean_stress_cond_distribution) that an 
        atom at the given position has the given stress based on the multi-variate distribution over the entire clusters. Atoms which are close (in terms of location) to the given position are likely to have the 
        similar values of the given stress & the atoms nearer to given point contribute to the stress at that point. The mixing coefficient prime here acts as a weighting factor for every cluster contribution based on 
        their proximity to the given position !!!
        
        Input:
        =====
        1) Clusters = Array of individual clusters of "N" dimensions (All data points should have equal no of dimensions); Array size -> (No of clusters, No of data points in a cluster, No of dimensions) ----- dtype = list / object
        2) No_of_data = Sum of the number of data points in all the clusters
        
        Output:
        ======
        1) GMM_probability = Multi-variate Gaussian distribution probability for all the data points; Array size -> (1,No of data points)
        2) GMM_stress = Expected stress value of the conditional distribution for each data point; Array size -> (No of data points,(No of dimension - 3)) [3 -- 3D position]
    """ 
        
    No_of_clusters = len(Clusters)
    if len(Clusters[0].shape) == 1: # For 1D clusters 
        No_of_dimensions = 1
    else:
        No_of_dimensions = Clusters[0].shape[1] # From the shape of the 1st clusters, the no of columns represents the no of dimensions         
    Mixing_coefficient = np.zeros(No_of_clusters)
    Cluster_mean = np.zeros((No_of_clusters,No_of_dimensions)) # Mean is a vector of size of dimension
    Cluster_covariance_matrix = np.zeros((No_of_clusters,No_of_dimensions,No_of_dimensions)) # Co-variance matrix of a cluster is a n x n matrix; n-> Dimensions

    for i in range(No_of_clusters):
        Mixing_coefficient[i] = len(Clusters[i]) / No_of_data # Weight of every cluster
        Cluster_mean[i] = np.mean(Clusters[i],axis=0) # Mean of data points in every cluster; axis = 0 -> Mean of every dimension
        Data_to_mean_delta = Clusters[i] - Cluster_mean[i]
        for delta_vector in Data_to_mean_delta:
            Covariance_matrix_data = np.outer(delta_vector,delta_vector) # Tensor product of the (x - mean) vectors
            Cluster_covariance_matrix[i] += Covariance_matrix_data # Summing up all data points outer product of (Data point - Mean)
        Cluster_covariance_matrix[i] = (1/len(Clusters[i])) * Cluster_covariance_matrix[i] # Dividing the sum by No of data points in the cluster   


    """================================================================================================================================================================
                                                                            GMM fit probability function
       ================================================================================================================================================================"""

    Total_data_points, GMM_probability = get_gmm_fit_probability(No_of_clusters,Clusters,No_of_dimensions,No_of_data,Mixing_coefficient,Cluster_mean,Cluster_covariance_matrix)
    
    """================================================================================================================================================================
                                                                        Stress from conditional distribution
       ================================================================================================================================================================"""
    
    GMM_stress = np.zeros((No_of_data,(No_of_dimensions-3))) # Initializing the new stress components from GMM; (No of dimensions - 3) = (12 - 3) = 9 dimensional stress if the input stress is also 9D
    for i in tqdm(range(No_of_data)):
        x_position = Total_data_points[i][:3] # Taking the positions 
        x_stress = Total_data_points[i][3:] # Taking the stress components
        
        mixing_coefficient_prime_denom = 0
        for k in range(No_of_clusters):
            mean_position = Cluster_mean[k][:3] # Taking the mean of positions 
            co_variance_position = Cluster_covariance_matrix[k][:3].T[:3].T # Slicing the first 3 x 3 matrix from co-variance matrix -> all position components
            mixing_coefficient_prime_denom += get_multivariate_distribution_probability(x_position,mean_position,co_variance_position, No_of_dimensions) # Mixing coefficient prime same for one data point
        
        for j in range(No_of_clusters):
            mean_position = Cluster_mean[j][:3] # Taking the mean of positions 
            mean_stress = Cluster_mean[j][3:] # Taking the mean of stress 
            co_variance_position = Cluster_covariance_matrix[j][:3].T[:3].T # Slicing the first 3 x 3 matrix from co-variance matrix -> all position components
            co_variance_position_stress = Cluster_covariance_matrix[j][:3].T[3:].T # Slicing the 3 x 6 matrix from co-variance matrix -> all position - stress components
            co_variance_stress = Cluster_covariance_matrix[j][3:].T[3:].T # Slicing the last 6 x 6 matrix from co-variance matrix -> all stress components
            
            # Mean Stress conditional distribution -> In the position-stress space, it is the centroid of the stress in the given Position space !!!
            
            mean_stress_cond_distribution = mean_stress + np.array((co_variance_position_stress.T) @ np.linalg.pinv(co_variance_position) @ (np.matrix(x_position - mean_position).T)).flatten() # Transpose is just for matrix multiplication compatibility; np.array broadcast to convert from np.matrix cast
            mixing_coefficient_prime = (Mixing_coefficient[j] * get_multivariate_distribution_probability(x_position,mean_position,co_variance_position, No_of_dimensions)) / (mixing_coefficient_prime_denom) # Mixing coefficient prime formula            
            GMM_stress[i] += float(mixing_coefficient_prime) * (mean_stress_cond_distribution) # Expected value of the conditional distribution of stress; float(mixing_coeff) -> It is returned as [[value]]
    return GMM_probability,GMM_stress
